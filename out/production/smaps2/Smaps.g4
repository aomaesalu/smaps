grammar Smaps;

programm
    :   lause*
    ;

lause
    :   muutujaDeklaratsioon
    |   struktuuriDeklaratsioon
    |   omistamine
    |   valikulause
    |   korduslause
    ;

kommentaar
    :   (UherealineKommentaar | MitmerealineKommentaar) // TODO: Võimalda igale poole vahele panna
    ;

muutujaDeklaratsioon // TODO: Parandada (puus millegipärast ei tuvastata)
    :   Andmetuup MuutujaNimi ';'
    |   Andmetuup '[]' MuutujaNimi ';'
    |   Struktuurituup '[]' MuutujaNimi ';'
    ;

struktuuriDeklaratsioon
    :   'point' MuutujaNimi '{' punktiParameeter* '}'
    |   'point' MuutujaNimi '(' aritmeetilineAvaldisKoordinaat ',' aritmeetilineAvaldisKoordinaat ',' aritmeetilineAvaldisSone ');'
    |   'object' MuutujaNimi '{' objektiParameeter* '}'
    |   'line' MuutujaNimi '{' jooneParameeter* '}'
    |   'area' MuutujaNimi '{' alaParameeter* '}'
    |   'map' MuutujaNimi '{' kaardiParameeter* '}'
    ;

omistamine
    :   ('int')? MuutujaNimi '=' aritmeetilineAvaldisArv ';'
    |   ('float')? MuutujaNimi '=' aritmeetilineAvaldisArv ';'
    |   ('crd' | 'coordinate')? MuutujaNimi '=' aritmeetilineAvaldisKoordinaat ';'
    |   ('bool' | 'boolean')? MuutujaNimi '=' vordlusAvaldis
    |   ('string')? MuutujaNimi '=' Sone ';'
    |   'int' '[]' MuutujaNimi '=' aritmeetilineAvaldisArvudeJarjend ';'
    |   'float' '[]' MuutujaNimi '=' aritmeetilineAvaldisArvudeJarjend ';'
    |   ('crd' | 'coordinate') '[]' MuutujaNimi '=' aritmeetilineAvaldisKoordinaatideJarjend ';'
    |   ('bool' | 'boolean') '[]' MuutujaNimi '=' aritmeetilineAvaldisToevaartusteJarjend ';'
    |   'string' '[]' MuutujaNimi '=' aritmeetilineAvaldisSonedeJarjend ';'
    |   ('point' | 'object' | 'line' | 'area' | 'map') '[]' MuutujaNimi '=' aritmeetilineAvaldisStruktuurideJarjend ';'
    ;
    // TODO: Lisa omistusoperaatorid +=, -=, *=, /=, %=
    // TODO: Luba järjendite sisu muuta

valikulause
    :   valikulauseIf valikulauseElseIf* valikulauseElse?
    ;

valikulauseIf
    :   'if' '(' vordlusAvaldis ')' '{' lause* '}'
    ;

valikulauseElseIf
    :   'else' valikulauseIf
    ;

valikulauseElse
    :   'else' '{' lause* '}'
    ;

korduslause
    :   'while' '(' vordlusAvaldis ')' '{' lause* '}'                                                           #KorduslauseWhile
    |   'do' '{' lause* '}' 'while' '(' vordlusAvaldis ');'                                                     #KorduslauseDoWhile
    |   'for' '(' lause* ';' vordlusAvaldis ';' lause* ')' '{' lause* '}'                                       #KorduslauseFor
    ;

vordlusAvaldis
    :   aritmeetilineAvaldisArv  ('>' | '<' | '>=' | '<=' | '==' | '!=') aritmeetilineAvaldisArv                #VordlusAvaldisArvud
    |   aritmeetilineAvaldisKoordinaat ('>' | '<' | '>=' | '<=' | '==' | '!=') aritmeetilineAvaldisKoordinaat   #VordlusAvaldisKoordinaadid
    |   Sone ('>' | '<' | '>=' | '<=' | '==' | '!=') Sone                                                       #VordlusAvaldisSone
    |   aritmeetilineAvaldisStruktuurideJarjend ('==' | '!=') aritmeetilineAvaldisStruktuurideJarjend           #VordlusAvaldisStruktuurideJarjend
    |   aritmeetilineAvaldisArvudeJarjend ('==' | '!=') aritmeetilineAvaldisArvudeJarjend                       #VordlusAvaldisArvudeJarjend
    |   aritmeetilineAvaldisKoordinaatideJarjend ('==' | '!=') aritmeetilineAvaldisKoordinaatideJarjend         #VordlusAvaldisKoordinaatideJarjend
    |   aritmeetilineAvaldisToevaartusteJarjend ('==' | '!=') aritmeetilineAvaldisToevaartusteJarjend           #VordlusAvaldisToevaartusteJarjend
    |   aritmeetilineAvaldisSonedeJarjend ('==' | '!=') aritmeetilineAvaldisSonedeJarjend                       #VordlusAvaldisSonedeJarjend
    |   vordlusAvaldis ('==' | '!=') Toevaartus                                                                 #VordlusAvaldisToevaartused
    |   Toevaartus                                                                                              #VordlusAvaldisToevaartus
    ;   // TODO: Boolean AND, OR, NOT jms

aritmeetilineAvaldisArv
    :   aritmeetilineAvaldisArv3
    ;

aritmeetilineAvaldisArv3
    :   aritmeetilineAvaldisArv3 ('+' | '-') aritmeetilineAvaldisArv2                   #ArvudeAvaldisLiitmineLahutamine
    |   aritmeetilineAvaldisArv2                                                        #ArvudeAvaldisTriviaalne3
    ;

aritmeetilineAvaldisArv2
    :   aritmeetilineAvaldisArv2 ('*' | '/' | '%') aritmeetilineAvaldisArv1             #ArvudeAvaldisKorrutamineJagamine
    |   aritmeetilineAvaldisArv1                                                        #ArvudeAvaldisTriviaalne2
    ;

aritmeetilineAvaldisArv1
    :   '-' aritmeetilineAvaldisArv1                                                    #ArvudeAvaldisUnaarneMiinus
    |   aritmeetilineAvaldisArv0                                                        #ArvudeAvaldisTriviaalne1
    ;

aritmeetilineAvaldisArv0
    :   MuutujaNimi                                                                     #ArvudeAvaldisMuutujaNimi
    |   (Taisarv | Ujupunktarv)                                                         #ArvudeAvaldisArv
    |   '(' aritmeetilineAvaldisArv ')'                                                 #ArvudeAvaldisSuluavaldis
    ;

aritmeetilineAvaldisKoordinaat
    :   aritmeetilineAvaldisKoordinaat ('+' | '-') koordinaat                           #KoordinaatideAvaldisLiitmineLahutamine
    |   koordinaat                                                                      #KoordinaatideAvaldisKoordinaat // MuutujaNimi on juba seal sees
    ;

aritmeetilineAvaldisSone
    :   MuutujaNimi                                                                     #SonedeAvaldisMuutujaNimi
    |   Sone                                                                            #SonedeAvaldisSone
    ;

aritmeetilineAvaldisStruktuurideJarjend
    :   aritmeetilineAvaldisStruktuurideJarjend ('+' | '-') struktuurideJarjend         #StruktuurideAvaldisLiitmineLahutamine
    |   struktuurideJarjend                                                             #StruktuurideAvaldisStruktuurideJarjend
    ;

aritmeetilineAvaldisArvudeJarjend
    :   aritmeetilineAvaldisArvudeJarjend ('+' | '-') arvudeJarjend                     #ArvudeJarjendiAvaldisLiitmineLahutamine
    |   arvudeJarjend                                                                   #ArvudeJarjendiAvaldisArvudeJarjend
    ;

aritmeetilineAvaldisKoordinaatideJarjend
    :   aritmeetilineAvaldisKoordinaatideJarjend ('+' | '-') koordinaatideJarjend       #KoordinaatideJarjendiAvaldisLiitmineLahutamine
    |   koordinaatideJarjend                                                            #KoordinaatideJarjendiAvaldisKoordinaatideJarjend
    ;

aritmeetilineAvaldisToevaartusteJarjend
    :   aritmeetilineAvaldisToevaartusteJarjend ('+' | '-') toevaartusteJarjend         #ToevaartusteJarjendiAvaldisLiitmineLahutamine
    |   toevaartusteJarjend                                                             #ToevaartusteJarjendiAvaldisToevaartusteJarjend
    ;

aritmeetilineAvaldisSonedeJarjend
    :   aritmeetilineAvaldisSonedeJarjend ('+' | '-') sonedeJarjend                     #SonedeJarjendiAvaldisLiitmineLahutamine
    |   sonedeJarjend                                                                   #SonedeJarjendiAvaldisSonedeJarjend
    ;

punktiParameeter
    :   ('crds' | 'coordinates') ':' punkt ';'
    |   'name:' nimeParameeter ';'
    |   'display:' KuvaVaartus ';'
    ;

objektiParameeter
    :   ('crds' | 'coordinates') ':' punkt ';'
    |   ('dim' | 'dimensions') ':' arvudeJarjend ';' // TODO: Mille alusel mõõtmeid määrata?
    |   'name:' nimeParameeter ';'
    |   'type:' ObjektiTuup ';'
    |   'display:' KuvaVaartus ';'
    ;

jooneParameeter
    :   ('crds' | 'coordinates') ':' punktideJarjend ';'
    |   'name:' nimeParameeter ';'
    |   'type:' JooneTuup ';'
    |   'display:' KuvaVaartus ';'
    ;

alaParameeter
    :   ('crds' | 'coordinates') ':' punktideJarjend ';'
    |   'name:' nimeParameeter ';'
    |   'type:' AlaTuup ';'
    |   'display:' KuvaVaartus ';'
    ;

kaardiParameeter
    :   'name:' nimeParameeter ';'
    |   'type:' KaardiTuup ';'
    |   ('maps' | 'areas' | 'lines' | 'objects' | 'points') ':' aritmeetilineAvaldisStruktuurideJarjend ';'
    ;

nimeParameeter
    :   MuutujaNimi
    |   Sone
    ;

arvudeJarjend
    :   '{' aritmeetilineAvaldisArv (',' aritmeetilineAvaldisArv)* '}'
    |   MuutujaNimi
    ;

koordinaatideJarjend
    :   '{' aritmeetilineAvaldisKoordinaat (',' aritmeetilineAvaldisKoordinaat)* '}'
    |   MuutujaNimi
    ;

toevaartusteJarjend
    :   '{' vordlusAvaldis (',' vordlusAvaldis)* '}'
    |   MuutujaNimi
    ;

sonedeJarjend
    :   '{' aritmeetilineAvaldisSone (',' aritmeetilineAvaldisSone)* '}'
    |   MuutujaNimi
    ;

struktuurideJarjend
    :   '{' MuutujaNimi (',' MuutujaNimi)* '}' // Kunagi hiljem nimeruumipõhisena
    |   MuutujaNimi
    ;

punktideJarjend
    :   '{' punktiKoordinaadid (',' punktiKoordinaadid)* '}'
    |   MuutujaNimi
    ;

punktiKoordinaadid
    :   punkt
    |   MuutujaNimi ('.' ('crds' | 'coordinates'))?
    ;

punkt
    :   '{' koordinaat ',' koordinaat '}'
    |   MuutujaNimi
    ;

koordinaat
    :   koordinaatarv
    |   MuutujaNimi ('.' ('x' | 'y'))?
    ;

koordinaatarv
    :   (laiuskraad|pikkuskraad)
    ;

laiuskraad
    :   laiuskraadiTahis Ujupunktarv
    |   laiuskraadiTahis Taisarv
    |   Ujupunktarv laiuskraadiTahis
    |   Taisarv laiuskraadiTahis
    ;

pikkuskraad
    :   pikkuskraadiTahis Ujupunktarv
    |   pikkuskraadiTahis Taisarv
    |   Ujupunktarv pikkuskraadiTahis
    |   Taisarv pikkuskraadiTahis
    ;

laiuskraadiTahis
    :   (Pohi|Louna)
    ;

pikkuskraadiTahis
    :   (Ida|Laas)
    ;

Andmetuup
    :   'int'
    |   'float'
    |   'crd' | 'coordinate'
    |   'bool' | 'boolean'
    |   'string'
    ;

Struktuurituup
    :   'point'
    |   'object'
    |   'line'
    |   'area'
    |   'map'
    ;

Pohi
    :   'N'
    ;

Louna
    :   'S'
    ;

Ida
    :   'E'
    ;

Laas
    :   'W'
    ;

MuutujaNimi
    :   [a-zA-Z_][a-zA-Z0-9_]*
    ;

Taisarv
    :   [-+]?[0-9]+
    ;

Ujupunktarv
    :   [-+]?[0-9]*'.'?[0-9]+
    ;

Toevaartus
    :   'True' | 'true'
    |   'False' | 'false'
    ;

Sone
    :   '"' [a-zA-Z0-9_ÕÄÖÜõäöü]* '"'
    ;

ObjektiTuup
    :   'building'
    |   'house'
    |   'monument'
    ;   // TODO: Loo uusi kirjeldusi

JooneTuup
    :   'street'
    |   'highway'
    |   'sidewalk'
    |   'path'
    |   'birds-eye'
    ;   // TODO: Loo uusi kirjeldusi

AlaTuup
    :   'state'
    |   'district'
    |   'range'
    ;   // TODO: Loo uusi kirjeldusi

KaardiTuup
    :   'geographical'
    ;   // TODO: Loo uusi kirjeldusi

KuvaVaartus
    :   'show'
    |   'hidden'
    ;

UherealineKommentaar
    :   '//' ~('\r' | '\n')* -> skip
    ;

MitmerealineKommentaar
    :   '/*' .*? '*/' -> skip
    ;

Whitespace
    :   [ \t\r\n]+ -> skip // TODO: Kontrolli, et tühemikke õigesti protsessitaks
    ;