package Struktuur;

import java.util.ArrayList;
import java.util.List;

public class Ala {
    private List<Punkt> koordinaadid = new ArrayList<Punkt>();
    private String nimi;
    private AlaTuup tuup;
    private Kuva kuva = Kuva.KUVA;

    public Ala() {

    }

    public List<Punkt> getKoordinaadid() {
        return koordinaadid;
    }

    public void setKoordinaadid(List<Punkt> koordinaadid) {
        this.koordinaadid = koordinaadid;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public AlaTuup getTuup() {
        return tuup;
    }

    public void setTuup(AlaTuup tuup) {
        this.tuup = tuup;
    }

    public Kuva getKuva() {
        return kuva;
    }

    public void setKuva(Kuva kuva) {
        this.kuva = kuva;
    }
}
