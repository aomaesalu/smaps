package Struktuur;

public enum AlaTuup {
    STATE("state"), DISTRICT("district"), AREA("area");

    private String vaartus;

    private AlaTuup(String vaartus) {
        this.vaartus = vaartus;
    }
}