package Struktuur;

/*enum Laiuskraad {
    POHI('N'), LOUNA('S');

    private char vaartus;

    private Laiuskraad(char vaartus) {
        this.vaartus = vaartus;
    }
}

enum Pikkuskraad {
    IDA('E'), LAAS('W');

    private char vaartus;

    private Pikkuskraad(char vaartus) {
        this.vaartus = vaartus;
    }
}*/

public enum Ilmakaar {
    POHI('N'), LOUNA('S'), IDA('E'), LAAS('W');

    private char vaartus;

    private Ilmakaar(char vaartus) {
        this.vaartus = vaartus;
    }
}