package Struktuur;

public enum ObjektiTuup {
    BUILDING("building"), HOUSE("house"), MONUMENT("monument");

    private String vaartus;

    private ObjektiTuup(String vaartus) {
        this.vaartus = vaartus;
    }
}