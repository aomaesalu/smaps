package Struktuur;
import java.util.List;
import java.util.ArrayList;

public class Objekt {
    Koordinaat laiuskraad;
    Koordinaat pikkuskraad;
    // TODO: Lisa mõõtmed!
    private String nimi;
    private ObjektiTuup tuup;
    private Kuva kuva = Kuva.KUVA;

    public Objekt() {

    }

    public Koordinaat getLaiuskraad() {
        return laiuskraad;
    }

    public void setLaiuskraad(Koordinaat laiuskraad) {
        this.laiuskraad = laiuskraad;
    }

    public Koordinaat getPikkuskraad() {
        return pikkuskraad;
    }

    public void setPikkuskraad(Koordinaat pikkuskraad) {
        this.pikkuskraad = pikkuskraad;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public ObjektiTuup getTuup() {
        return tuup;
    }

    public void setTuup(ObjektiTuup tuup) {
        this.tuup = tuup;
    }

    public Kuva getKuva() {
        return kuva;
    }

    public void setKuva(Kuva kuva) {
        this.kuva = kuva;
    }
}
