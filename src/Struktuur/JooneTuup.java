package Struktuur;

public enum JooneTuup {
    STREET("street"), HIGHWAY("highway"), SIDEWALK("sidewalk"), PATH("path"), BIRDSEYE("birds-eye");

    private String vaartus;

    private JooneTuup(String vaartus) {
        this.vaartus = vaartus;
    }
}