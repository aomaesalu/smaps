package Struktuur;
import java.util.List;
import java.util.ArrayList;

public class Punkt {
    Koordinaat laiuskraad;
    Koordinaat pikkuskraad;
    private String nimi;
    private Kuva kuva = Kuva.KUVA;

    public Punkt(Koordinaat laiuskraad, Koordinaat pikkuskraad, String nimi, Kuva kuva) {
        this.laiuskraad = laiuskraad;
        this.pikkuskraad = pikkuskraad;
        this.nimi = nimi;
        this.kuva = kuva;
    }

    public Punkt(Koordinaat laiuskraad, Koordinaat pikkuskraad, String nimi) {
        this.laiuskraad = laiuskraad;
        this.pikkuskraad = pikkuskraad;
        this.nimi = nimi;
    }

    public Punkt(Koordinaat laiuskraad, Koordinaat pikkuskraad, Kuva kuva) {
        this.laiuskraad = laiuskraad;
        this.pikkuskraad = pikkuskraad;
        this.kuva = kuva;
    }

    public Punkt(Koordinaat laiuskraad, Koordinaat pikkuskraad) {
        this.laiuskraad = laiuskraad;
        this.pikkuskraad = pikkuskraad;
    }

    public Punkt() {

    }

    public Koordinaat getLaiuskraad() {
        return laiuskraad;
    }

    public void setLaiuskraad(Koordinaat laiuskraad) {
        this.laiuskraad = laiuskraad;
    }

    public Koordinaat getPikkuskraad() {
        return pikkuskraad;
    }

    public void setPikkuskraad(Koordinaat pikkuskraad) {
        this.pikkuskraad = pikkuskraad;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public Kuva getKuva() {
        return kuva;
    }

    public void setKuva(Kuva kuva) {
        this.kuva = kuva;
    }
}
