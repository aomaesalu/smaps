package Struktuur;

public class Koordinaat {
    private float suurus;
    private Ilmakaar ilmakaar;

    public Koordinaat(float suurus, Ilmakaar ilmakaar) {
        this.suurus = suurus;
        this.ilmakaar = ilmakaar;
    }

    public Koordinaat(Koordinaat koordinaat) {
        this.suurus = koordinaat.getSuurus();
        this.ilmakaar = koordinaat.getIlmakaar();
    }

    public Koordinaat() {

    }

    public Koordinaat liida(Koordinaat koordinaat) {
        Koordinaat ajutine = new Koordinaat(this);
        if (ajutine.getIlmakaar() == Ilmakaar.POHI || ajutine.getIlmakaar() == Ilmakaar.LOUNA) {
            if (koordinaat.getIlmakaar() == Ilmakaar.POHI || koordinaat.getIlmakaar() == Ilmakaar.LOUNA) {
                if (ajutine.getIlmakaar() == koordinaat.getIlmakaar()) {
                    ajutine.setSuurus(ajutine.getSuurus() + koordinaat.getSuurus());
                    if (ajutine.getSuurus() > 90) {
                        ajutine.setSuurus(180 - ajutine.getSuurus());
                        if (ajutine.getIlmakaar() == Ilmakaar.POHI) {
                            ajutine.setIlmakaar(Ilmakaar.LOUNA);
                        } else {
                            ajutine.setIlmakaar(Ilmakaar.POHI);
                        }
                    }
                } else {
                    if (ajutine.getSuurus() > koordinaat.getSuurus()) {
                        ajutine.setSuurus(ajutine.getSuurus() - koordinaat.getSuurus());
                    } else {
                        ajutine.setSuurus(koordinaat.getSuurus() - ajutine.getSuurus());
                        if (ajutine.getIlmakaar() == Ilmakaar.POHI) {
                            ajutine.setIlmakaar(Ilmakaar.LOUNA);
                        } else {
                            ajutine.setIlmakaar(Ilmakaar.POHI);
                        }
                    }
                }
            }
        } else {
            if (koordinaat.getIlmakaar() == Ilmakaar.IDA || koordinaat.getIlmakaar() == Ilmakaar.LAAS) {
                if (ajutine.getIlmakaar() == koordinaat.getIlmakaar()) {
                    ajutine.setSuurus(ajutine.getSuurus() + koordinaat.getSuurus());
                    if (ajutine.getSuurus() > 180) {
                        ajutine.setSuurus(360 - ajutine.getSuurus());
                        if (ajutine.getIlmakaar() == Ilmakaar.IDA) {
                            ajutine.setIlmakaar(Ilmakaar.LAAS);
                        } else {
                            ajutine.setIlmakaar(Ilmakaar.IDA);
                        }
                    }
                } else {
                    if (ajutine.getSuurus() > koordinaat.getSuurus()) {
                        ajutine.setSuurus(ajutine.getSuurus() - koordinaat.getSuurus());
                    } else {
                        ajutine.setSuurus(koordinaat.getSuurus() - ajutine.getSuurus());
                        if (ajutine.getIlmakaar() == Ilmakaar.IDA) {
                            ajutine.setIlmakaar(Ilmakaar.LAAS);
                        } else {
                            ajutine.setIlmakaar(Ilmakaar.IDA);
                        }
                    }
                }
            }
        }
        return ajutine;
    }

    public Koordinaat lahuta(Koordinaat koordinaat) {
        Koordinaat ajutine = new Koordinaat(this);
        if (ajutine.getIlmakaar() == Ilmakaar.POHI || ajutine.getIlmakaar() == Ilmakaar.LOUNA) {
            if (koordinaat.getIlmakaar() == Ilmakaar.POHI || koordinaat.getIlmakaar() == Ilmakaar.LOUNA) {
                if (ajutine.getIlmakaar() == koordinaat.getIlmakaar()) {
                    if (ajutine.getSuurus() > koordinaat.getSuurus()) {
                        ajutine.setSuurus(ajutine.getSuurus() - koordinaat.getSuurus());
                    } else {
                        ajutine.setSuurus(koordinaat.getSuurus() - ajutine.getSuurus());
                        if (ajutine.getIlmakaar() == Ilmakaar.POHI) {
                            ajutine.setIlmakaar(Ilmakaar.LOUNA);
                        } else {
                            ajutine.setIlmakaar(Ilmakaar.POHI);
                        }
                    }
                } else {
                    ajutine.setSuurus(ajutine.getSuurus() + koordinaat.getSuurus());
                    if (ajutine.getSuurus() > 90) {
                        ajutine.setSuurus(180 - ajutine.getSuurus());
                        if (ajutine.getIlmakaar() == Ilmakaar.POHI) {
                            ajutine.setIlmakaar(Ilmakaar.LOUNA);
                        } else {
                            ajutine.setIlmakaar(Ilmakaar.POHI);
                        }
                    }
                }
            }
        } else {
            if (koordinaat.getIlmakaar() == Ilmakaar.IDA || koordinaat.getIlmakaar() == Ilmakaar.LAAS) {
                if (ajutine.getIlmakaar() == koordinaat.getIlmakaar()) {
                    if (ajutine.getSuurus() > koordinaat.getSuurus()) {
                        ajutine.setSuurus(ajutine.getSuurus() - koordinaat.getSuurus());
                    } else {
                        ajutine.setSuurus(koordinaat.getSuurus() - ajutine.getSuurus());
                        if (ajutine.getIlmakaar() == Ilmakaar.IDA) {
                            ajutine.setIlmakaar(Ilmakaar.LAAS);
                        } else {
                            ajutine.setIlmakaar(Ilmakaar.IDA);
                        }
                    }
                } else {
                    ajutine.setSuurus(ajutine.getSuurus() + koordinaat.getSuurus());
                    if (ajutine.getSuurus() > 180) {
                        ajutine.setSuurus(360 - ajutine.getSuurus());
                        if (ajutine.getIlmakaar() == Ilmakaar.IDA) {
                            ajutine.setIlmakaar(Ilmakaar.LAAS);
                        } else {
                            ajutine.setIlmakaar(Ilmakaar.IDA);
                        }
                    }
                }
            }
        }
        return ajutine;
    }

    public float getSuurus() {
        return suurus;
    }

    public void setSuurus(float suurus) {
        this.suurus = suurus;
    }

    public Ilmakaar getIlmakaar() {
        return ilmakaar;
    }

    public void setIlmakaar(Ilmakaar ilmakaar) {
        this.ilmakaar = ilmakaar;
    }
}
