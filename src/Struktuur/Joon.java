package Struktuur;

import java.util.ArrayList;
import java.util.List;

public class Joon {
    private List<Punkt> koordinaadid = new ArrayList<Punkt>();
    private String nimi;
    private JooneTuup tuup;
    private Kuva kuva = Kuva.KUVA;

    public Joon() {

    }

    public List<Punkt> getKoordinaadid() {
        return koordinaadid;
    }

    public void setKoordinaadid(List<Punkt> koordinaadid) {
        this.koordinaadid = koordinaadid;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public JooneTuup getTuup() {
        return tuup;
    }

    public void setTuup(JooneTuup tuup) {
        this.tuup = tuup;
    }

    public Kuva getKuva() {
        return kuva;
    }

    public void setKuva(Kuva kuva) {
        this.kuva = kuva;
    }
}
