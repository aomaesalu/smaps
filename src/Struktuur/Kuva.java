package Struktuur;

public enum Kuva {
    KUVA("show"), PEIDA("hidden");

    private String vaartus;

    private Kuva(String vaartus) {
        this.vaartus = vaartus;
    }
}