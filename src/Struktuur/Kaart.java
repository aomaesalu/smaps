package Struktuur;

import java.util.ArrayList;
import java.util.List;

public class Kaart {
    private String nimi;
    private KaardiTuup tuup;
    private List<Punkt> punktid = new ArrayList<Punkt>();
    private List<Objekt> objektid = new ArrayList<Objekt>();
    private List<Joon> jooned = new ArrayList<Joon>();
    private List<Ala> alad = new ArrayList<Ala>();
    private List<Kaart> kaardid = new ArrayList<Kaart>();

    public Kaart() {

    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public KaardiTuup getTuup() {
        return tuup;
    }

    public void setTuup(KaardiTuup tuup) {
        this.tuup = tuup;
    }

    public List<Punkt> getPunktid() {
        return punktid;
    }

    public void setPunktid(List<Punkt> punktid) {
        this.punktid = punktid;
    }

    public List<Objekt> getObjektid() {
        return objektid;
    }

    public void setObjektid(List<Objekt> objektid) {
        this.objektid = objektid;
    }

    public List<Joon> getJooned() {
        return jooned;
    }

    public void setJooned(List<Joon> jooned) {
        this.jooned = jooned;
    }

    public List<Ala> getAlad() {
        return alad;
    }

    public void setAlad(List<Ala> alad) {
        this.alad = alad;
    }

    public List<Kaart> getKaardid() {
        return kaardid;
    }

    public void setKaardid(List<Kaart> kaardid) {
        this.kaardid = kaardid;
    }
}
