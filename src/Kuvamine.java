import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;

class Kuvamine extends JFrame{


    public static JTextField tekstikast;
    public static JPanel panel;
    public static int i;
    public static int j=0;
    public static String inputFile;
    public static String ekraanile;
    public static JScrollPane scrollPane;

    public static ArrayList<String> getList() {
        return list;
    }

    public static void setList(ArrayList<String> list) {
        Kuvamine.list = list;
    }

    public static String getInputFile() {
        return inputFile;
    }

    public static void setInputFile(String inputFile) {
        Kuvamine.inputFile = inputFile;
    }

    public static ArrayList<String> list;



    int x = 0, y = 0;
    Kuvamine() {
        super("Smaps");
        this.setBounds(400,100,800,700);
        list = new ArrayList<String>() {{}};

        panel = new JPanel();
        panel.setLayout(null);

        JButton nupp = new JButton("Kuvamine");
        nupp.setBounds(50,100,100,30);
        nupp.setLocation(350,40);
        panel.add(nupp);

        JLabel kastiNimi = new JLabel("Sisestage siia teisendatava .smaps faili nimi: ");
        kastiNimi.setBounds(20, 10, 300, 20);
        panel.add(kastiNimi);
        ekraanile="";

        tekstikast=new JTextField();
        tekstikast.setBounds(20, 40, 280, 30);
        panel.add(tekstikast);


        panel.setBackground(new Color(153,255,153));

        //JPanel p = new JPanel();
        panel.setPreferredSize(new Dimension(getWidth(), getHeight()));
       // p.setLayout(null);
        //p.add(panel);

        scrollPane = new JScrollPane(panel);
        //scrollPane.setBounds(0,0,getWidth(),getHeight());
        scrollPane.setPreferredSize(new Dimension(getWidth(), getHeight()));
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        JPanel contentPane = new JPanel(null);

       // contentPane.setPreferredSize(new Dimension(500, 400));
        contentPane.add(scrollPane);
        this.setContentPane(scrollPane);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        nupp.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                i++;
                try {
                    setInputFile(tekstikast.getText());
                    System.out.println(getInputFile());
                    panel.repaint();
                    Main.algus();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SmapsViga smapsViga) {
                    smapsViga.printStackTrace();
                }

                lisaEkraanile();
                repaint();
            }
        });
        this.setVisible(true);
    }

    public void paint(Graphics g) {
        super.paint(g);

        g.drawString("Fail: "+ getInputFile(),30,120);

    }


    public static void main(String args[]) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Kuvamine();
            }
        });
    }

    public static void lisaEkraanile() {

        for (int l=0; l < getList().size(); l++) {

            JLabel myLabel = new JLabel();
            myLabel.setText(list.get(l));
            System.out.println("tekst labelile: "+list.get(l));
            myLabel.setSize(1000,20);
            myLabel.setLocation(10,(l*15)+100);
            System.out.println(myLabel.getY()+" "+list.get(l));
           /* if (myLabel.getY()>610){
                //TODO vajadusel suurenda paneeli suurust, et elemendid ara mahuksid
            }*/

            panel.add(myLabel);
            panel.revalidate();
            panel.repaint();

        }




        return;
    }



    public static void tukeldaLabeliteks(String s) {
        String [] osad = s.split("\r\n");

        for (int k =0; k<osad.length; k++){
            System.out.println("Tegi osadeks: "+osad[k]);
            getList().add(osad[k]);

        }
        lisaEkraanile();

    }

    private static void lisaScroll() {
    }


}