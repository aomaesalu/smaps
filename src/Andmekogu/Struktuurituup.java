package Andmekogu;

public enum Struktuurituup {
    PUNKT("point"), OBJEKT("object"), JOON("line"), ALA("area"), KAART("map");

    private String vaartus;

    private Struktuurituup(String vaartus) {
        this.vaartus = vaartus;
    }
}