package Andmekogu;

public enum Muutujatuup {
    INT("int"), FLOAT("float"), CRD("crd"), COORDINATE("coordinate"), BOOL("bool"), STRING("string");

    private String vaartus;

    private Muutujatuup(String vaartus) {
        this.vaartus = vaartus;
    }
}