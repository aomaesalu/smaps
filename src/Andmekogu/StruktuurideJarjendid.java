package Andmekogu;

import Struktuur.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StruktuurideJarjendid {
    private HashMap<String, List<Punkt>> punktid = new HashMap<String, List<Punkt>>();
    private HashMap<String, List<Objekt>> objektid = new HashMap<String, List<Objekt>>();
    private HashMap<String, List<Joon>> jooned = new HashMap<String, List<Joon>>();
    private HashMap<String, List<Ala>> alad = new HashMap<String, List<Ala>>();
    private HashMap<String, List<Kaart>> kaardid = new HashMap<String, List<Kaart>>();

    public void addPunktideJarjend(String muutuja) {
        punktid.put(muutuja, new ArrayList<Punkt>());
    }

    public void addObjektideJarjend(String muutuja) {
        objektid.put(muutuja, new ArrayList<Objekt>());
    }

    public void addJoonteJarjend(String muutuja) {
        jooned.put(muutuja, new ArrayList<Joon>());
    }

    public void addAladeJarjend(String muutuja) {
        alad.put(muutuja, new ArrayList<Ala>());
    }

    public void addKaartideJarjend(String muutuja) {
        kaardid.put(muutuja, new ArrayList<Kaart>());
    }

    public List<Punkt> getPunktideJarjend(String muutuja) {
        return punktid.get(muutuja);
    }

    public List<Objekt> getObjektideJarjend(String muutuja) {
        return objektid.get(muutuja);
    }

    public List<Joon> getJoonteJarjend(String muutuja) {
        return jooned.get(muutuja);
    }

    public List<Ala> getAladeJarjend(String muutuja) {
        return alad.get(muutuja);
    }

    public List<Kaart> getKaartideJarjend(String muutuja) {
        return kaardid.get(muutuja);
    }

    public void addPunkt(String muutuja, Punkt sisend) {
        punktid.get(muutuja).add(sisend);
    }

    public void addObjekt(String muutuja, Objekt sisend) {
        objektid.get(muutuja).add(sisend);
    }

    public void addJoon(String muutuja, Joon sisend) {
        jooned.get(muutuja).add(sisend);
    }

    public void addAla(String muutuja, Ala sisend) {
        alad.get(muutuja).add(sisend);
    }

    public void addKaart(String muutuja, Kaart sisend) {
        kaardid.get(muutuja).add(sisend);
    }

    public HashMap<String, List<Punkt>> getPunktid() {
        return punktid;
    }

    public void setPunktid(HashMap<String, List<Punkt>> punktid) {
        this.punktid = punktid;
    }

    public HashMap<String, List<Objekt>> getObjektid() {
        return objektid;
    }

    public void setObjektid(HashMap<String, List<Objekt>> objektid) {
        this.objektid = objektid;
    }

    public HashMap<String, List<Joon>> getJooned() {
        return jooned;
    }

    public void setJooned(HashMap<String, List<Joon>> jooned) {
        this.jooned = jooned;
    }

    public HashMap<String, List<Ala>> getAlad() {
        return alad;
    }

    public void setAlad(HashMap<String, List<Ala>> alad) {
        this.alad = alad;
    }

    public HashMap<String, List<Kaart>> getKaardid() {
        return kaardid;
    }

    public void setKaardid(HashMap<String, List<Kaart>> kaardid) {
        this.kaardid = kaardid;
    }
}
