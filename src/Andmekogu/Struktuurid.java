package Andmekogu;
import Struktuur.*;
import java.util.HashMap;

public class Struktuurid {
    private HashMap<String, Punkt> punktid = new HashMap<String, Punkt>();
    private HashMap<String, Objekt> objektid = new HashMap<String, Objekt>();
    private HashMap<String, Joon> jooned = new HashMap<String, Joon>();
    private HashMap<String, Ala> alad = new HashMap<String, Ala>();
    private HashMap<String, Kaart> kaardid = new HashMap<String, Kaart>();

    public Struktuurid() {

    }
    
    public void addPunkt(String muutuja, Punkt vaartus) {
        punktid.put(muutuja, vaartus);
    }
    
    public Punkt getPunkt(String muutuja) {
        return punktid.get(muutuja);
    }
    
    public void addObjekt(String muutuja, Objekt vaartus) {
        objektid.put(muutuja, vaartus);
    }
    
    public Objekt getObjekt(String muutuja) {
        return objektid.get(muutuja);
    }
    
    public void addJoon(String muutuja, Joon vaartus) {
        jooned.put(muutuja, vaartus);
    }
    
    public Joon getJoon(String muutuja) {
        return jooned.get(muutuja);
    }
    
    public void addAla(String muutuja, Ala vaartus) {
        alad.put(muutuja, vaartus);
    }
    
    public Ala getAla(String muutuja) {
        return alad.get(muutuja);
    }

    public void addKaart(String muutuja, Kaart vaartus) {
        kaardid.put(muutuja, vaartus);
    }

    public Kaart getKaart(String muutuja) {
        return kaardid.get(muutuja);
    }

    public HashMap<String, Punkt> getPunktid() {
        return punktid;
    }

    public void setPunktid(HashMap<String, Punkt> punktid) {
        this.punktid = punktid;
    }

    public HashMap<String, Objekt> getObjektid() {
        return objektid;
    }

    public void setObjektid(HashMap<String, Objekt> objektid) {
        this.objektid = objektid;
    }

    public HashMap<String, Joon> getJooned() {
        return jooned;
    }

    public void setJooned(HashMap<String, Joon> jooned) {
        this.jooned = jooned;
    }

    public HashMap<String, Ala> getAlad() {
        return alad;
    }

    public void setAlad(HashMap<String, Ala> alad) {
        this.alad = alad;
    }

    public HashMap<String, Kaart> getKaardid() {
        return kaardid;
    }

    public void setKaardid(HashMap<String, Kaart> kaardid) {
        this.kaardid = kaardid;
    }
}
