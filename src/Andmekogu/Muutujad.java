package Andmekogu;

import Struktuur.Koordinaat;
import java.util.HashMap;

public class Muutujad {
    private HashMap<String, Integer> taisarvud = new HashMap<String, Integer>();
    private HashMap<String, Float> ujupunktarvud = new HashMap<String, Float>();
    private HashMap<String, Koordinaat> koordinaadid = new HashMap<String, Koordinaat>();
    private HashMap<String, Boolean> toevaartused = new HashMap<String, Boolean>();
    private HashMap<String, String> soned = new HashMap<String, String>();

    public Muutujad() {

    }

    public void addTaisarv(String muutuja, Integer vaartus) {
        taisarvud.put(muutuja, vaartus);
    }

    public Integer getTaisarv(String muutuja) {
        return taisarvud.get(muutuja);
    }

    public void addUjupunktarv(String muutuja, Float vaartus) {
        ujupunktarvud.put(muutuja, vaartus);
    }

    public Float getUjupunktarv(String muutuja) {
        return ujupunktarvud.get(muutuja);
    }

    public void addKoordinaat(String muutuja, Koordinaat vaartus) {
        koordinaadid.put(muutuja, vaartus);
    }

    public Koordinaat getKoordinaat(String muutuja) {
        return koordinaadid.get(muutuja);
    }

    public void addToevaartus(String muutuja, Boolean vaartus) {
        toevaartused.put(muutuja, vaartus);
    }

    public Boolean getToevaartus(String muutuja) {
        return toevaartused.get(muutuja);
    }

    public void addSone(String muutuja, String vaartus) {
        soned.put(muutuja, vaartus);
    }

    public String getSone(String muutuja) {
        return soned.get(muutuja);
    }

    public HashMap<String, Integer> getTaisarvud() {
        return taisarvud;
    }

    public void setTaisarvud(HashMap<String, Integer> taisarvud) {
        this.taisarvud = taisarvud;
    }

    public HashMap<String, Float> getUjupunktarvud() {
        return ujupunktarvud;
    }

    public void setUjupunktarvud(HashMap<String, Float> ujupunktarvud) {
        this.ujupunktarvud = ujupunktarvud;
    }

    public HashMap<String, Koordinaat> getKoordinaadid() {
        return koordinaadid;
    }

    public void setKoordinaadid(HashMap<String, Koordinaat> koordinaadid) {
        this.koordinaadid = koordinaadid;
    }

    public HashMap<String, Boolean> getToevaartused() {
        return toevaartused;
    }

    public void setToevaartused(HashMap<String, Boolean> toevaartused) {
        this.toevaartused = toevaartused;
    }

    public HashMap<String, String> getSoned() {
        return soned;
    }

    public void setSoned(HashMap<String, String> soned) {
        this.soned = soned;
    }
}
