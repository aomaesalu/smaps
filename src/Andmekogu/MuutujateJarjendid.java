package Andmekogu;

import Struktuur.Koordinaat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MuutujateJarjendid {
    private HashMap<String, List<Integer>> taisarvud = new HashMap<String, List<Integer>>();
    private HashMap<String, List<Float>> ujupunktarvud = new HashMap<String, List<Float>>();
    private HashMap<String, List<Koordinaat>> koordinaadid = new HashMap<String, List<Koordinaat>>();
    private HashMap<String, List<Boolean>> toevaartused = new HashMap<String, List<Boolean>>();
    private HashMap<String, List<String>> soned = new HashMap<String, List<String>>();

    public MuutujateJarjendid() {

    }

    public void addTaisarvudeJarjend(String muutuja) {
        taisarvud.put(muutuja, new ArrayList<Integer>());
    }

    public void addUjupunktarvudeJarjend(String muutuja) {
        taisarvud.put(muutuja, new ArrayList<Integer>());
    }

    public void addKoordinaatideJarjend(String muutuja) {
        taisarvud.put(muutuja, new ArrayList<Integer>());
    }

    public void addToevaartusteJarjend(String muutuja) {
        taisarvud.put(muutuja, new ArrayList<Integer>());
    }

    public void addSonedeJarjend(String muutuja) {
        taisarvud.put(muutuja, new ArrayList<Integer>());
    }

    public List<Integer> getTaisarvudeJarjend(String muutuja) {
        return taisarvud.get(muutuja);
    }

    public List<Integer> getUjupunktarvudeJarjend(String muutuja) {
        return taisarvud.get(muutuja);
    }

    public List<Integer> getKoordinaatideJarjend(String muutuja) {
        return taisarvud.get(muutuja);
    }

    public List<Integer> getToevaartusteJarjend(String muutuja) {
        return taisarvud.get(muutuja);
    }

    public List<Integer> getSonedeJarjend(String muutuja) {
        return taisarvud.get(muutuja);
    }

    public void addTaisarv(String muutuja, Integer sisend) {
        taisarvud.get(muutuja).add(sisend);
    }

    public void addUjupunktarv(String muutuja, Float sisend) {
        ujupunktarvud.get(muutuja).add(sisend);
    }

    public void addKoordinaat(String muutuja, Koordinaat sisend) {
        koordinaadid.get(muutuja).add(sisend);
    }

    public void addToevaartus(String muutuja, Boolean sisend) {
        toevaartused.get(muutuja).add(sisend);
    }

    public void addSone(String muutuja, String sisend) {
        soned.get(muutuja).add(sisend);
    }

    public HashMap<String, List<Integer>> getTaisarvud() {
        return taisarvud;
    }

    public void setTaisarvud(HashMap<String, List<Integer>> taisarvud) {
        this.taisarvud = taisarvud;
    }

    public HashMap<String, List<Float>> getUjupunktarvud() {
        return ujupunktarvud;
    }

    public void setUjupunktarvud(HashMap<String, List<Float>> ujupunktarvud) {
        this.ujupunktarvud = ujupunktarvud;
    }

    public HashMap<String, List<Koordinaat>> getKoordinaadid() {
        return koordinaadid;
    }

    public void setKoordinaadid(HashMap<String, List<Koordinaat>> koordinaadid) {
        this.koordinaadid = koordinaadid;
    }

    public HashMap<String, List<Boolean>> getToevaartused() {
        return toevaartused;
    }

    public void setToevaartused(HashMap<String, List<Boolean>> toevaartused) {
        this.toevaartused = toevaartused;
    }

    public HashMap<String, List<String>> getSoned() {
        return soned;
    }

    public void setSoned(HashMap<String, List<String>> soned) {
        this.soned = soned;
    }
}
