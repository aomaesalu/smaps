package Andmekogu;

public enum AritmeetilineOperaator {
    LIITMINE("+"), LAHUTAMINE("-"), KORRUTAMINE("*"), JAGAMINE("/"), MOODUL("%");

    private String vaartus;

    private AritmeetilineOperaator(String vaartus) {
        this.vaartus = vaartus;
    }
}
