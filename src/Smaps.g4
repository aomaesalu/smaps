grammar Smaps;

programm
    :   lause*
    ;

lause
    :   muutujaDeklaratsioon
    |   struktuuriDeklaratsioon
    |   omistamine
    |   valikulause
    |   korduslause
    ;

kommentaar
    :   (UherealineKommentaar | MitmerealineKommentaar) // TODO: Võimalda igale poole vahele panna
    ;

muutujaDeklaratsioon // TODO: Parandada (puus millegipärast ei tuvastata)
    :   Andmetuup MuutujaNimi ';'
    |   Andmetuup '[]' MuutujaNimi ';'
    |   Struktuurituup '[]' MuutujaNimi ';'
    ;

struktuuriDeklaratsioon
    :   'point' MuutujaNimi '{' punktiParameeter* '}'
    |   'point' MuutujaNimi '(' aritmeetilineAvaldisLaiuskoordinaat ',' aritmeetilineAvaldisPikkuskoordinaat ',' aritmeetilineAvaldisSone ');'
    |   'object' MuutujaNimi '{' objektiParameeter* '}'
    |   'line' MuutujaNimi '{' jooneParameeter* '}'
    |   'area' MuutujaNimi '{' alaParameeter* '}'
    |   'map' MuutujaNimi '{' kaardiParameeter* '}'
    ;

omistamine
    :   ('int')? MuutujaNimi '=' aritmeetilineAvaldisArv ';'
    |   ('float')? MuutujaNimi '=' aritmeetilineAvaldisArv ';'
    |   ('crd' | 'coordinate')? MuutujaNimi '=' (aritmeetilineAvaldisLaiuskoordinaat|aritmeetilineAvaldisPikkuskoordinaat) ';'
    |   ('bool' | 'boolean')? MuutujaNimi '=' vordlusAvaldis
    |   ('string')? MuutujaNimi '=' Sone ';'
    |   'int' '[]' MuutujaNimi '=' aritmeetilineAvaldisArvudeJarjend ';'
    |   'float' '[]' MuutujaNimi '=' aritmeetilineAvaldisArvudeJarjend ';'
    |   ('crd' | 'coordinate') '[]' MuutujaNimi '=' aritmeetilineAvaldisKoordinaatideJarjend ';'
    |   ('bool' | 'boolean') '[]' MuutujaNimi '=' aritmeetilineAvaldisToevaartusteJarjend ';'
    |   'string' '[]' MuutujaNimi '=' aritmeetilineAvaldisSonedeJarjend ';'
    |   'point' '[]' MuutujaNimi '=' aritmeetilineAvaldisPunktideJarjend ';'
    |   'object' '[]' MuutujaNimi '=' aritmeetilineAvaldisObjektideJarjend ';'
    |   'line' '[]' MuutujaNimi '=' aritmeetilineAvaldisJoonteJarjend ';'
    |   'area' '[]' MuutujaNimi '=' aritmeetilineAvaldisAladeJarjend ';'
    |   'map' '[]' MuutujaNimi '=' aritmeetilineAvaldisKaartideJarjend ';'
    ;
    // TODO: Lisa omistusoperaatorid +=, -=, *=, /=, %=
    // TODO: Luba järjendite sisu muuta

valikulause
    :   valikulauseIf valikulauseElseIf* valikulauseElse?
    ;

valikulauseIf
    :   'if' '(' vordlusAvaldis ')' '{' lause* '}'
    ;

valikulauseElseIf
    :   'else' valikulauseIf
    ;

valikulauseElse
    :   'else' '{' lause* '}'
    ;

korduslause
    :   'while' '(' vordlusAvaldis ')' '{' lause* '}'                                                           #KorduslauseWhile
    |   'do' '{' lause* '}' 'while' '(' vordlusAvaldis ');'                                                     #KorduslauseDoWhile
    |   'for' '(' lause* ';' vordlusAvaldis ';' lause* ')' '{' lause* '}'                                       #KorduslauseFor
    ;

vordlusAvaldis
    :   aritmeetilineAvaldisArv  ('>' | '<' | '>=' | '<=' | '==' | '!=') aritmeetilineAvaldisArv                            #VordlusAvaldisArvud
    |   aritmeetilineAvaldisLaiuskoordinaat ('>' | '<' | '>=' | '<=' | '==' | '!=') aritmeetilineAvaldisLaiuskoordinaat     #VordlusAvaldisKoordinaadid
    |   aritmeetilineAvaldisPikkuskoordinaat ('>' | '<' | '>=' | '<=' | '==' | '!=') aritmeetilineAvaldisPikkuskoordinaat   #VordlusAvaldisKoordinaadid
    |   Sone ('>' | '<' | '>=' | '<=' | '==' | '!=') Sone                                                                   #VordlusAvaldisSone
    |   aritmeetilineAvaldisPunktideJarjend ('==' | '!=') aritmeetilineAvaldisPunktideJarjend                               #VordlusAvaldisPunktideJarjend
    |   aritmeetilineAvaldisObjektideJarjend ('==' | '!=') aritmeetilineAvaldisObjektideJarjend                             #VordlusAvaldisObjektideJarjend
    |   aritmeetilineAvaldisJoonteJarjend ('==' | '!=') aritmeetilineAvaldisJoonteJarjend                                   #VordlusAvaldisJoonteJarjend
    |   aritmeetilineAvaldisAladeJarjend ('==' | '!=') aritmeetilineAvaldisAladeJarjend                                     #VordlusAvaldisAladeJarjend
    |   aritmeetilineAvaldisKaartideJarjend ('==' | '!=') aritmeetilineAvaldisKaartideJarjend                               #VordlusAvaldisKaartidedeJarjend
    |   aritmeetilineAvaldisArvudeJarjend ('==' | '!=') aritmeetilineAvaldisArvudeJarjend                                   #VordlusAvaldisArvudeJarjend
    |   aritmeetilineAvaldisKoordinaatideJarjend ('==' | '!=') aritmeetilineAvaldisKoordinaatideJarjend                     #VordlusAvaldisKoordinaatideJarjend
    |   aritmeetilineAvaldisToevaartusteJarjend ('==' | '!=') aritmeetilineAvaldisToevaartusteJarjend                       #VordlusAvaldisToevaartusteJarjend
    |   aritmeetilineAvaldisSonedeJarjend ('==' | '!=') aritmeetilineAvaldisSonedeJarjend                                   #VordlusAvaldisSonedeJarjend
    |   vordlusAvaldis ('==' | '!=') Toevaartus                                                                             #VordlusAvaldisToevaartused
    |   Toevaartus                                                                                                          #VordlusAvaldisToevaartus
    ;   // TODO: Boolean AND, OR, NOT jms

aritmeetilineAvaldisArv
    :   aritmeetilineAvaldisArv3
    ;

aritmeetilineAvaldisArv3
    :   aritmeetilineAvaldisArv3 ('+' | '-') aritmeetilineAvaldisArv2                   #ArvudeAvaldisLiitmineLahutamine
    |   aritmeetilineAvaldisArv2                                                        #ArvudeAvaldisTriviaalne3
    ;

aritmeetilineAvaldisArv2
    :   aritmeetilineAvaldisArv2 ('*' | '/' | '%') aritmeetilineAvaldisArv1             #ArvudeAvaldisKorrutamineJagamine
    |   aritmeetilineAvaldisArv1                                                        #ArvudeAvaldisTriviaalne2
    ;

aritmeetilineAvaldisArv1
    :   '-' aritmeetilineAvaldisArv1                                                    #ArvudeAvaldisUnaarneMiinus
    |   aritmeetilineAvaldisArv0                                                        #ArvudeAvaldisTriviaalne1
    ;

aritmeetilineAvaldisArv0
    :   MuutujaNimi                                                                     #ArvudeAvaldisMuutujaNimi
    |   (Taisarv | Ujupunktarv)                                                         #ArvudeAvaldisArv
    |   '(' aritmeetilineAvaldisArv ')'                                                 #ArvudeAvaldisSuluavaldis
    ;

aritmeetilineAvaldisLaiuskoordinaat
    :   aritmeetilineAvaldisLaiuskoordinaat ('+' | '-') laiuskoordinaat                 #LaiuskoordinaatideAvaldisLiitmineLahutamine
    |   laiuskoordinaat                                                                 #LaiuskoordinaatideAvaldisLaiuskoordinaat // MuutujaNimi on juba seal sees
    ;

aritmeetilineAvaldisPikkuskoordinaat
    :   aritmeetilineAvaldisPikkuskoordinaat ('+' | '-') pikkuskoordinaat               #PikkuskoordinaatideAvaldisLiitmineLahutamine
    |   pikkuskoordinaat                                                                #PikkuskoordinaatideAvaldisPikkuskoordinaat // MuutujaNimi on juba seal sees
    ;

aritmeetilineAvaldisSone
    :   MuutujaNimi                                                                     #SonedeAvaldisMuutujaNimi
    |   Sone                                                                            #SonedeAvaldisSone
    ;

aritmeetilineAvaldisPunktideJarjend
    :   aritmeetilineAvaldisPunktideJarjend ('+' | '-') punktideJarjend                 #PunktideJarjendiAvaldisLiitmineLahutamine
    |   punktideJarjend                                                                 #PunktideJarjendiAvaldisPunktideJarjend
    ;

aritmeetilineAvaldisObjektideJarjend
    :   aritmeetilineAvaldisObjektideJarjend ('+' | '-') objektideJarjend               #ObjektideJarjendiAvaldisLiitmineLahutamine
    |   objektideJarjend                                                                #ObjektideJarjendiAvaldisObjektideJarjend
    ;

aritmeetilineAvaldisJoonteJarjend
    :   aritmeetilineAvaldisJoonteJarjend ('+' | '-') joonteJarjend                     #JoonteJarjendiAvaldisLiitmineLahutamine
    |   joonteJarjend                                                                   #JoonteJarjendiAvaldisJoonteJarjend
    ;

aritmeetilineAvaldisAladeJarjend
    :   aritmeetilineAvaldisAladeJarjend ('+' | '-') aladeJarjend                       #AladeJarjendiAvaldisLiitmineLahutamine
    |   aladeJarjend                                                                    #AladeJarjendiAvaldisAladeJarjend
    ;

aritmeetilineAvaldisKaartideJarjend
    :   aritmeetilineAvaldisKaartideJarjend ('+' | '-') kaartideJarjend                 #KaartideJarjendiAvaldisLiitmineLahutamine
    |   kaartideJarjend                                                                 #KaartideJarjendiAvaldisKaartideJarjend
    ;

aritmeetilineAvaldisArvudeJarjend
    :   aritmeetilineAvaldisArvudeJarjend ('+' | '-') arvudeJarjend                     #ArvudeJarjendiAvaldisLiitmineLahutamine
    |   arvudeJarjend                                                                   #ArvudeJarjendiAvaldisArvudeJarjend
    ;

aritmeetilineAvaldisKoordinaatideJarjend
    :   aritmeetilineAvaldisKoordinaatideJarjend ('+' | '-') koordinaatideJarjend       #KoordinaatideJarjendiAvaldisLiitmineLahutamine
    |   koordinaatideJarjend                                                            #KoordinaatideJarjendiAvaldisKoordinaatideJarjend
    ;

aritmeetilineAvaldisToevaartusteJarjend
    :   aritmeetilineAvaldisToevaartusteJarjend ('+' | '-') toevaartusteJarjend         #ToevaartusteJarjendiAvaldisLiitmineLahutamine
    |   toevaartusteJarjend                                                             #ToevaartusteJarjendiAvaldisToevaartusteJarjend
    ;

aritmeetilineAvaldisSonedeJarjend
    :   aritmeetilineAvaldisSonedeJarjend ('+' | '-') sonedeJarjend                     #SonedeJarjendiAvaldisLiitmineLahutamine
    |   sonedeJarjend                                                                   #SonedeJarjendiAvaldisSonedeJarjend
    ;

punktiParameeter
    :   ('crds' | 'coordinates') ':' punkt ';'                                          #PunktiParameeterKoordinaadid
    |   'name:' sone ';'                                                                #PunktiParameeterNimi
    |   'display:' KuvaVaartus ';'                                                      #PunktiParameeterKuva
    ;

objektiParameeter
    :   ('crds' | 'coordinates') ':' punkt ';'                                          #ObjektiParameeterKoordinaadid
    |   ('dim' | 'dimensions') ':' arvudeJarjend ';' /* TODO: Mille alusel määrata? */  #ObjektiParameeterMootmed
    |   'name:' sone ';'                                                                #ObjektiParameeterNimi
    |   'type:' ObjektiTuup ';'                                                         #ObjektiParameeterTuup
    |   'display:' KuvaVaartus ';'                                                      #ObjektiParameeterKuva
    ;

jooneParameeter
    :   ('crds' | 'coordinates') ':' punktideJarjend ';'                                #JooneParameeterKoordinaadid
    |   'name:' sone ';'                                                                #JooneParameeterNimi
    |   'type:' JooneTuup ';'                                                           #JooneParameeterTuup
    |   'display:' KuvaVaartus ';'                                                      #JooneParameeterKuva
    ;

alaParameeter
    :   ('crds' | 'coordinates') ':' punktideJarjend ';'                                #AlaParameeterKoordinaadid
    |   'name:' sone ';'                                                                #AlaParameeterNimi
    |   'type:' AlaTuup ';'                                                             #AlaParameeterTuup
    |   'display:' KuvaVaartus ';'                                                      #AlaParameeterKuva
    ;

kaardiParameeter
    :   'name:' sone ';'                                                                                    #KaardiParameeterNimi
    |   'type:' KaardiTuup ';'                                                                              #KaardiParameeterTuup
    |   'points:' aritmeetilineAvaldisPunktideJarjend ';'                                                   #KaardiParameeterPunktid
    |   'object:' aritmeetilineAvaldisObjektideJarjend ';'                                                  #KaardiParameeterObjektid
    |   'lines:' aritmeetilineAvaldisJoonteJarjend ';'                                                      #KaardiParameeterJooned
    |   'areas:' aritmeetilineAvaldisAladeJarjend ';'                                                       #KaardiParameeterAlad
    |   'maps:' aritmeetilineAvaldisKaartideJarjend ';'                                                     #KaardiParameeterKaardid
    ;

arvudeJarjend
    :   '{' aritmeetilineAvaldisArv (',' aritmeetilineAvaldisArv)* '}'
    |   MuutujaNimi
    ;

koordinaatideJarjend
    :   '{' aritmeetilineAvaldisLaiuskoordinaat (',' aritmeetilineAvaldisLaiuskoordinaat)* '}'
    |   '{' aritmeetilineAvaldisPikkuskoordinaat (',' aritmeetilineAvaldisPikkuskoordinaat)* '}'
    |   MuutujaNimi
    ;

toevaartusteJarjend
    :   '{' vordlusAvaldis (',' vordlusAvaldis)* '}'
    |   MuutujaNimi
    ;

sonedeJarjend
    :   '{' aritmeetilineAvaldisSone (',' aritmeetilineAvaldisSone)* '}'
    |   MuutujaNimi
    ;

struktuurideJarjend
    :   '{' MuutujaNimi (',' MuutujaNimi)* '}' // Kunagi hiljem nimeruumipõhisena
    |   MuutujaNimi
    ;

punktideJarjend
    :   '{' punkt (',' punkt)* '}'                                      #PunktideJarjendJarjend
    |   MuutujaNimi                                                     #PunktideJarjendMuutujaNimi
    ;

objektideJarjend
    :   '{' MuutujaNimi (',' MuutujaNimi)* '}'                          #ObjektideJarjendJarjend
    |   MuutujaNimi                                                     #ObjektideJarjendMuutujaNimi
    ;

joonteJarjend
    :   '{' MuutujaNimi (',' MuutujaNimi)* '}'                          #JoonteJarjendJarjend
    |   MuutujaNimi                                                     #JoonteJarjendMuutujaNimi
    ;

aladeJarjend
    :   '{' MuutujaNimi (',' MuutujaNimi)* '}'                          #AladeJarjendJarjend
    |   MuutujaNimi                                                     #AladeJarjendMuutujaNimi
    ;

kaartideJarjend
    :   '{' MuutujaNimi (',' MuutujaNimi)* '}'                          #KaartideJarjendJarjend
    |   MuutujaNimi                                                     #KaartideJarjendMuutujaNimi
    ;

punktiKoordinaadid
    :   punkt                                                           #PunktiKoordinaadidPunkt
    |   MuutujaNimi ('.' ('crds' | 'coordinates'))?                     #PunktiKoordinaadidMuutujaNimi
    ;

sone
    :   MuutujaNimi                                                     #SoneMuutujaNimi
    |   Sone                                                            #SoneSone
    ;

punkt
    :   '{' laiuskoordinaat ',' pikkuskoordinaat '}'                    #PunktKoordinaadid
    |   MuutujaNimi                                                     #PunktMuutujaNimi
    ;

laiuskoordinaat
    :   laiuskraad                                                      #LaiuskoordinaatLaiuskraad
    |   MuutujaNimi ('.' 'x')?                                          #LaiuskoordinaatMuutujaNimi
    ;

pikkuskoordinaat
    :   laiuskraad                                                      #PikkuskoordinaatPikkuskraad
    |   MuutujaNimi ('.' 'x')?                                          #PikkuskoordinaatMuutujaNimi
    ;

laiuskraad
    :   laiuskraadiTahis Ujupunktarv                            #Laiuskraad1
    |   laiuskraadiTahis Taisarv                                #Laiuskraad2
    |   Ujupunktarv laiuskraadiTahis                            #Laiuskraad3
    |   Taisarv laiuskraadiTahis                                #Laiuskraad4
    ;

pikkuskraad
    :   pikkuskraadiTahis Ujupunktarv                           #Pikkuskraad1
    |   pikkuskraadiTahis Taisarv                               #Pikkuskraad2
    |   Ujupunktarv pikkuskraadiTahis                           #Pikkuskraad3
    |   Taisarv pikkuskraadiTahis                               #Pikkuskraad4
    ;

laiuskraadiTahis
    :   Pohi                                                    #LaiuskraadPohi
    |   Louna                                                   #LaiuskraadLouna
    ;

pikkuskraadiTahis
    :   Ida                                                     #PikkuskraadIda
    |   Laas                                                    #PikkuskraadLaas
    ;

Andmetuup
    :   'int'
    |   'float'
    |   'crd' | 'coordinate'
    |   'bool' | 'boolean'
    |   'string'
    ;

Struktuurituup
    :   'point'
    |   'object'
    |   'line'
    |   'area'
    |   'map'
    ;

Pohi
    :   'N'
    ;

Louna
    :   'S'
    ;

Ida
    :   'E'
    ;

Laas
    :   'W'
    ;

MuutujaNimi
    :   [a-zA-Z_][a-zA-Z0-9_]*
    ;

Taisarv
    :   [-+]?[0-9]+
    ;

Ujupunktarv
    :   [-+]?[0-9]*'.'?[0-9]+
    ;

Toevaartus
    :   'True' | 'true'
    |   'False' | 'false'
    ;

Sone
    :   '"' [a-zA-Z0-9_ÕÄÖÜõäöü]* '"'
    ;

ObjektiTuup
    :   'building'
    |   'house'
    |   'monument'
    ;   // TODO: Loo uusi kirjeldusi

JooneTuup
    :   'street'
    |   'highway'
    |   'sidewalk'
    |   'path'
    |   'birds-eye'
    ;   // TODO: Loo uusi kirjeldusi

AlaTuup
    :   'state'
    |   'district'
    |   'range'
    ;   // TODO: Loo uusi kirjeldusi

KaardiTuup
    :   'geographical'
    ;   // TODO: Loo uusi kirjeldusi

KuvaVaartus
    :   'show'
    |   'hidden'
    ;

UherealineKommentaar
    :   '//' ~('\r' | '\n')* -> skip
    ;

MitmerealineKommentaar
    :   '/*' .*? '*/' -> skip
    ;

Whitespace
    :   [ \t\r\n]+ -> skip // TODO: Kontrolli, et tühemikke õigesti protsessitaks
    ;