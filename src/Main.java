import Andmekogu.*;
import Struktuur.*;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

// TODO: Eralda evaluaator peaklassist (kaotab muuhulgas ka static'ud)

public class Main {
    private static Muutujad muutujad;
    private static Struktuurid struktuurid;
    private static MuutujateJarjendid muutujaJarjendid;
    private static StruktuurideJarjendid struktuurideJarjendid;

    public static void main(String[] args) throws IOException, SmapsViga {

       String inputFile = Kuvamine.getInputFile();
        System.out.println(Kuvamine.getInputFile());
       // String inputFile = "src/nt.smaps";
        System.out.println(inputFile);
        if (args.length > 0) {
            inputFile = args[0];
        }
        InputStream is = System.in;
        if (inputFile != null) {
            is = new FileInputStream(inputFile);
        }
        //String is = "crd TartuLaiuskoordinaat = 58N;";
        ANTLRInputStream input = new ANTLRInputStream(is);
        System.out.println(input);
        Kuvamine.getList().add(input.toString());
        SmapsLexer lexer = new SmapsLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SmapsParser parser = new SmapsParser(tokens);
        ParseTree tree = parser.programm();
        System.out.println(tree.toStringTree(parser));
        Kuvamine.getList().add(tree.toStringTree(parser).toString());
        evaluate(tree);
    }

    public static void algus() throws IOException, SmapsViga {
        String inputFile = Kuvamine.getInputFile();
        System.out.println(Kuvamine.getInputFile());
        //String is = "crd TartuLaiuskoordinaat = 58N;";
        ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(inputFile));
        System.out.println(input);
        Kuvamine.tukeldaLabeliteks(input.toString());
        System.out.println("tagasi");
       /* SmapsLexer lexer = new SmapsLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SmapsParser parser = new SmapsParser(tokens);
        ParseTree tree = parser.programm();
        System.out.println(tree.toStringTree(parser));
        Kuvamine.getList().add(tree.toStringTree(parser).toString());
        evaluate(tree);*/

    }

    private static void evaluate(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.ProgrammContext) {
            for (int i = 0; i < tree.getChildCount(); i++) {
                evaluateLause(tree.getChild(i));
            }
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static void evaluateLause(ParseTree tree) throws SmapsViga {
        tree = tree.getChild(0);
        if (tree instanceof SmapsParser.MuutujaDeklaratsioonContext){
            evaluateMuutujaDeklaratsioon(tree);
        } else if (tree instanceof SmapsParser.StruktuuriDeklaratsioonContext){
            evaluateStruktuuriDeklaratsioon(tree);
        } else if (tree instanceof SmapsParser.OmistamineContext) {
            evaluateOmistamine(tree);
        } else if (tree instanceof SmapsParser.ValikulauseContext) {
            evaluateValikulause(tree);
        } else if (tree instanceof SmapsParser.KorduslauseContext) {
            evaluateKorduslause(tree);
        } else {
            throw new SmapsViga(tree);
        }
    }

    // TODO: Juba grammatikas ei tuvastata õigesti - tuleb parandada
    private static void evaluateMuutujaDeklaratsioon(ParseTree tree) {

    }

    private static void evaluateStruktuuriDeklaratsioon(ParseTree tree) throws SmapsViga {
        System.out.println("Lapsi deklaratsioonis: " + tree.getChildCount());
        Kuvamine.getList().add("Lapsi deklaratsioonis: " + tree.getChildCount());
        for (int i = 0; i < tree.getChildCount(); i++) {
            System.out.println("Laps " + i + ": " + tree.getChild(i));
            Kuvamine.getList().add("Laps " + i + ": " + tree.getChild(i));
        }
        String struktuurituup = tree.getChild(0).getText();
        String struktuurimuutuja = tree.getChild(1).getText();
        if (tree.getChild(2).getText().equals("(")) {
            if (Struktuurituup.valueOf(struktuurituup) == Struktuurituup.PUNKT) {
                Koordinaat laiuskraad = new Koordinaat(evaluateAritmeetilineAvaldisLaiuskoordinaat(tree.getChild(3)));
                if (tree.getChild(4).getText().equals(",")) {
                    Koordinaat pikkuskraad = new Koordinaat(evaluateAritmeetilineAvaldisPikkuskoordinaat(tree.getChild(5)));
                    if (tree.getChild(6).getText().equals(",")) {
                        String struktuuriNimi = evaluateAritmeetilineAvaldisSone(tree.getChild(7));
                        if (tree.getChild(8).getText().equals(");")) {
                            struktuurid.addPunkt(struktuurimuutuja, new Punkt(laiuskraad, pikkuskraad, struktuuriNimi));
                        } else {
                            throw new SmapsViga(tree, "Ootasin lõpetavat sulgu ja semikoolonit, aga leidsin hoopis \"" + tree.getChild(8).getText() + "\"");
                        }
                    } else {
                        if (tree.getChild(6).getText().equals(");")) {
                            struktuurid.addPunkt(struktuurimuutuja, new Punkt(laiuskraad, pikkuskraad));
                        } else {
                            throw new SmapsViga(tree, "Ootasin koma või lõpetavat sulgu ja semikoolonit, aga leidsin hoopis \"" + tree.getChild(8).getText() + "\"");
                        }
                    }
                } else {
                    throw new SmapsViga(tree, "Ootasin koma, sain " + tree.getChild(8).getChild(0).getText());
                }
            } else {
                throw new SmapsViga(tree, "Lühideklaratsiooni saab kasutada vaid punkti kirjeldamisel");
            }
        } else if (tree.getChild(2).getText().equals("{")) {
            switch (Struktuurituup.valueOf(struktuurituup)) {
                case PUNKT:
                    struktuurid.addPunkt(struktuurimuutuja, new Punkt());
                    break;
                case OBJEKT:
                    struktuurid.addObjekt(struktuurimuutuja, new Objekt());
                    break;
                case JOON:
                    struktuurid.addJoon(struktuurimuutuja, new Joon());
                    break;
                case ALA:
                    struktuurid.addAla(struktuurimuutuja, new Ala());
                    break;
                case KAART:
                    struktuurid.addKaart(struktuurimuutuja, new Kaart());
                    break;
                default:
                    throw new SmapsViga(tree, "Ootasin sobivat struktuuritüüpi, aga leidsin hoopis \"" + struktuurituup + "\"");
            }
            for (int i = 3; i < tree.getChildCount() - 1; i++) {
                switch (Struktuurituup.valueOf(struktuurituup)) {
                    case PUNKT:
                        if (tree.getChild(i) instanceof SmapsParser.PunktiParameeterContext) {
                            evaluatePunktiParameeter(tree.getChild(i), struktuurimuutuja);
                        } else {
                            throw new SmapsViga(tree, "Ootasin sobivat punkti parameetrit, aga leidsin hoopis \"" + tree.getChild(i).getText() + "\"");
                        }
                        break;
                    case OBJEKT:
                        if (tree.getChild(i) instanceof SmapsParser.ObjektiParameeterContext) {
                            evaluateObjektiParameeter(tree.getChild(i), struktuurimuutuja);
                        } else {
                            throw new SmapsViga(tree, "Ootasin sobivat objekti parameetrit, aga leidsin hoopis \"" + tree.getChild(i).getText() + "\"");
                        }
                        break;
                    case JOON:
                        if (tree.getChild(i) instanceof SmapsParser.JooneParameeterContext) {
                            evaluateJooneParameeter(tree.getChild(i), struktuurimuutuja);
                        } else {
                            throw new SmapsViga(tree, "Ootasin sobivat joone parameetrit, aga leidsin hoopis \"" + tree.getChild(i).getText() + "\"");
                        }
                        break;
                    case ALA:
                        if (tree.getChild(i) instanceof SmapsParser.AlaParameeterContext) {
                            evaluateAlaParameeter(tree.getChild(i), struktuurimuutuja);
                        } else {
                            throw new SmapsViga(tree, "Ootasin sobivat ala parameetrit, aga leidsin hoopis \"" + tree.getChild(i).getText() + "\"");
                        }
                        break;
                    case KAART:
                        if (tree.getChild(i) instanceof SmapsParser.KaardiParameeterContext) {
                            evaluateKaardiParameeter(tree.getChild(i), struktuurimuutuja);
                        } else {
                            throw new SmapsViga(tree, "Ootasin sobivat kaardi parameetrit, aga leidsin hoopis \"" + tree.getChild(i).getText() + "\"");
                        }
                        break;
                    default:
                        throw new SmapsViga(tree, "Ootasin struktuurile sobivat parameetrit, aga leidsin hoopis \"" + tree.getChild(i).getText() + "\"");
                }
            }
            if (!tree.getChild(tree.getChildCount() - 1).getText().equals("}")) {
                throw new SmapsViga(tree, "Ootasin lõpetavat loogelist sulgu, aga leidsin hoopis \"" + tree.getChild(tree.getChildCount() - 1).getText() + "\"");
            }
        } else {
            throw new SmapsViga(tree, "Ootasin alustavat sulgu või alustavat loogelist sulgu, aga leidsin hoopis \"" + tree.getChild(2).getText() + "\"");
        }
    }

    // TODO
    private static void evaluateOmistamine(ParseTree tree) {

    }

    // TODO
    private static void evaluateValikulause(ParseTree tree) {

    }

    // TODO
    private static void evaluateKorduslause(ParseTree tree) {

    }

    private static void evaluatePunktiParameeter(ParseTree tree, String struktuurimuutuja) throws SmapsViga {
        if (tree instanceof SmapsParser.PunktiParameeterKoordinaadidContext) {
            struktuurid.getPunkt(struktuurimuutuja).setLaiuskraad(evaluatePunkt(tree.getChild(2)).getLaiuskraad());
            struktuurid.getPunkt(struktuurimuutuja).setPikkuskraad(evaluatePunkt(tree.getChild(2)).getPikkuskraad());
        } else if (tree instanceof SmapsParser.PunktiParameeterNimiContext) {
            struktuurid.getPunkt(struktuurimuutuja).setNimi(evaluateSone(tree.getChild(1)));
        } else if (tree instanceof SmapsParser.PunktiParameeterKuvaContext) {
            struktuurid.getPunkt(struktuurimuutuja).setKuva(evaluateKuva(tree.getChild(1)));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static void evaluateObjektiParameeter(ParseTree tree, String struktuurimuutuja) throws SmapsViga {
        if (tree instanceof SmapsParser.ObjektiParameeterKoordinaadidContext) {
            struktuurid.getObjekt(struktuurimuutuja).setLaiuskraad(evaluatePunkt(tree.getChild(2)).getLaiuskraad());
            struktuurid.getObjekt(struktuurimuutuja).setPikkuskraad(evaluatePunkt(tree.getChild(2)).getPikkuskraad());
        } else if (tree instanceof SmapsParser.ObjektiParameeterMootmedContext) {
            //evaluateObjektiParameeterMootmed(tree); // TODO: Kuidas mõõtmed planeerida?
        } else if (tree instanceof SmapsParser.ObjektiParameeterNimiContext) {
            struktuurid.getObjekt(struktuurimuutuja).setNimi(evaluateSone(tree.getChild(1)));
        } else if (tree instanceof SmapsParser.ObjektiParameeterTuupContext) {
            struktuurid.getObjekt(struktuurimuutuja).setTuup(evaluateObjektiParameeterTuup(tree.getChild(1)));
        } else if (tree instanceof SmapsParser.ObjektiParameeterKuvaContext) {
            struktuurid.getObjekt(struktuurimuutuja).setKuva(evaluateKuva(tree.getChild(1)));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static void evaluateJooneParameeter(ParseTree tree, String struktuurimuutuja) throws SmapsViga {
        if (tree instanceof SmapsParser.JooneParameeterKoordinaadidContext) {
            List<Punkt> punktid = evaluatePunktideJarjend(tree.getChild(2));
            struktuurid.getJoon(struktuurimuutuja).setKoordinaadid(punktid);
        } else if (tree instanceof SmapsParser.JooneParameeterNimiContext) {
            struktuurid.getJoon(struktuurimuutuja).setNimi(evaluateSone(tree.getChild(1)));
        } else if (tree instanceof SmapsParser.JooneParameeterTuupContext) {
            struktuurid.getJoon(struktuurimuutuja).setTuup(evaluateJooneParameeterTuup(tree.getChild(1)));
        } else if (tree instanceof SmapsParser.JooneParameeterKuvaContext) {
            struktuurid.getJoon(struktuurimuutuja).setKuva(evaluateKuva(tree.getChild(1)));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static void evaluateAlaParameeter(ParseTree tree, String struktuurimuutuja) throws SmapsViga {
        if (tree instanceof SmapsParser.AlaParameeterKoordinaadidContext) {
            List<Punkt> punktid = evaluatePunktideJarjend(tree.getChild(2));
            struktuurid.getAla(struktuurimuutuja).setKoordinaadid(punktid);
        } else if (tree instanceof SmapsParser.AlaParameeterNimiContext) {
            struktuurid.getAla(struktuurimuutuja).setNimi(evaluateSone(tree.getChild(1)));
        } else if (tree instanceof SmapsParser.AlaParameeterTuupContext) {
            struktuurid.getAla(struktuurimuutuja).setTuup(evaluateAlaParameeterTuup(tree.getChild(1)));
        } else if (tree instanceof SmapsParser.AlaParameeterKuvaContext) {
            struktuurid.getAla(struktuurimuutuja).setKuva(evaluateKuva(tree.getChild(1)));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static void evaluateKaardiParameeter(ParseTree tree, String struktuurimuutuja) throws SmapsViga {
        if (tree instanceof SmapsParser.KaardiParameeterNimiContext) {
            struktuurid.getKaart(struktuurimuutuja).setNimi(evaluateSone(tree.getChild(1)));
        } else if (tree instanceof SmapsParser.KaardiParameeterTuupContext) {
            struktuurid.getKaart(struktuurimuutuja).setTuup(evaluateKaardiParameeterTuup(tree.getChild(1)));
        } else if (tree instanceof SmapsParser.KaardiParameeterPunktidContext) {
            struktuurid.getKaart(struktuurimuutuja).setPunktid(evaluateAritmeetilineAvaldisPunktideJarjend(tree.getChild(2)));
        } else if (tree instanceof SmapsParser.KaardiParameeterObjektidContext) {
            struktuurid.getKaart(struktuurimuutuja).setObjektid(evaluateAritmeetilineAvaldisObjektideJarjend(tree.getChild(2)));
        } else if (tree instanceof SmapsParser.KaardiParameeterJoonedContext) {
            struktuurid.getKaart(struktuurimuutuja).setJooned(evaluateAritmeetilineAvaldisJoonteJarjend(tree.getChild(2)));
        } else if (tree instanceof SmapsParser.KaardiParameeterAladContext) {
            struktuurid.getKaart(struktuurimuutuja).setAlad(evaluateAritmeetilineAvaldisAladeJarjend(tree.getChild(2)));
        } else if (tree instanceof SmapsParser.KaardiParameeterKaardidContext) {
            struktuurid.getKaart(struktuurimuutuja).setKaardid(evaluateAritmeetilineAvaldisKaartideJarjend(tree.getChild(2)));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static List<Punkt> evaluateAritmeetilineAvaldisPunktideJarjend(ParseTree tree) throws SmapsViga {
        List<Punkt> punktid;
        if (tree instanceof SmapsParser.PunktideJarjendiAvaldisLiitmineLahutamineContext) {
            switch (AritmeetilineOperaator.valueOf(tree.getChild(1).getText())) {
                case LIITMINE:
                    punktid = evaluateAritmeetilineAvaldisPunktideJarjend(tree.getChild(0));
                    punktid.addAll(evaluatePunktideJarjend(tree.getChild(2)));
                    break;
                case LAHUTAMINE:
                    punktid = evaluateAritmeetilineAvaldisPunktideJarjend(tree.getChild(0));
                    // TODO: Lahutamine!
                    break;
                default:
                    throw new SmapsViga(tree);
            }
            return punktid;
        } else if (tree instanceof SmapsParser.PunktideJarjendiAvaldisPunktideJarjendContext) {
            punktid = evaluatePunktideJarjend(tree.getChild(0));
            return punktid;
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static List<Objekt> evaluateAritmeetilineAvaldisObjektideJarjend(ParseTree tree) throws SmapsViga {
        List<Objekt> objektid;
        if (tree instanceof SmapsParser.ObjektideJarjendiAvaldisLiitmineLahutamineContext) {
            switch (AritmeetilineOperaator.valueOf(tree.getChild(1).getText())) {
                case LIITMINE:
                    objektid = evaluateAritmeetilineAvaldisObjektideJarjend(tree.getChild(0));
                    objektid.addAll(evaluateObjektideJarjend(tree.getChild(2)));
                    break;
                case LAHUTAMINE:
                    objektid = evaluateAritmeetilineAvaldisObjektideJarjend(tree.getChild(0));
                    // TODO: Lahutamine!
                    break;
                default:
                    throw new SmapsViga(tree);
            }
            return objektid;
        } else if (tree instanceof SmapsParser.ObjektideJarjendiAvaldisObjektideJarjendContext) {
            objektid = evaluateObjektideJarjend(tree.getChild(0));
            return objektid;
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static List<Joon> evaluateAritmeetilineAvaldisJoonteJarjend(ParseTree tree) throws SmapsViga {
        List<Joon> jooned;
        if (tree instanceof SmapsParser.JoonteJarjendiAvaldisLiitmineLahutamineContext) {
            switch (AritmeetilineOperaator.valueOf(tree.getChild(1).getText())) {
                case LIITMINE:
                    jooned = evaluateAritmeetilineAvaldisJoonteJarjend(tree.getChild(0));
                    jooned.addAll(evaluateJoonteJarjend(tree.getChild(2)));
                    break;
                case LAHUTAMINE:
                    jooned = evaluateAritmeetilineAvaldisJoonteJarjend(tree.getChild(0));
                    // TODO: Lahutamine!
                    break;
                default:
                    throw new SmapsViga(tree);
            }
            return jooned;
        } else if (tree instanceof SmapsParser.JoonteJarjendiAvaldisJoonteJarjendContext) {
            jooned = evaluateJoonteJarjend(tree.getChild(0));
            return jooned;
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static List<Ala> evaluateAritmeetilineAvaldisAladeJarjend(ParseTree tree) throws SmapsViga {
        List<Ala> alad;
        if (tree instanceof SmapsParser.AladeJarjendiAvaldisLiitmineLahutamineContext) {
            switch (AritmeetilineOperaator.valueOf(tree.getChild(1).getText())) {
                case LIITMINE:
                    alad = evaluateAritmeetilineAvaldisAladeJarjend(tree.getChild(0));
                    alad.addAll(evaluateAladeJarjend(tree.getChild(2)));
                    break;
                case LAHUTAMINE:
                    alad = evaluateAritmeetilineAvaldisAladeJarjend(tree.getChild(0));
                    // TODO: Lahutamine!
                    break;
                default:
                    throw new SmapsViga(tree);
            }
            return alad;
        } else if (tree instanceof SmapsParser.AladeJarjendiAvaldisAladeJarjendContext) {
            alad = evaluateAladeJarjend(tree.getChild(0));
            return alad;
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static List<Kaart> evaluateAritmeetilineAvaldisKaartideJarjend(ParseTree tree) throws SmapsViga {
        List<Kaart> kaardid;
        if (tree instanceof SmapsParser.KaartideJarjendiAvaldisLiitmineLahutamineContext) {
            switch (AritmeetilineOperaator.valueOf(tree.getChild(1).getText())) {
                case LIITMINE:
                    kaardid = evaluateAritmeetilineAvaldisKaartideJarjend(tree.getChild(0));
                    kaardid.addAll(evaluateKaartideJarjend(tree.getChild(2)));
                    break;
                case LAHUTAMINE:
                    kaardid = evaluateAritmeetilineAvaldisKaartideJarjend(tree.getChild(0));
                    // TODO: Lahutamine!
                    break;
                default:
                    throw new SmapsViga(tree);
            }
            return kaardid;
        } else if (tree instanceof SmapsParser.KaartideJarjendiAvaldisKaartideJarjendContext) {
            kaardid = evaluateKaartideJarjend(tree.getChild(0));
            return kaardid;
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static ObjektiTuup evaluateObjektiParameeterTuup(ParseTree tree) throws SmapsViga {
        switch (ObjektiTuup.valueOf(tree.getChild(0).getText())) {
            case BUILDING:
                return ObjektiTuup.BUILDING;
            case HOUSE:
                return ObjektiTuup.HOUSE;
            case MONUMENT:
                return ObjektiTuup.MONUMENT;
            default:
                throw new SmapsViga(tree);
        }
    }

    private static JooneTuup evaluateJooneParameeterTuup(ParseTree tree) throws SmapsViga {
        switch (JooneTuup.valueOf(tree.getChild(0).getText())) {
            case STREET:
                return JooneTuup.STREET;
            case HIGHWAY:
                return JooneTuup.HIGHWAY;
            case SIDEWALK:
                return JooneTuup.SIDEWALK;
            case PATH:
                return JooneTuup.PATH;
            case BIRDSEYE:
                return JooneTuup.BIRDSEYE;
            default:
                throw new SmapsViga(tree);
        }
    }

    private static AlaTuup evaluateAlaParameeterTuup(ParseTree tree) throws SmapsViga {
        switch (AlaTuup.valueOf(tree.getChild(0).getText())) {
            case STATE:
                return AlaTuup.STATE;
            case DISTRICT:
                return AlaTuup.DISTRICT;
            case AREA:
                return AlaTuup.AREA;
            default:
                throw new SmapsViga(tree);
        }
    }

    private static KaardiTuup evaluateKaardiParameeterTuup(ParseTree tree) throws SmapsViga {
        switch (KaardiTuup.valueOf(tree.getChild(0).getText())) {
            case GEOGRAPHICAL:
                return KaardiTuup.GEOGRAPHICAL;
            default:
                throw new SmapsViga(tree);
        }
    }

    private static Kuva evaluateKuva(ParseTree tree) throws SmapsViga {
        switch (Kuva.valueOf(tree.getChild(0).getText())) {
            case KUVA:
                return Kuva.KUVA;
            case PEIDA:
                return Kuva.PEIDA;
            default:
                throw new SmapsViga(tree);
        }
    }

    private static List<Punkt> evaluatePunktideJarjend(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.PunktideJarjendJarjendContext) {
            List<Punkt> punktid = new ArrayList<Punkt>();
            for (int i = 1; i < tree.getChildCount(); i += 2) {
                punktid.add(evaluatePunkt(tree.getChild(i)));
            }
            return punktid;
        } else if (tree instanceof SmapsParser.PunktideJarjendMuutujaNimiContext) {
            return evaluateStruktuuriNimiPunktideJarjend(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static List<Objekt> evaluateObjektideJarjend(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.ObjektideJarjendJarjendContext) {
            List<Objekt> objektid = new ArrayList<Objekt>();
            for (int i = 1; i < tree.getChildCount(); i += 2) {
                objektid.addAll(evaluateStruktuuriNimiObjektideJarjend(tree.getChild(i)));
            }
            return objektid;
        } else if (tree instanceof SmapsParser.ObjektideJarjendMuutujaNimiContext) {
            return evaluateStruktuuriNimiObjektideJarjend(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static List<Joon> evaluateJoonteJarjend(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.JoonteJarjendJarjendContext) {
            List<Joon> jooned = new ArrayList<Joon>();
            for (int i = 1; i < tree.getChildCount(); i += 2) {
                jooned.addAll(evaluateStruktuuriNimiJoonteJarjend(tree.getChild(i)));
            }
            return jooned;
        } else if (tree instanceof SmapsParser.ObjektideJarjendMuutujaNimiContext) {
            return evaluateStruktuuriNimiJoonteJarjend(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static List<Ala> evaluateAladeJarjend(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.AladeJarjendJarjendContext) {
            List<Ala> alad = new ArrayList<Ala>();
            for (int i = 1; i < tree.getChildCount(); i += 2) {
                alad.addAll(evaluateStruktuuriNimiAladeJarjend(tree.getChild(i)));
            }
            return alad;
        } else if (tree instanceof SmapsParser.AladeJarjendMuutujaNimiContext) {
            return evaluateStruktuuriNimiAladeJarjend(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static List<Kaart> evaluateKaartideJarjend(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.KaartideJarjendJarjendContext) {
            List<Kaart> kaardid = new ArrayList<Kaart>();
            for (int i = 1; i < tree.getChildCount(); i += 2) {
                kaardid.addAll(evaluateStruktuuriNimiKaartideJarjend(tree.getChild(i)));
            }
            return kaardid;
        } else if (tree instanceof SmapsParser.KaartideJarjendMuutujaNimiContext) {
            return evaluateStruktuuriNimiKaartideJarjend(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static Punkt evaluatePunkt(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.PunktKoordinaadidContext) {
            return new Punkt(evaluateLaiuskoordinaat(tree.getChild(1)), evaluatePikkuskoordinaat(tree.getChild(3)));
        } else if (tree instanceof SmapsParser.PunktMuutujaNimiContext) {
            return evaluateStruktuuriNimiPunkt(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static Koordinaat evaluateAritmeetilineAvaldisLaiuskoordinaat(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.LaiuskoordinaatideAvaldisLiitmineLahutamineContext) {
            return evaluateLaiuskoordinaatideAvaldisLiitmineLahutamine(tree.getChild(0));
        } else if (tree instanceof SmapsParser.LaiuskoordinaatideAvaldisLaiuskoordinaatContext) {
            return evaluateLaiuskoordinaat(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static Koordinaat evaluateAritmeetilineAvaldisPikkuskoordinaat(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.PikkuskoordinaatideAvaldisLiitmineLahutamineContext) {
            return evaluatePikkuskoordinaatideAvaldisLiitmineLahutamine(tree.getChild(0));
        } else if (tree instanceof SmapsParser.PikkuskoordinaatideAvaldisPikkuskoordinaatContext) {
            return evaluatePikkuskoordinaat(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static Koordinaat evaluateLaiuskoordinaatideAvaldisLiitmineLahutamine(ParseTree tree) throws SmapsViga {
        Koordinaat esimene = evaluateAritmeetilineAvaldisLaiuskoordinaat(tree.getChild(0));
        AritmeetilineOperaator operaator = evaluateAritmeetilineOperaator(tree.getChild(1));
        Koordinaat teine = evaluateLaiuskoordinaat(tree);
        switch (operaator) {
            case LIITMINE:
                return esimene.liida(teine);
            case LAHUTAMINE:
                return esimene.lahuta(teine);
            default:
                throw new SmapsViga(tree);
        }
    }

    private static Koordinaat evaluatePikkuskoordinaatideAvaldisLiitmineLahutamine(ParseTree tree) throws SmapsViga {
        Koordinaat esimene = evaluateAritmeetilineAvaldisPikkuskoordinaat(tree.getChild(0));
        AritmeetilineOperaator operaator = evaluateAritmeetilineOperaator(tree.getChild(1));
        Koordinaat teine = evaluatePikkuskoordinaat(tree);
        switch (operaator) {
            case LIITMINE:
                return esimene.liida(teine);
            case LAHUTAMINE:
                return esimene.lahuta(teine);
            default:
                throw new SmapsViga(tree);
        }
    }

    private static Koordinaat evaluateLaiuskoordinaat(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.LaiuskoordinaatLaiuskraadContext) {
            return evaluateLaiuskraad(tree.getChild(0));
        } else if (tree instanceof SmapsParser.LaiuskoordinaatMuutujaNimiContext) {
            if (tree.getChildCount() == 3) {
                Punkt punkt = evaluateStruktuuriNimiPunkt(tree.getChild(0));
                if (tree.getChild(1).getText().equals(".")) {
                    if (tree.getChild(2).getText().equals("x")) {
                        return punkt.getLaiuskraad();
                    } else {
                        throw new SmapsViga(tree, "Ootasin \"x\", aga leidsin hoopis \"" + tree.getChild(2).getText() + "\"");
                    }
                } else {
                    throw new SmapsViga(tree, "Ootasin punkti, aga leidsin hoopis \"" + tree.getChild(1).getText() + "\"");
                }
            } else if (tree.getChildCount() == 1) {
                return evaluateMuutujaNimiKoordinaat(tree.getChild(0));
            } else {
                throw new SmapsViga(tree, "Ootasin koordinaatmuutuja nime, aga leidsin hoopis \"" + tree.getText() + "\"");
            }
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static Koordinaat evaluatePikkuskoordinaat(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.PikkuskoordinaatPikkuskraadContext) {
            return evaluatePikkuskraad(tree.getChild(0));
        } else if (tree instanceof SmapsParser.PikkuskoordinaatMuutujaNimiContext) {
            if (tree.getChildCount() == 3) {
                Punkt punkt = evaluateStruktuuriNimiPunkt(tree.getChild(0));
                if (tree.getChild(1).getText().equals(".")) {
                    if (tree.getChild(2).getText().equals("y")) {
                        return punkt.getPikkuskraad();
                    } else {
                        throw new SmapsViga(tree, "Ootasin \"y\", aga leidsin hoopis \"" + tree.getChild(2).getText() + "\"");
                    }
                } else {
                    throw new SmapsViga(tree, "Ootasin punkti, aga leidsin hoopis \"" + tree.getChild(1).getText() + "\"");
                }
            } else if (tree.getChildCount() == 1) {
                return evaluateMuutujaNimiKoordinaat(tree.getChild(0));
            } else {
                throw new SmapsViga(tree, "Ootasin koordinaatmuutuja nime, aga leidsin hoopis \"" + tree.getText() + "\"");
            }
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static Koordinaat evaluateLaiuskraad(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.Laiuskraad1Context) {
            return evaluateLaiuskraad1(tree.getChild(0));
        } else if (tree instanceof SmapsParser.Laiuskraad2Context) {
            return evaluateLaiuskraad2(tree.getChild(0));
        } else if (tree instanceof SmapsParser.Laiuskraad3Context) {
            return evaluateLaiuskraad3(tree.getChild(0));
        } else if (tree instanceof SmapsParser.Laiuskraad4Context) {
            return evaluateLaiuskraad4(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static Koordinaat evaluateLaiuskraad1(ParseTree tree) throws SmapsViga {
        return new Koordinaat(evaluateUjupunktarv(tree.getChild(1)), evaluateLaiuskraadiTahis(tree.getChild(0)));
    }

    private static Koordinaat evaluateLaiuskraad2(ParseTree tree) throws SmapsViga {
        return new Koordinaat(evaluateTaisarv(tree.getChild(1)), evaluateLaiuskraadiTahis(tree.getChild(0)));
    }

    private static Koordinaat evaluateLaiuskraad3(ParseTree tree) throws SmapsViga {
        return new Koordinaat(evaluateUjupunktarv(tree.getChild(0)), evaluateLaiuskraadiTahis(tree.getChild(1)));
    }

    private static Koordinaat evaluateLaiuskraad4(ParseTree tree) throws SmapsViga {
        return new Koordinaat(evaluateTaisarv(tree.getChild(0)), evaluateLaiuskraadiTahis(tree.getChild(1)));
    }

    private static Ilmakaar evaluateLaiuskraadiTahis(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.LaiuskraadPohiContext) {
            return evaluatePohi(tree.getChild(0));
        } else  if (tree instanceof SmapsParser.LaiuskraadLounaContext) {
            return evaluateLouna(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static Koordinaat evaluatePikkuskraad(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.Pikkuskraad1Context) {
            return evaluatePikkuskraad1(tree.getChild(0));
        } else if (tree instanceof SmapsParser.Pikkuskraad2Context) {
            return evaluatePikkuskraad2(tree.getChild(0));
        } else if (tree instanceof SmapsParser.Pikkuskraad3Context) {
            return evaluatePikkuskraad3(tree.getChild(0));
        } else if (tree instanceof SmapsParser.Pikkuskraad4Context) {
            return evaluatePikkuskraad4(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static Koordinaat evaluatePikkuskraad1(ParseTree tree) throws SmapsViga {
        return new Koordinaat(evaluateUjupunktarv(tree.getChild(1)), evaluatePikkuskraadiTahis(tree.getChild(0)));
    }

    private static Koordinaat evaluatePikkuskraad2(ParseTree tree) throws SmapsViga {
        return new Koordinaat(evaluateTaisarv(tree.getChild(1)), evaluatePikkuskraadiTahis(tree.getChild(0)));
    }

    private static Koordinaat evaluatePikkuskraad3(ParseTree tree) throws SmapsViga {
        return new Koordinaat(evaluateUjupunktarv(tree.getChild(0)), evaluatePikkuskraadiTahis(tree.getChild(1)));
    }

    private static Koordinaat evaluatePikkuskraad4(ParseTree tree) throws SmapsViga {
        return new Koordinaat(evaluateTaisarv(tree.getChild(0)), evaluatePikkuskraadiTahis(tree.getChild(1)));
    }

    private static Ilmakaar evaluatePikkuskraadiTahis(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.PikkuskraadIdaContext) {
            return evaluateIda(tree.getChild(0));
        } else  if (tree instanceof SmapsParser.PikkuskraadLaasContext) {
            return evaluateLaas(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static String evaluateAritmeetilineAvaldisSone(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.SonedeAvaldisMuutujaNimiContext) {
            return evaluateMuutujaNimiSone(tree.getChild(0));
        } else if (tree instanceof SmapsParser.SonedeAvaldisSoneContext) {
            return evaluateSoneLiteraal(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static List<Punkt> evaluateStruktuuriNimiPunktideJarjend(ParseTree tree) throws SmapsViga {
        return struktuurideJarjendid.getPunktideJarjend(tree.getChild(0).getText());
    }

    private static List<Objekt> evaluateStruktuuriNimiObjektideJarjend(ParseTree tree) throws SmapsViga {
        return struktuurideJarjendid.getObjektideJarjend(tree.getChild(0).getText());
    }

    private static List<Joon> evaluateStruktuuriNimiJoonteJarjend(ParseTree tree) throws SmapsViga {
        return struktuurideJarjendid.getJoonteJarjend(tree.getChild(0).getText());
    }

    private static List<Ala> evaluateStruktuuriNimiAladeJarjend(ParseTree tree) throws SmapsViga {
        return struktuurideJarjendid.getAladeJarjend(tree.getChild(0).getText());
    }

    private static List<Kaart> evaluateStruktuuriNimiKaartideJarjend(ParseTree tree) throws SmapsViga {
        return struktuurideJarjendid.getKaartideJarjend(tree.getChild(0).getText());
    }

    private static Koordinaat evaluateMuutujaNimiKoordinaat(ParseTree tree) throws SmapsViga {
        Koordinaat koordinaat = muutujad.getKoordinaat(tree.getChild(0).getText());
        if (koordinaat != null) {
            return koordinaat;
        } else {
            throw new SmapsViga(tree, "Koordinaati nimega \"" + tree.getChild(0).getText() + "\" pole väärtustatud");
        }
    }

    private static String evaluateMuutujaNimiSone(ParseTree tree) throws SmapsViga {
        String sone = muutujad.getSone(tree.getChild(0).getText());
        if (sone != null) {
            return sone;
        } else {
            throw new SmapsViga(tree, "Sõne nimega \"" + tree.getChild(0).getText() + "\" pole väärtustatud");
        }
    }

    private static Punkt evaluateStruktuuriNimiPunkt(ParseTree tree) throws SmapsViga {
        Punkt punkt = struktuurid.getPunkt(tree.getChild(0).getText());
        if (punkt != null) {
            return punkt;
        } else {
            throw new SmapsViga(tree, "Punkti nimega \"" + tree.getChild(0).getText() + "\" pole väärtustatud");
        }
    }

    private static Ilmakaar evaluatePohi(ParseTree tree) {
        return Ilmakaar.valueOf(tree.getChild(0).getText());
    }

    private static Ilmakaar evaluateLouna(ParseTree tree) {
        return Ilmakaar.valueOf(tree.getChild(0).getText());
    }

    private static Ilmakaar evaluateIda(ParseTree tree) {
        return Ilmakaar.valueOf(tree.getChild(0).getText());
    }

    private static Ilmakaar evaluateLaas(ParseTree tree) {
        return Ilmakaar.valueOf(tree.getChild(0).getText());
    }

    private static AritmeetilineOperaator evaluateAritmeetilineOperaator(ParseTree tree) {
        return AritmeetilineOperaator.valueOf(tree.getChild(0).getText());
    }

    private static int evaluateTaisarv(ParseTree tree) {
        return Integer.parseInt(tree.getChild(0).getText());
    }

    private static float evaluateUjupunktarv(ParseTree tree) {
        return Float.parseFloat(tree.getChild(0).getText());
    }

    private static String evaluateSone(ParseTree tree) throws SmapsViga {
        if (tree instanceof SmapsParser.SoneMuutujaNimiContext) {
            return evaluateMuutujaNimiSone(tree.getChild(0));
        } else if (tree instanceof SmapsParser.SoneSoneContext) {
            return evaluateSoneLiteraal(tree.getChild(0));
        } else {
            throw new SmapsViga(tree);
        }
    }

    private static String evaluateSoneLiteraal(ParseTree tree) {
        return tree.getChild(0).getText();
    }
}