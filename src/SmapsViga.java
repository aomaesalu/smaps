import org.antlr.v4.runtime.tree.ParseTree;

public class SmapsViga extends Exception {
    public SmapsViga() {
        super();
    }

    public SmapsViga(ParseTree tree) {
        throw new UnsupportedOperationException("Viga: Ei vasta grammatikale: " + tree);
    }

    public SmapsViga(ParseTree tree, String veateade) {
        throw new UnsupportedOperationException("Viga: " + veateade + ": " + tree);
    }
}