// Generated from C:/Users/Ants-Oskar/Documents/Bitbucket/smaps/src\Smaps.g4 by ANTLR 4.x
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SmapsParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__51=1, T__50=2, T__49=3, T__48=4, T__47=5, T__46=6, T__45=7, T__44=8, 
		T__43=9, T__42=10, T__41=11, T__40=12, T__39=13, T__38=14, T__37=15, T__36=16, 
		T__35=17, T__34=18, T__33=19, T__32=20, T__31=21, T__30=22, T__29=23, 
		T__28=24, T__27=25, T__26=26, T__25=27, T__24=28, T__23=29, T__22=30, 
		T__21=31, T__20=32, T__19=33, T__18=34, T__17=35, T__16=36, T__15=37, 
		T__14=38, T__13=39, T__12=40, T__11=41, T__10=42, T__9=43, T__8=44, T__7=45, 
		T__6=46, T__5=47, T__4=48, T__3=49, T__2=50, T__1=51, T__0=52, Andmetuup=53, 
		Struktuurituup=54, Pohi=55, Louna=56, Ida=57, Laas=58, MuutujaNimi=59, 
		Taisarv=60, Ujupunktarv=61, Toevaartus=62, Sone=63, ObjektiTuup=64, JooneTuup=65, 
		AlaTuup=66, KaardiTuup=67, KuvaVaartus=68, UherealineKommentaar=69, MitmerealineKommentaar=70, 
		Whitespace=71;
	public static final String[] tokenNames = {
		"<INVALID>", "'*'", "'line'", "'crds'", "'<'", "'!='", "'<='", "'dimensions'", 
		"'dim'", "'}'", "'boolean'", "'float'", "'areas:'", "'do'", "'%'", "'[]'", 
		"'lines:'", "'bool'", "')'", "'name:'", "'map'", "'object:'", "'object'", 
		"'='", "'maps:'", "'x'", "'coordinates'", "','", "'-'", "'while'", "':'", 
		"'('", "'if'", "'int'", "'{'", "'area'", "'crd'", "'else'", "'coordinate'", 
		"'points:'", "'.'", "'+'", "'for'", "');'", "';'", "'>'", "'string'", 
		"'/'", "'=='", "'type:'", "'>='", "'display:'", "'point'", "Andmetuup", 
		"Struktuurituup", "'N'", "'S'", "'E'", "'W'", "MuutujaNimi", "Taisarv", 
		"Ujupunktarv", "Toevaartus", "Sone", "ObjektiTuup", "JooneTuup", "AlaTuup", 
		"'geographical'", "KuvaVaartus", "UherealineKommentaar", "MitmerealineKommentaar", 
		"Whitespace"
	};
	public static final int
		RULE_programm = 0, RULE_lause = 1, RULE_kommentaar = 2, RULE_muutujaDeklaratsioon = 3, 
		RULE_struktuuriDeklaratsioon = 4, RULE_omistamine = 5, RULE_valikulause = 6, 
		RULE_valikulauseIf = 7, RULE_valikulauseElseIf = 8, RULE_valikulauseElse = 9, 
		RULE_korduslause = 10, RULE_vordlusAvaldis = 11, RULE_aritmeetilineAvaldisArv = 12, 
		RULE_aritmeetilineAvaldisArv3 = 13, RULE_aritmeetilineAvaldisArv2 = 14, 
		RULE_aritmeetilineAvaldisArv1 = 15, RULE_aritmeetilineAvaldisArv0 = 16, 
		RULE_aritmeetilineAvaldisLaiuskoordinaat = 17, RULE_aritmeetilineAvaldisPikkuskoordinaat = 18, 
		RULE_aritmeetilineAvaldisSone = 19, RULE_aritmeetilineAvaldisPunktideJarjend = 20, 
		RULE_aritmeetilineAvaldisObjektideJarjend = 21, RULE_aritmeetilineAvaldisJoonteJarjend = 22, 
		RULE_aritmeetilineAvaldisAladeJarjend = 23, RULE_aritmeetilineAvaldisKaartideJarjend = 24, 
		RULE_aritmeetilineAvaldisArvudeJarjend = 25, RULE_aritmeetilineAvaldisKoordinaatideJarjend = 26, 
		RULE_aritmeetilineAvaldisToevaartusteJarjend = 27, RULE_aritmeetilineAvaldisSonedeJarjend = 28, 
		RULE_punktiParameeter = 29, RULE_objektiParameeter = 30, RULE_jooneParameeter = 31, 
		RULE_alaParameeter = 32, RULE_kaardiParameeter = 33, RULE_arvudeJarjend = 34, 
		RULE_koordinaatideJarjend = 35, RULE_toevaartusteJarjend = 36, RULE_sonedeJarjend = 37, 
		RULE_struktuurideJarjend = 38, RULE_punktideJarjend = 39, RULE_objektideJarjend = 40, 
		RULE_joonteJarjend = 41, RULE_aladeJarjend = 42, RULE_kaartideJarjend = 43, 
		RULE_punktiKoordinaadid = 44, RULE_sone = 45, RULE_punkt = 46, RULE_laiuskoordinaat = 47, 
		RULE_pikkuskoordinaat = 48, RULE_laiuskraad = 49, RULE_pikkuskraad = 50, 
		RULE_laiuskraadiTahis = 51, RULE_pikkuskraadiTahis = 52;
	public static final String[] ruleNames = {
		"programm", "lause", "kommentaar", "muutujaDeklaratsioon", "struktuuriDeklaratsioon", 
		"omistamine", "valikulause", "valikulauseIf", "valikulauseElseIf", "valikulauseElse", 
		"korduslause", "vordlusAvaldis", "aritmeetilineAvaldisArv", "aritmeetilineAvaldisArv3", 
		"aritmeetilineAvaldisArv2", "aritmeetilineAvaldisArv1", "aritmeetilineAvaldisArv0", 
		"aritmeetilineAvaldisLaiuskoordinaat", "aritmeetilineAvaldisPikkuskoordinaat", 
		"aritmeetilineAvaldisSone", "aritmeetilineAvaldisPunktideJarjend", "aritmeetilineAvaldisObjektideJarjend", 
		"aritmeetilineAvaldisJoonteJarjend", "aritmeetilineAvaldisAladeJarjend", 
		"aritmeetilineAvaldisKaartideJarjend", "aritmeetilineAvaldisArvudeJarjend", 
		"aritmeetilineAvaldisKoordinaatideJarjend", "aritmeetilineAvaldisToevaartusteJarjend", 
		"aritmeetilineAvaldisSonedeJarjend", "punktiParameeter", "objektiParameeter", 
		"jooneParameeter", "alaParameeter", "kaardiParameeter", "arvudeJarjend", 
		"koordinaatideJarjend", "toevaartusteJarjend", "sonedeJarjend", "struktuurideJarjend", 
		"punktideJarjend", "objektideJarjend", "joonteJarjend", "aladeJarjend", 
		"kaartideJarjend", "punktiKoordinaadid", "sone", "punkt", "laiuskoordinaat", 
		"pikkuskoordinaat", "laiuskraad", "pikkuskraad", "laiuskraadiTahis", "pikkuskraadiTahis"
	};

	@Override
	public String getGrammarFileName() { return "Smaps.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SmapsParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgrammContext extends ParserRuleContext {
		public LauseContext lause(int i) {
			return getRuleContext(LauseContext.class,i);
		}
		public List<LauseContext> lause() {
			return getRuleContexts(LauseContext.class);
		}
		public ProgrammContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_programm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterProgramm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitProgramm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitProgramm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgrammContext programm() throws RecognitionException {
		ProgrammContext _localctx = new ProgrammContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_programm);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 10) | (1L << 11) | (1L << 13) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 29) | (1L << 32) | (1L << 33) | (1L << 35) | (1L << 36) | (1L << 38) | (1L << 42) | (1L << 46) | (1L << 52) | (1L << Andmetuup) | (1L << Struktuurituup) | (1L << MuutujaNimi))) != 0)) {
				{
				{
				setState(106); lause();
				}
				}
				setState(111);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LauseContext extends ParserRuleContext {
		public MuutujaDeklaratsioonContext muutujaDeklaratsioon() {
			return getRuleContext(MuutujaDeklaratsioonContext.class,0);
		}
		public KorduslauseContext korduslause() {
			return getRuleContext(KorduslauseContext.class,0);
		}
		public ValikulauseContext valikulause() {
			return getRuleContext(ValikulauseContext.class,0);
		}
		public StruktuuriDeklaratsioonContext struktuuriDeklaratsioon() {
			return getRuleContext(StruktuuriDeklaratsioonContext.class,0);
		}
		public OmistamineContext omistamine() {
			return getRuleContext(OmistamineContext.class,0);
		}
		public LauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LauseContext lause() throws RecognitionException {
		LauseContext _localctx = new LauseContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_lause);
		try {
			setState(117);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(112); muutujaDeklaratsioon();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(113); struktuuriDeklaratsioon();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(114); omistamine();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(115); valikulause();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(116); korduslause();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KommentaarContext extends ParserRuleContext {
		public TerminalNode UherealineKommentaar() { return getToken(SmapsParser.UherealineKommentaar, 0); }
		public TerminalNode MitmerealineKommentaar() { return getToken(SmapsParser.MitmerealineKommentaar, 0); }
		public KommentaarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kommentaar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKommentaar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKommentaar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKommentaar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KommentaarContext kommentaar() throws RecognitionException {
		KommentaarContext _localctx = new KommentaarContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_kommentaar);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(119);
			_la = _input.LA(1);
			if ( !(_la==UherealineKommentaar || _la==MitmerealineKommentaar) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MuutujaDeklaratsioonContext extends ParserRuleContext {
		public TerminalNode Andmetuup() { return getToken(SmapsParser.Andmetuup, 0); }
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public TerminalNode Struktuurituup() { return getToken(SmapsParser.Struktuurituup, 0); }
		public MuutujaDeklaratsioonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_muutujaDeklaratsioon; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterMuutujaDeklaratsioon(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitMuutujaDeklaratsioon(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitMuutujaDeklaratsioon(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MuutujaDeklaratsioonContext muutujaDeklaratsioon() throws RecognitionException {
		MuutujaDeklaratsioonContext _localctx = new MuutujaDeklaratsioonContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_muutujaDeklaratsioon);
		try {
			setState(132);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(121); match(Andmetuup);
				setState(122); match(MuutujaNimi);
				setState(123); match(44);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(124); match(Andmetuup);
				setState(125); match(15);
				setState(126); match(MuutujaNimi);
				setState(127); match(44);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(128); match(Struktuurituup);
				setState(129); match(15);
				setState(130); match(MuutujaNimi);
				setState(131); match(44);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StruktuuriDeklaratsioonContext extends ParserRuleContext {
		public List<KaardiParameeterContext> kaardiParameeter() {
			return getRuleContexts(KaardiParameeterContext.class);
		}
		public List<AlaParameeterContext> alaParameeter() {
			return getRuleContexts(AlaParameeterContext.class);
		}
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public List<JooneParameeterContext> jooneParameeter() {
			return getRuleContexts(JooneParameeterContext.class);
		}
		public AritmeetilineAvaldisLaiuskoordinaatContext aritmeetilineAvaldisLaiuskoordinaat() {
			return getRuleContext(AritmeetilineAvaldisLaiuskoordinaatContext.class,0);
		}
		public JooneParameeterContext jooneParameeter(int i) {
			return getRuleContext(JooneParameeterContext.class,i);
		}
		public ObjektiParameeterContext objektiParameeter(int i) {
			return getRuleContext(ObjektiParameeterContext.class,i);
		}
		public PunktiParameeterContext punktiParameeter(int i) {
			return getRuleContext(PunktiParameeterContext.class,i);
		}
		public KaardiParameeterContext kaardiParameeter(int i) {
			return getRuleContext(KaardiParameeterContext.class,i);
		}
		public AritmeetilineAvaldisPikkuskoordinaatContext aritmeetilineAvaldisPikkuskoordinaat() {
			return getRuleContext(AritmeetilineAvaldisPikkuskoordinaatContext.class,0);
		}
		public List<PunktiParameeterContext> punktiParameeter() {
			return getRuleContexts(PunktiParameeterContext.class);
		}
		public List<ObjektiParameeterContext> objektiParameeter() {
			return getRuleContexts(ObjektiParameeterContext.class);
		}
		public AritmeetilineAvaldisSoneContext aritmeetilineAvaldisSone() {
			return getRuleContext(AritmeetilineAvaldisSoneContext.class,0);
		}
		public AlaParameeterContext alaParameeter(int i) {
			return getRuleContext(AlaParameeterContext.class,i);
		}
		public StruktuuriDeklaratsioonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struktuuriDeklaratsioon; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterStruktuuriDeklaratsioon(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitStruktuuriDeklaratsioon(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitStruktuuriDeklaratsioon(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StruktuuriDeklaratsioonContext struktuuriDeklaratsioon() throws RecognitionException {
		StruktuuriDeklaratsioonContext _localctx = new StruktuuriDeklaratsioonContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_struktuuriDeklaratsioon);
		int _la;
		try {
			setState(194);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(134); match(52);
				setState(135); match(MuutujaNimi);
				setState(136); match(34);
				setState(140);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 19) | (1L << 26) | (1L << 51))) != 0)) {
					{
					{
					setState(137); punktiParameeter();
					}
					}
					setState(142);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(143); match(9);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(144); match(52);
				setState(145); match(MuutujaNimi);
				setState(146); match(31);
				setState(147); aritmeetilineAvaldisLaiuskoordinaat(0);
				setState(148); match(27);
				setState(149); aritmeetilineAvaldisPikkuskoordinaat(0);
				setState(150); match(27);
				setState(151); aritmeetilineAvaldisSone();
				setState(152); match(43);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(154); match(22);
				setState(155); match(MuutujaNimi);
				setState(156); match(34);
				setState(160);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 7) | (1L << 8) | (1L << 19) | (1L << 26) | (1L << 49) | (1L << 51))) != 0)) {
					{
					{
					setState(157); objektiParameeter();
					}
					}
					setState(162);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(163); match(9);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(164); match(2);
				setState(165); match(MuutujaNimi);
				setState(166); match(34);
				setState(170);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 19) | (1L << 26) | (1L << 49) | (1L << 51))) != 0)) {
					{
					{
					setState(167); jooneParameeter();
					}
					}
					setState(172);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(173); match(9);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(174); match(35);
				setState(175); match(MuutujaNimi);
				setState(176); match(34);
				setState(180);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 19) | (1L << 26) | (1L << 49) | (1L << 51))) != 0)) {
					{
					{
					setState(177); alaParameeter();
					}
					}
					setState(182);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(183); match(9);
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(184); match(20);
				setState(185); match(MuutujaNimi);
				setState(186); match(34);
				setState(190);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 12) | (1L << 16) | (1L << 19) | (1L << 21) | (1L << 24) | (1L << 39) | (1L << 49))) != 0)) {
					{
					{
					setState(187); kaardiParameeter();
					}
					}
					setState(192);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(193); match(9);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OmistamineContext extends ParserRuleContext {
		public AritmeetilineAvaldisAladeJarjendContext aritmeetilineAvaldisAladeJarjend() {
			return getRuleContext(AritmeetilineAvaldisAladeJarjendContext.class,0);
		}
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public AritmeetilineAvaldisArvudeJarjendContext aritmeetilineAvaldisArvudeJarjend() {
			return getRuleContext(AritmeetilineAvaldisArvudeJarjendContext.class,0);
		}
		public AritmeetilineAvaldisLaiuskoordinaatContext aritmeetilineAvaldisLaiuskoordinaat() {
			return getRuleContext(AritmeetilineAvaldisLaiuskoordinaatContext.class,0);
		}
		public AritmeetilineAvaldisToevaartusteJarjendContext aritmeetilineAvaldisToevaartusteJarjend() {
			return getRuleContext(AritmeetilineAvaldisToevaartusteJarjendContext.class,0);
		}
		public AritmeetilineAvaldisArvContext aritmeetilineAvaldisArv() {
			return getRuleContext(AritmeetilineAvaldisArvContext.class,0);
		}
		public TerminalNode Sone() { return getToken(SmapsParser.Sone, 0); }
		public AritmeetilineAvaldisObjektideJarjendContext aritmeetilineAvaldisObjektideJarjend() {
			return getRuleContext(AritmeetilineAvaldisObjektideJarjendContext.class,0);
		}
		public VordlusAvaldisContext vordlusAvaldis() {
			return getRuleContext(VordlusAvaldisContext.class,0);
		}
		public AritmeetilineAvaldisJoonteJarjendContext aritmeetilineAvaldisJoonteJarjend() {
			return getRuleContext(AritmeetilineAvaldisJoonteJarjendContext.class,0);
		}
		public AritmeetilineAvaldisPikkuskoordinaatContext aritmeetilineAvaldisPikkuskoordinaat() {
			return getRuleContext(AritmeetilineAvaldisPikkuskoordinaatContext.class,0);
		}
		public AritmeetilineAvaldisKoordinaatideJarjendContext aritmeetilineAvaldisKoordinaatideJarjend() {
			return getRuleContext(AritmeetilineAvaldisKoordinaatideJarjendContext.class,0);
		}
		public AritmeetilineAvaldisPunktideJarjendContext aritmeetilineAvaldisPunktideJarjend() {
			return getRuleContext(AritmeetilineAvaldisPunktideJarjendContext.class,0);
		}
		public AritmeetilineAvaldisKaartideJarjendContext aritmeetilineAvaldisKaartideJarjend() {
			return getRuleContext(AritmeetilineAvaldisKaartideJarjendContext.class,0);
		}
		public AritmeetilineAvaldisSonedeJarjendContext aritmeetilineAvaldisSonedeJarjend() {
			return getRuleContext(AritmeetilineAvaldisSonedeJarjendContext.class,0);
		}
		public OmistamineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_omistamine; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterOmistamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitOmistamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitOmistamine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OmistamineContext omistamine() throws RecognitionException {
		OmistamineContext _localctx = new OmistamineContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_omistamine);
		int _la;
		try {
			setState(306);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(197);
				_la = _input.LA(1);
				if (_la==33) {
					{
					setState(196); match(33);
					}
				}

				setState(199); match(MuutujaNimi);
				setState(200); match(23);
				setState(201); aritmeetilineAvaldisArv();
				setState(202); match(44);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(205);
				_la = _input.LA(1);
				if (_la==11) {
					{
					setState(204); match(11);
					}
				}

				setState(207); match(MuutujaNimi);
				setState(208); match(23);
				setState(209); aritmeetilineAvaldisArv();
				setState(210); match(44);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(213);
				_la = _input.LA(1);
				if (_la==36 || _la==38) {
					{
					setState(212);
					_la = _input.LA(1);
					if ( !(_la==36 || _la==38) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
				}

				setState(215); match(MuutujaNimi);
				setState(216); match(23);
				setState(219);
				switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
				case 1:
					{
					setState(217); aritmeetilineAvaldisLaiuskoordinaat(0);
					}
					break;

				case 2:
					{
					setState(218); aritmeetilineAvaldisPikkuskoordinaat(0);
					}
					break;
				}
				setState(221); match(44);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(224);
				_la = _input.LA(1);
				if (_la==10 || _la==17) {
					{
					setState(223);
					_la = _input.LA(1);
					if ( !(_la==10 || _la==17) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
				}

				setState(226); match(MuutujaNimi);
				setState(227); match(23);
				setState(228); vordlusAvaldis(0);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(230);
				_la = _input.LA(1);
				if (_la==46) {
					{
					setState(229); match(46);
					}
				}

				setState(232); match(MuutujaNimi);
				setState(233); match(23);
				setState(234); match(Sone);
				setState(235); match(44);
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(236); match(33);
				setState(237); match(15);
				setState(238); match(MuutujaNimi);
				setState(239); match(23);
				setState(240); aritmeetilineAvaldisArvudeJarjend(0);
				setState(241); match(44);
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(243); match(11);
				setState(244); match(15);
				setState(245); match(MuutujaNimi);
				setState(246); match(23);
				setState(247); aritmeetilineAvaldisArvudeJarjend(0);
				setState(248); match(44);
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(250);
				_la = _input.LA(1);
				if ( !(_la==36 || _la==38) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(251); match(15);
				setState(252); match(MuutujaNimi);
				setState(253); match(23);
				setState(254); aritmeetilineAvaldisKoordinaatideJarjend(0);
				setState(255); match(44);
				}
				break;

			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(257);
				_la = _input.LA(1);
				if ( !(_la==10 || _la==17) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(258); match(15);
				setState(259); match(MuutujaNimi);
				setState(260); match(23);
				setState(261); aritmeetilineAvaldisToevaartusteJarjend(0);
				setState(262); match(44);
				}
				break;

			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(264); match(46);
				setState(265); match(15);
				setState(266); match(MuutujaNimi);
				setState(267); match(23);
				setState(268); aritmeetilineAvaldisSonedeJarjend(0);
				setState(269); match(44);
				}
				break;

			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(271); match(52);
				setState(272); match(15);
				setState(273); match(MuutujaNimi);
				setState(274); match(23);
				setState(275); aritmeetilineAvaldisPunktideJarjend(0);
				setState(276); match(44);
				}
				break;

			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(278); match(22);
				setState(279); match(15);
				setState(280); match(MuutujaNimi);
				setState(281); match(23);
				setState(282); aritmeetilineAvaldisObjektideJarjend(0);
				setState(283); match(44);
				}
				break;

			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(285); match(2);
				setState(286); match(15);
				setState(287); match(MuutujaNimi);
				setState(288); match(23);
				setState(289); aritmeetilineAvaldisJoonteJarjend(0);
				setState(290); match(44);
				}
				break;

			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(292); match(35);
				setState(293); match(15);
				setState(294); match(MuutujaNimi);
				setState(295); match(23);
				setState(296); aritmeetilineAvaldisAladeJarjend(0);
				setState(297); match(44);
				}
				break;

			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(299); match(20);
				setState(300); match(15);
				setState(301); match(MuutujaNimi);
				setState(302); match(23);
				setState(303); aritmeetilineAvaldisKaartideJarjend(0);
				setState(304); match(44);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValikulauseContext extends ParserRuleContext {
		public ValikulauseElseIfContext valikulauseElseIf(int i) {
			return getRuleContext(ValikulauseElseIfContext.class,i);
		}
		public ValikulauseElseContext valikulauseElse() {
			return getRuleContext(ValikulauseElseContext.class,0);
		}
		public ValikulauseIfContext valikulauseIf() {
			return getRuleContext(ValikulauseIfContext.class,0);
		}
		public List<ValikulauseElseIfContext> valikulauseElseIf() {
			return getRuleContexts(ValikulauseElseIfContext.class);
		}
		public ValikulauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valikulause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterValikulause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitValikulause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitValikulause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValikulauseContext valikulause() throws RecognitionException {
		ValikulauseContext _localctx = new ValikulauseContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_valikulause);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(308); valikulauseIf();
			setState(312);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(309); valikulauseElseIf();
					}
					} 
				}
				setState(314);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			setState(316);
			_la = _input.LA(1);
			if (_la==37) {
				{
				setState(315); valikulauseElse();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValikulauseIfContext extends ParserRuleContext {
		public LauseContext lause(int i) {
			return getRuleContext(LauseContext.class,i);
		}
		public VordlusAvaldisContext vordlusAvaldis() {
			return getRuleContext(VordlusAvaldisContext.class,0);
		}
		public List<LauseContext> lause() {
			return getRuleContexts(LauseContext.class);
		}
		public ValikulauseIfContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valikulauseIf; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterValikulauseIf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitValikulauseIf(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitValikulauseIf(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValikulauseIfContext valikulauseIf() throws RecognitionException {
		ValikulauseIfContext _localctx = new ValikulauseIfContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_valikulauseIf);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(318); match(32);
			setState(319); match(31);
			setState(320); vordlusAvaldis(0);
			setState(321); match(18);
			setState(322); match(34);
			setState(326);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 10) | (1L << 11) | (1L << 13) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 29) | (1L << 32) | (1L << 33) | (1L << 35) | (1L << 36) | (1L << 38) | (1L << 42) | (1L << 46) | (1L << 52) | (1L << Andmetuup) | (1L << Struktuurituup) | (1L << MuutujaNimi))) != 0)) {
				{
				{
				setState(323); lause();
				}
				}
				setState(328);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(329); match(9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValikulauseElseIfContext extends ParserRuleContext {
		public ValikulauseIfContext valikulauseIf() {
			return getRuleContext(ValikulauseIfContext.class,0);
		}
		public ValikulauseElseIfContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valikulauseElseIf; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterValikulauseElseIf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitValikulauseElseIf(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitValikulauseElseIf(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValikulauseElseIfContext valikulauseElseIf() throws RecognitionException {
		ValikulauseElseIfContext _localctx = new ValikulauseElseIfContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_valikulauseElseIf);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(331); match(37);
			setState(332); valikulauseIf();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValikulauseElseContext extends ParserRuleContext {
		public LauseContext lause(int i) {
			return getRuleContext(LauseContext.class,i);
		}
		public List<LauseContext> lause() {
			return getRuleContexts(LauseContext.class);
		}
		public ValikulauseElseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valikulauseElse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterValikulauseElse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitValikulauseElse(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitValikulauseElse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValikulauseElseContext valikulauseElse() throws RecognitionException {
		ValikulauseElseContext _localctx = new ValikulauseElseContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_valikulauseElse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(334); match(37);
			setState(335); match(34);
			setState(339);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 10) | (1L << 11) | (1L << 13) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 29) | (1L << 32) | (1L << 33) | (1L << 35) | (1L << 36) | (1L << 38) | (1L << 42) | (1L << 46) | (1L << 52) | (1L << Andmetuup) | (1L << Struktuurituup) | (1L << MuutujaNimi))) != 0)) {
				{
				{
				setState(336); lause();
				}
				}
				setState(341);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(342); match(9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KorduslauseContext extends ParserRuleContext {
		public KorduslauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_korduslause; }
	 
		public KorduslauseContext() { }
		public void copyFrom(KorduslauseContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class KorduslauseForContext extends KorduslauseContext {
		public LauseContext lause(int i) {
			return getRuleContext(LauseContext.class,i);
		}
		public VordlusAvaldisContext vordlusAvaldis() {
			return getRuleContext(VordlusAvaldisContext.class,0);
		}
		public List<LauseContext> lause() {
			return getRuleContexts(LauseContext.class);
		}
		public KorduslauseForContext(KorduslauseContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKorduslauseFor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKorduslauseFor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKorduslauseFor(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KorduslauseWhileContext extends KorduslauseContext {
		public LauseContext lause(int i) {
			return getRuleContext(LauseContext.class,i);
		}
		public VordlusAvaldisContext vordlusAvaldis() {
			return getRuleContext(VordlusAvaldisContext.class,0);
		}
		public List<LauseContext> lause() {
			return getRuleContexts(LauseContext.class);
		}
		public KorduslauseWhileContext(KorduslauseContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKorduslauseWhile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKorduslauseWhile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKorduslauseWhile(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KorduslauseDoWhileContext extends KorduslauseContext {
		public LauseContext lause(int i) {
			return getRuleContext(LauseContext.class,i);
		}
		public VordlusAvaldisContext vordlusAvaldis() {
			return getRuleContext(VordlusAvaldisContext.class,0);
		}
		public List<LauseContext> lause() {
			return getRuleContexts(LauseContext.class);
		}
		public KorduslauseDoWhileContext(KorduslauseContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKorduslauseDoWhile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKorduslauseDoWhile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKorduslauseDoWhile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KorduslauseContext korduslause() throws RecognitionException {
		KorduslauseContext _localctx = new KorduslauseContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_korduslause);
		int _la;
		try {
			setState(398);
			switch (_input.LA(1)) {
			case 29:
				_localctx = new KorduslauseWhileContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(344); match(29);
				setState(345); match(31);
				setState(346); vordlusAvaldis(0);
				setState(347); match(18);
				setState(348); match(34);
				setState(352);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 10) | (1L << 11) | (1L << 13) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 29) | (1L << 32) | (1L << 33) | (1L << 35) | (1L << 36) | (1L << 38) | (1L << 42) | (1L << 46) | (1L << 52) | (1L << Andmetuup) | (1L << Struktuurituup) | (1L << MuutujaNimi))) != 0)) {
					{
					{
					setState(349); lause();
					}
					}
					setState(354);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(355); match(9);
				}
				break;
			case 13:
				_localctx = new KorduslauseDoWhileContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(357); match(13);
				setState(358); match(34);
				setState(362);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 10) | (1L << 11) | (1L << 13) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 29) | (1L << 32) | (1L << 33) | (1L << 35) | (1L << 36) | (1L << 38) | (1L << 42) | (1L << 46) | (1L << 52) | (1L << Andmetuup) | (1L << Struktuurituup) | (1L << MuutujaNimi))) != 0)) {
					{
					{
					setState(359); lause();
					}
					}
					setState(364);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(365); match(9);
				setState(366); match(29);
				setState(367); match(31);
				setState(368); vordlusAvaldis(0);
				setState(369); match(43);
				}
				break;
			case 42:
				_localctx = new KorduslauseForContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(371); match(42);
				setState(372); match(31);
				setState(376);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 10) | (1L << 11) | (1L << 13) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 29) | (1L << 32) | (1L << 33) | (1L << 35) | (1L << 36) | (1L << 38) | (1L << 42) | (1L << 46) | (1L << 52) | (1L << Andmetuup) | (1L << Struktuurituup) | (1L << MuutujaNimi))) != 0)) {
					{
					{
					setState(373); lause();
					}
					}
					setState(378);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(379); match(44);
				setState(380); vordlusAvaldis(0);
				setState(381); match(44);
				setState(385);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 10) | (1L << 11) | (1L << 13) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 29) | (1L << 32) | (1L << 33) | (1L << 35) | (1L << 36) | (1L << 38) | (1L << 42) | (1L << 46) | (1L << 52) | (1L << Andmetuup) | (1L << Struktuurituup) | (1L << MuutujaNimi))) != 0)) {
					{
					{
					setState(382); lause();
					}
					}
					setState(387);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(388); match(18);
				setState(389); match(34);
				setState(393);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 10) | (1L << 11) | (1L << 13) | (1L << 17) | (1L << 20) | (1L << 22) | (1L << 29) | (1L << 32) | (1L << 33) | (1L << 35) | (1L << 36) | (1L << 38) | (1L << 42) | (1L << 46) | (1L << 52) | (1L << Andmetuup) | (1L << Struktuurituup) | (1L << MuutujaNimi))) != 0)) {
					{
					{
					setState(390); lause();
					}
					}
					setState(395);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(396); match(9);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VordlusAvaldisContext extends ParserRuleContext {
		public VordlusAvaldisContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_vordlusAvaldis; }
	 
		public VordlusAvaldisContext() { }
		public void copyFrom(VordlusAvaldisContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class VordlusAvaldisObjektideJarjendContext extends VordlusAvaldisContext {
		public List<AritmeetilineAvaldisObjektideJarjendContext> aritmeetilineAvaldisObjektideJarjend() {
			return getRuleContexts(AritmeetilineAvaldisObjektideJarjendContext.class);
		}
		public AritmeetilineAvaldisObjektideJarjendContext aritmeetilineAvaldisObjektideJarjend(int i) {
			return getRuleContext(AritmeetilineAvaldisObjektideJarjendContext.class,i);
		}
		public VordlusAvaldisObjektideJarjendContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisObjektideJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisObjektideJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisObjektideJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisToevaartusteJarjendContext extends VordlusAvaldisContext {
		public AritmeetilineAvaldisToevaartusteJarjendContext aritmeetilineAvaldisToevaartusteJarjend(int i) {
			return getRuleContext(AritmeetilineAvaldisToevaartusteJarjendContext.class,i);
		}
		public List<AritmeetilineAvaldisToevaartusteJarjendContext> aritmeetilineAvaldisToevaartusteJarjend() {
			return getRuleContexts(AritmeetilineAvaldisToevaartusteJarjendContext.class);
		}
		public VordlusAvaldisToevaartusteJarjendContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisToevaartusteJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisToevaartusteJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisToevaartusteJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisPunktideJarjendContext extends VordlusAvaldisContext {
		public AritmeetilineAvaldisPunktideJarjendContext aritmeetilineAvaldisPunktideJarjend(int i) {
			return getRuleContext(AritmeetilineAvaldisPunktideJarjendContext.class,i);
		}
		public List<AritmeetilineAvaldisPunktideJarjendContext> aritmeetilineAvaldisPunktideJarjend() {
			return getRuleContexts(AritmeetilineAvaldisPunktideJarjendContext.class);
		}
		public VordlusAvaldisPunktideJarjendContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisPunktideJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisPunktideJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisPunktideJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisToevaartusContext extends VordlusAvaldisContext {
		public TerminalNode Toevaartus() { return getToken(SmapsParser.Toevaartus, 0); }
		public VordlusAvaldisToevaartusContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisToevaartus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisToevaartus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisToevaartus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisKoordinaatideJarjendContext extends VordlusAvaldisContext {
		public AritmeetilineAvaldisKoordinaatideJarjendContext aritmeetilineAvaldisKoordinaatideJarjend(int i) {
			return getRuleContext(AritmeetilineAvaldisKoordinaatideJarjendContext.class,i);
		}
		public List<AritmeetilineAvaldisKoordinaatideJarjendContext> aritmeetilineAvaldisKoordinaatideJarjend() {
			return getRuleContexts(AritmeetilineAvaldisKoordinaatideJarjendContext.class);
		}
		public VordlusAvaldisKoordinaatideJarjendContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisKoordinaatideJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisKoordinaatideJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisKoordinaatideJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisArvudContext extends VordlusAvaldisContext {
		public AritmeetilineAvaldisArvContext aritmeetilineAvaldisArv(int i) {
			return getRuleContext(AritmeetilineAvaldisArvContext.class,i);
		}
		public List<AritmeetilineAvaldisArvContext> aritmeetilineAvaldisArv() {
			return getRuleContexts(AritmeetilineAvaldisArvContext.class);
		}
		public VordlusAvaldisArvudContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisArvud(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisArvud(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisArvud(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisArvudeJarjendContext extends VordlusAvaldisContext {
		public AritmeetilineAvaldisArvudeJarjendContext aritmeetilineAvaldisArvudeJarjend(int i) {
			return getRuleContext(AritmeetilineAvaldisArvudeJarjendContext.class,i);
		}
		public List<AritmeetilineAvaldisArvudeJarjendContext> aritmeetilineAvaldisArvudeJarjend() {
			return getRuleContexts(AritmeetilineAvaldisArvudeJarjendContext.class);
		}
		public VordlusAvaldisArvudeJarjendContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisArvudeJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisArvudeJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisArvudeJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisSonedeJarjendContext extends VordlusAvaldisContext {
		public List<AritmeetilineAvaldisSonedeJarjendContext> aritmeetilineAvaldisSonedeJarjend() {
			return getRuleContexts(AritmeetilineAvaldisSonedeJarjendContext.class);
		}
		public AritmeetilineAvaldisSonedeJarjendContext aritmeetilineAvaldisSonedeJarjend(int i) {
			return getRuleContext(AritmeetilineAvaldisSonedeJarjendContext.class,i);
		}
		public VordlusAvaldisSonedeJarjendContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisSonedeJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisSonedeJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisSonedeJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisSoneContext extends VordlusAvaldisContext {
		public TerminalNode Sone(int i) {
			return getToken(SmapsParser.Sone, i);
		}
		public List<TerminalNode> Sone() { return getTokens(SmapsParser.Sone); }
		public VordlusAvaldisSoneContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisSone(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisSone(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisSone(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisJoonteJarjendContext extends VordlusAvaldisContext {
		public List<AritmeetilineAvaldisJoonteJarjendContext> aritmeetilineAvaldisJoonteJarjend() {
			return getRuleContexts(AritmeetilineAvaldisJoonteJarjendContext.class);
		}
		public AritmeetilineAvaldisJoonteJarjendContext aritmeetilineAvaldisJoonteJarjend(int i) {
			return getRuleContext(AritmeetilineAvaldisJoonteJarjendContext.class,i);
		}
		public VordlusAvaldisJoonteJarjendContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisJoonteJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisJoonteJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisJoonteJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisKoordinaadidContext extends VordlusAvaldisContext {
		public AritmeetilineAvaldisPikkuskoordinaatContext aritmeetilineAvaldisPikkuskoordinaat(int i) {
			return getRuleContext(AritmeetilineAvaldisPikkuskoordinaatContext.class,i);
		}
		public List<AritmeetilineAvaldisPikkuskoordinaatContext> aritmeetilineAvaldisPikkuskoordinaat() {
			return getRuleContexts(AritmeetilineAvaldisPikkuskoordinaatContext.class);
		}
		public List<AritmeetilineAvaldisLaiuskoordinaatContext> aritmeetilineAvaldisLaiuskoordinaat() {
			return getRuleContexts(AritmeetilineAvaldisLaiuskoordinaatContext.class);
		}
		public AritmeetilineAvaldisLaiuskoordinaatContext aritmeetilineAvaldisLaiuskoordinaat(int i) {
			return getRuleContext(AritmeetilineAvaldisLaiuskoordinaatContext.class,i);
		}
		public VordlusAvaldisKoordinaadidContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisKoordinaadid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisKoordinaadid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisKoordinaadid(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisAladeJarjendContext extends VordlusAvaldisContext {
		public AritmeetilineAvaldisAladeJarjendContext aritmeetilineAvaldisAladeJarjend(int i) {
			return getRuleContext(AritmeetilineAvaldisAladeJarjendContext.class,i);
		}
		public List<AritmeetilineAvaldisAladeJarjendContext> aritmeetilineAvaldisAladeJarjend() {
			return getRuleContexts(AritmeetilineAvaldisAladeJarjendContext.class);
		}
		public VordlusAvaldisAladeJarjendContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisAladeJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisAladeJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisAladeJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisToevaartusedContext extends VordlusAvaldisContext {
		public VordlusAvaldisContext vordlusAvaldis() {
			return getRuleContext(VordlusAvaldisContext.class,0);
		}
		public TerminalNode Toevaartus() { return getToken(SmapsParser.Toevaartus, 0); }
		public VordlusAvaldisToevaartusedContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisToevaartused(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisToevaartused(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisToevaartused(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VordlusAvaldisKaartidedeJarjendContext extends VordlusAvaldisContext {
		public List<AritmeetilineAvaldisKaartideJarjendContext> aritmeetilineAvaldisKaartideJarjend() {
			return getRuleContexts(AritmeetilineAvaldisKaartideJarjendContext.class);
		}
		public AritmeetilineAvaldisKaartideJarjendContext aritmeetilineAvaldisKaartideJarjend(int i) {
			return getRuleContext(AritmeetilineAvaldisKaartideJarjendContext.class,i);
		}
		public VordlusAvaldisKaartidedeJarjendContext(VordlusAvaldisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterVordlusAvaldisKaartidedeJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitVordlusAvaldisKaartidedeJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitVordlusAvaldisKaartidedeJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VordlusAvaldisContext vordlusAvaldis() throws RecognitionException {
		return vordlusAvaldis(0);
	}

	private VordlusAvaldisContext vordlusAvaldis(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		VordlusAvaldisContext _localctx = new VordlusAvaldisContext(_ctx, _parentState);
		VordlusAvaldisContext _prevctx = _localctx;
		int _startState = 22;
		enterRecursionRule(_localctx, 22, RULE_vordlusAvaldis, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(453);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				{
				_localctx = new VordlusAvaldisArvudContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(401); aritmeetilineAvaldisArv();
				setState(402);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 4) | (1L << 5) | (1L << 6) | (1L << 45) | (1L << 48) | (1L << 50))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(403); aritmeetilineAvaldisArv();
				}
				break;

			case 2:
				{
				_localctx = new VordlusAvaldisKoordinaadidContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(405); aritmeetilineAvaldisLaiuskoordinaat(0);
				setState(406);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 4) | (1L << 5) | (1L << 6) | (1L << 45) | (1L << 48) | (1L << 50))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(407); aritmeetilineAvaldisLaiuskoordinaat(0);
				}
				break;

			case 3:
				{
				_localctx = new VordlusAvaldisKoordinaadidContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(409); aritmeetilineAvaldisPikkuskoordinaat(0);
				setState(410);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 4) | (1L << 5) | (1L << 6) | (1L << 45) | (1L << 48) | (1L << 50))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(411); aritmeetilineAvaldisPikkuskoordinaat(0);
				}
				break;

			case 4:
				{
				_localctx = new VordlusAvaldisSoneContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(413); match(Sone);
				setState(414);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 4) | (1L << 5) | (1L << 6) | (1L << 45) | (1L << 48) | (1L << 50))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(415); match(Sone);
				}
				break;

			case 5:
				{
				_localctx = new VordlusAvaldisPunktideJarjendContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(416); aritmeetilineAvaldisPunktideJarjend(0);
				setState(417);
				_la = _input.LA(1);
				if ( !(_la==5 || _la==48) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(418); aritmeetilineAvaldisPunktideJarjend(0);
				}
				break;

			case 6:
				{
				_localctx = new VordlusAvaldisObjektideJarjendContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(420); aritmeetilineAvaldisObjektideJarjend(0);
				setState(421);
				_la = _input.LA(1);
				if ( !(_la==5 || _la==48) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(422); aritmeetilineAvaldisObjektideJarjend(0);
				}
				break;

			case 7:
				{
				_localctx = new VordlusAvaldisJoonteJarjendContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(424); aritmeetilineAvaldisJoonteJarjend(0);
				setState(425);
				_la = _input.LA(1);
				if ( !(_la==5 || _la==48) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(426); aritmeetilineAvaldisJoonteJarjend(0);
				}
				break;

			case 8:
				{
				_localctx = new VordlusAvaldisAladeJarjendContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(428); aritmeetilineAvaldisAladeJarjend(0);
				setState(429);
				_la = _input.LA(1);
				if ( !(_la==5 || _la==48) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(430); aritmeetilineAvaldisAladeJarjend(0);
				}
				break;

			case 9:
				{
				_localctx = new VordlusAvaldisKaartidedeJarjendContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(432); aritmeetilineAvaldisKaartideJarjend(0);
				setState(433);
				_la = _input.LA(1);
				if ( !(_la==5 || _la==48) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(434); aritmeetilineAvaldisKaartideJarjend(0);
				}
				break;

			case 10:
				{
				_localctx = new VordlusAvaldisArvudeJarjendContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(436); aritmeetilineAvaldisArvudeJarjend(0);
				setState(437);
				_la = _input.LA(1);
				if ( !(_la==5 || _la==48) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(438); aritmeetilineAvaldisArvudeJarjend(0);
				}
				break;

			case 11:
				{
				_localctx = new VordlusAvaldisKoordinaatideJarjendContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(440); aritmeetilineAvaldisKoordinaatideJarjend(0);
				setState(441);
				_la = _input.LA(1);
				if ( !(_la==5 || _la==48) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(442); aritmeetilineAvaldisKoordinaatideJarjend(0);
				}
				break;

			case 12:
				{
				_localctx = new VordlusAvaldisToevaartusteJarjendContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(444); aritmeetilineAvaldisToevaartusteJarjend(0);
				setState(445);
				_la = _input.LA(1);
				if ( !(_la==5 || _la==48) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(446); aritmeetilineAvaldisToevaartusteJarjend(0);
				}
				break;

			case 13:
				{
				_localctx = new VordlusAvaldisSonedeJarjendContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(448); aritmeetilineAvaldisSonedeJarjend(0);
				setState(449);
				_la = _input.LA(1);
				if ( !(_la==5 || _la==48) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(450); aritmeetilineAvaldisSonedeJarjend(0);
				}
				break;

			case 14:
				{
				_localctx = new VordlusAvaldisToevaartusContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(452); match(Toevaartus);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(460);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new VordlusAvaldisToevaartusedContext(new VordlusAvaldisContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_vordlusAvaldis);
					setState(455);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(456);
					_la = _input.LA(1);
					if ( !(_la==5 || _la==48) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(457); match(Toevaartus);
					}
					} 
				}
				setState(462);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisArvContext extends ParserRuleContext {
		public AritmeetilineAvaldisArv3Context aritmeetilineAvaldisArv3() {
			return getRuleContext(AritmeetilineAvaldisArv3Context.class,0);
		}
		public AritmeetilineAvaldisArvContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisArv; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterAritmeetilineAvaldisArv(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitAritmeetilineAvaldisArv(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitAritmeetilineAvaldisArv(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisArvContext aritmeetilineAvaldisArv() throws RecognitionException {
		AritmeetilineAvaldisArvContext _localctx = new AritmeetilineAvaldisArvContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_aritmeetilineAvaldisArv);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(463); aritmeetilineAvaldisArv3(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisArv3Context extends ParserRuleContext {
		public AritmeetilineAvaldisArv3Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisArv3; }
	 
		public AritmeetilineAvaldisArv3Context() { }
		public void copyFrom(AritmeetilineAvaldisArv3Context ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ArvudeAvaldisTriviaalne3Context extends AritmeetilineAvaldisArv3Context {
		public AritmeetilineAvaldisArv2Context aritmeetilineAvaldisArv2() {
			return getRuleContext(AritmeetilineAvaldisArv2Context.class,0);
		}
		public ArvudeAvaldisTriviaalne3Context(AritmeetilineAvaldisArv3Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeAvaldisTriviaalne3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeAvaldisTriviaalne3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeAvaldisTriviaalne3(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArvudeAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisArv3Context {
		public AritmeetilineAvaldisArv3Context aritmeetilineAvaldisArv3() {
			return getRuleContext(AritmeetilineAvaldisArv3Context.class,0);
		}
		public AritmeetilineAvaldisArv2Context aritmeetilineAvaldisArv2() {
			return getRuleContext(AritmeetilineAvaldisArv2Context.class,0);
		}
		public ArvudeAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisArv3Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisArv3Context aritmeetilineAvaldisArv3() throws RecognitionException {
		return aritmeetilineAvaldisArv3(0);
	}

	private AritmeetilineAvaldisArv3Context aritmeetilineAvaldisArv3(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisArv3Context _localctx = new AritmeetilineAvaldisArv3Context(_ctx, _parentState);
		AritmeetilineAvaldisArv3Context _prevctx = _localctx;
		int _startState = 26;
		enterRecursionRule(_localctx, 26, RULE_aritmeetilineAvaldisArv3, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new ArvudeAvaldisTriviaalne3Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(466); aritmeetilineAvaldisArv2(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(473);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ArvudeAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisArv3Context(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisArv3);
					setState(468);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(469);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(470); aritmeetilineAvaldisArv2(0);
					}
					} 
				}
				setState(475);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisArv2Context extends ParserRuleContext {
		public AritmeetilineAvaldisArv2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisArv2; }
	 
		public AritmeetilineAvaldisArv2Context() { }
		public void copyFrom(AritmeetilineAvaldisArv2Context ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ArvudeAvaldisKorrutamineJagamineContext extends AritmeetilineAvaldisArv2Context {
		public AritmeetilineAvaldisArv1Context aritmeetilineAvaldisArv1() {
			return getRuleContext(AritmeetilineAvaldisArv1Context.class,0);
		}
		public AritmeetilineAvaldisArv2Context aritmeetilineAvaldisArv2() {
			return getRuleContext(AritmeetilineAvaldisArv2Context.class,0);
		}
		public ArvudeAvaldisKorrutamineJagamineContext(AritmeetilineAvaldisArv2Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeAvaldisKorrutamineJagamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeAvaldisKorrutamineJagamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeAvaldisKorrutamineJagamine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArvudeAvaldisTriviaalne2Context extends AritmeetilineAvaldisArv2Context {
		public AritmeetilineAvaldisArv1Context aritmeetilineAvaldisArv1() {
			return getRuleContext(AritmeetilineAvaldisArv1Context.class,0);
		}
		public ArvudeAvaldisTriviaalne2Context(AritmeetilineAvaldisArv2Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeAvaldisTriviaalne2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeAvaldisTriviaalne2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeAvaldisTriviaalne2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisArv2Context aritmeetilineAvaldisArv2() throws RecognitionException {
		return aritmeetilineAvaldisArv2(0);
	}

	private AritmeetilineAvaldisArv2Context aritmeetilineAvaldisArv2(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisArv2Context _localctx = new AritmeetilineAvaldisArv2Context(_ctx, _parentState);
		AritmeetilineAvaldisArv2Context _prevctx = _localctx;
		int _startState = 28;
		enterRecursionRule(_localctx, 28, RULE_aritmeetilineAvaldisArv2, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new ArvudeAvaldisTriviaalne2Context(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(477); aritmeetilineAvaldisArv1();
			}
			_ctx.stop = _input.LT(-1);
			setState(484);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ArvudeAvaldisKorrutamineJagamineContext(new AritmeetilineAvaldisArv2Context(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisArv2);
					setState(479);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(480);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 14) | (1L << 47))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(481); aritmeetilineAvaldisArv1();
					}
					} 
				}
				setState(486);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisArv1Context extends ParserRuleContext {
		public AritmeetilineAvaldisArv1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisArv1; }
	 
		public AritmeetilineAvaldisArv1Context() { }
		public void copyFrom(AritmeetilineAvaldisArv1Context ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ArvudeAvaldisUnaarneMiinusContext extends AritmeetilineAvaldisArv1Context {
		public AritmeetilineAvaldisArv1Context aritmeetilineAvaldisArv1() {
			return getRuleContext(AritmeetilineAvaldisArv1Context.class,0);
		}
		public ArvudeAvaldisUnaarneMiinusContext(AritmeetilineAvaldisArv1Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeAvaldisUnaarneMiinus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeAvaldisUnaarneMiinus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeAvaldisUnaarneMiinus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArvudeAvaldisTriviaalne1Context extends AritmeetilineAvaldisArv1Context {
		public AritmeetilineAvaldisArv0Context aritmeetilineAvaldisArv0() {
			return getRuleContext(AritmeetilineAvaldisArv0Context.class,0);
		}
		public ArvudeAvaldisTriviaalne1Context(AritmeetilineAvaldisArv1Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeAvaldisTriviaalne1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeAvaldisTriviaalne1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeAvaldisTriviaalne1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisArv1Context aritmeetilineAvaldisArv1() throws RecognitionException {
		AritmeetilineAvaldisArv1Context _localctx = new AritmeetilineAvaldisArv1Context(_ctx, getState());
		enterRule(_localctx, 30, RULE_aritmeetilineAvaldisArv1);
		try {
			setState(490);
			switch (_input.LA(1)) {
			case 28:
				_localctx = new ArvudeAvaldisUnaarneMiinusContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(487); match(28);
				setState(488); aritmeetilineAvaldisArv1();
				}
				break;
			case 31:
			case MuutujaNimi:
			case Taisarv:
			case Ujupunktarv:
				_localctx = new ArvudeAvaldisTriviaalne1Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(489); aritmeetilineAvaldisArv0();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisArv0Context extends ParserRuleContext {
		public AritmeetilineAvaldisArv0Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisArv0; }
	 
		public AritmeetilineAvaldisArv0Context() { }
		public void copyFrom(AritmeetilineAvaldisArv0Context ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ArvudeAvaldisMuutujaNimiContext extends AritmeetilineAvaldisArv0Context {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public ArvudeAvaldisMuutujaNimiContext(AritmeetilineAvaldisArv0Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeAvaldisMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeAvaldisMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeAvaldisMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArvudeAvaldisSuluavaldisContext extends AritmeetilineAvaldisArv0Context {
		public AritmeetilineAvaldisArvContext aritmeetilineAvaldisArv() {
			return getRuleContext(AritmeetilineAvaldisArvContext.class,0);
		}
		public ArvudeAvaldisSuluavaldisContext(AritmeetilineAvaldisArv0Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeAvaldisSuluavaldis(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeAvaldisSuluavaldis(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeAvaldisSuluavaldis(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArvudeAvaldisArvContext extends AritmeetilineAvaldisArv0Context {
		public TerminalNode Taisarv() { return getToken(SmapsParser.Taisarv, 0); }
		public TerminalNode Ujupunktarv() { return getToken(SmapsParser.Ujupunktarv, 0); }
		public ArvudeAvaldisArvContext(AritmeetilineAvaldisArv0Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeAvaldisArv(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeAvaldisArv(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeAvaldisArv(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisArv0Context aritmeetilineAvaldisArv0() throws RecognitionException {
		AritmeetilineAvaldisArv0Context _localctx = new AritmeetilineAvaldisArv0Context(_ctx, getState());
		enterRule(_localctx, 32, RULE_aritmeetilineAvaldisArv0);
		int _la;
		try {
			setState(498);
			switch (_input.LA(1)) {
			case MuutujaNimi:
				_localctx = new ArvudeAvaldisMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(492); match(MuutujaNimi);
				}
				break;
			case Taisarv:
			case Ujupunktarv:
				_localctx = new ArvudeAvaldisArvContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(493);
				_la = _input.LA(1);
				if ( !(_la==Taisarv || _la==Ujupunktarv) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;
			case 31:
				_localctx = new ArvudeAvaldisSuluavaldisContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(494); match(31);
				setState(495); aritmeetilineAvaldisArv();
				setState(496); match(18);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisLaiuskoordinaatContext extends ParserRuleContext {
		public AritmeetilineAvaldisLaiuskoordinaatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisLaiuskoordinaat; }
	 
		public AritmeetilineAvaldisLaiuskoordinaatContext() { }
		public void copyFrom(AritmeetilineAvaldisLaiuskoordinaatContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LaiuskoordinaatideAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisLaiuskoordinaatContext {
		public LaiuskoordinaatContext laiuskoordinaat() {
			return getRuleContext(LaiuskoordinaatContext.class,0);
		}
		public AritmeetilineAvaldisLaiuskoordinaatContext aritmeetilineAvaldisLaiuskoordinaat() {
			return getRuleContext(AritmeetilineAvaldisLaiuskoordinaatContext.class,0);
		}
		public LaiuskoordinaatideAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisLaiuskoordinaatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLaiuskoordinaatideAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLaiuskoordinaatideAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLaiuskoordinaatideAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LaiuskoordinaatideAvaldisLaiuskoordinaatContext extends AritmeetilineAvaldisLaiuskoordinaatContext {
		public LaiuskoordinaatContext laiuskoordinaat() {
			return getRuleContext(LaiuskoordinaatContext.class,0);
		}
		public LaiuskoordinaatideAvaldisLaiuskoordinaatContext(AritmeetilineAvaldisLaiuskoordinaatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLaiuskoordinaatideAvaldisLaiuskoordinaat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLaiuskoordinaatideAvaldisLaiuskoordinaat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLaiuskoordinaatideAvaldisLaiuskoordinaat(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisLaiuskoordinaatContext aritmeetilineAvaldisLaiuskoordinaat() throws RecognitionException {
		return aritmeetilineAvaldisLaiuskoordinaat(0);
	}

	private AritmeetilineAvaldisLaiuskoordinaatContext aritmeetilineAvaldisLaiuskoordinaat(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisLaiuskoordinaatContext _localctx = new AritmeetilineAvaldisLaiuskoordinaatContext(_ctx, _parentState);
		AritmeetilineAvaldisLaiuskoordinaatContext _prevctx = _localctx;
		int _startState = 34;
		enterRecursionRule(_localctx, 34, RULE_aritmeetilineAvaldisLaiuskoordinaat, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new LaiuskoordinaatideAvaldisLaiuskoordinaatContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(501); laiuskoordinaat();
			}
			_ctx.stop = _input.LT(-1);
			setState(508);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new LaiuskoordinaatideAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisLaiuskoordinaatContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisLaiuskoordinaat);
					setState(503);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(504);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(505); laiuskoordinaat();
					}
					} 
				}
				setState(510);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisPikkuskoordinaatContext extends ParserRuleContext {
		public AritmeetilineAvaldisPikkuskoordinaatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisPikkuskoordinaat; }
	 
		public AritmeetilineAvaldisPikkuskoordinaatContext() { }
		public void copyFrom(AritmeetilineAvaldisPikkuskoordinaatContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PikkuskoordinaatideAvaldisPikkuskoordinaatContext extends AritmeetilineAvaldisPikkuskoordinaatContext {
		public PikkuskoordinaatContext pikkuskoordinaat() {
			return getRuleContext(PikkuskoordinaatContext.class,0);
		}
		public PikkuskoordinaatideAvaldisPikkuskoordinaatContext(AritmeetilineAvaldisPikkuskoordinaatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPikkuskoordinaatideAvaldisPikkuskoordinaat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPikkuskoordinaatideAvaldisPikkuskoordinaat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPikkuskoordinaatideAvaldisPikkuskoordinaat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PikkuskoordinaatideAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisPikkuskoordinaatContext {
		public PikkuskoordinaatContext pikkuskoordinaat() {
			return getRuleContext(PikkuskoordinaatContext.class,0);
		}
		public AritmeetilineAvaldisPikkuskoordinaatContext aritmeetilineAvaldisPikkuskoordinaat() {
			return getRuleContext(AritmeetilineAvaldisPikkuskoordinaatContext.class,0);
		}
		public PikkuskoordinaatideAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisPikkuskoordinaatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPikkuskoordinaatideAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPikkuskoordinaatideAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPikkuskoordinaatideAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisPikkuskoordinaatContext aritmeetilineAvaldisPikkuskoordinaat() throws RecognitionException {
		return aritmeetilineAvaldisPikkuskoordinaat(0);
	}

	private AritmeetilineAvaldisPikkuskoordinaatContext aritmeetilineAvaldisPikkuskoordinaat(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisPikkuskoordinaatContext _localctx = new AritmeetilineAvaldisPikkuskoordinaatContext(_ctx, _parentState);
		AritmeetilineAvaldisPikkuskoordinaatContext _prevctx = _localctx;
		int _startState = 36;
		enterRecursionRule(_localctx, 36, RULE_aritmeetilineAvaldisPikkuskoordinaat, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new PikkuskoordinaatideAvaldisPikkuskoordinaatContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(512); pikkuskoordinaat();
			}
			_ctx.stop = _input.LT(-1);
			setState(519);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new PikkuskoordinaatideAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisPikkuskoordinaatContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisPikkuskoordinaat);
					setState(514);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(515);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(516); pikkuskoordinaat();
					}
					} 
				}
				setState(521);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisSoneContext extends ParserRuleContext {
		public AritmeetilineAvaldisSoneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisSone; }
	 
		public AritmeetilineAvaldisSoneContext() { }
		public void copyFrom(AritmeetilineAvaldisSoneContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SonedeAvaldisMuutujaNimiContext extends AritmeetilineAvaldisSoneContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public SonedeAvaldisMuutujaNimiContext(AritmeetilineAvaldisSoneContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterSonedeAvaldisMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitSonedeAvaldisMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitSonedeAvaldisMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SonedeAvaldisSoneContext extends AritmeetilineAvaldisSoneContext {
		public TerminalNode Sone() { return getToken(SmapsParser.Sone, 0); }
		public SonedeAvaldisSoneContext(AritmeetilineAvaldisSoneContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterSonedeAvaldisSone(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitSonedeAvaldisSone(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitSonedeAvaldisSone(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisSoneContext aritmeetilineAvaldisSone() throws RecognitionException {
		AritmeetilineAvaldisSoneContext _localctx = new AritmeetilineAvaldisSoneContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_aritmeetilineAvaldisSone);
		try {
			setState(524);
			switch (_input.LA(1)) {
			case MuutujaNimi:
				_localctx = new SonedeAvaldisMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(522); match(MuutujaNimi);
				}
				break;
			case Sone:
				_localctx = new SonedeAvaldisSoneContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(523); match(Sone);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisPunktideJarjendContext extends ParserRuleContext {
		public AritmeetilineAvaldisPunktideJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisPunktideJarjend; }
	 
		public AritmeetilineAvaldisPunktideJarjendContext() { }
		public void copyFrom(AritmeetilineAvaldisPunktideJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PunktideJarjendiAvaldisPunktideJarjendContext extends AritmeetilineAvaldisPunktideJarjendContext {
		public PunktideJarjendContext punktideJarjend() {
			return getRuleContext(PunktideJarjendContext.class,0);
		}
		public PunktideJarjendiAvaldisPunktideJarjendContext(AritmeetilineAvaldisPunktideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktideJarjendiAvaldisPunktideJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktideJarjendiAvaldisPunktideJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktideJarjendiAvaldisPunktideJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PunktideJarjendiAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisPunktideJarjendContext {
		public PunktideJarjendContext punktideJarjend() {
			return getRuleContext(PunktideJarjendContext.class,0);
		}
		public AritmeetilineAvaldisPunktideJarjendContext aritmeetilineAvaldisPunktideJarjend() {
			return getRuleContext(AritmeetilineAvaldisPunktideJarjendContext.class,0);
		}
		public PunktideJarjendiAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisPunktideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktideJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktideJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktideJarjendiAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisPunktideJarjendContext aritmeetilineAvaldisPunktideJarjend() throws RecognitionException {
		return aritmeetilineAvaldisPunktideJarjend(0);
	}

	private AritmeetilineAvaldisPunktideJarjendContext aritmeetilineAvaldisPunktideJarjend(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisPunktideJarjendContext _localctx = new AritmeetilineAvaldisPunktideJarjendContext(_ctx, _parentState);
		AritmeetilineAvaldisPunktideJarjendContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, 40, RULE_aritmeetilineAvaldisPunktideJarjend, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new PunktideJarjendiAvaldisPunktideJarjendContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(527); punktideJarjend();
			}
			_ctx.stop = _input.LT(-1);
			setState(534);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,35,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new PunktideJarjendiAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisPunktideJarjendContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisPunktideJarjend);
					setState(529);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(530);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(531); punktideJarjend();
					}
					} 
				}
				setState(536);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,35,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisObjektideJarjendContext extends ParserRuleContext {
		public AritmeetilineAvaldisObjektideJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisObjektideJarjend; }
	 
		public AritmeetilineAvaldisObjektideJarjendContext() { }
		public void copyFrom(AritmeetilineAvaldisObjektideJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ObjektideJarjendiAvaldisObjektideJarjendContext extends AritmeetilineAvaldisObjektideJarjendContext {
		public ObjektideJarjendContext objektideJarjend() {
			return getRuleContext(ObjektideJarjendContext.class,0);
		}
		public ObjektideJarjendiAvaldisObjektideJarjendContext(AritmeetilineAvaldisObjektideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterObjektideJarjendiAvaldisObjektideJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitObjektideJarjendiAvaldisObjektideJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitObjektideJarjendiAvaldisObjektideJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ObjektideJarjendiAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisObjektideJarjendContext {
		public AritmeetilineAvaldisObjektideJarjendContext aritmeetilineAvaldisObjektideJarjend() {
			return getRuleContext(AritmeetilineAvaldisObjektideJarjendContext.class,0);
		}
		public ObjektideJarjendContext objektideJarjend() {
			return getRuleContext(ObjektideJarjendContext.class,0);
		}
		public ObjektideJarjendiAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisObjektideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterObjektideJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitObjektideJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitObjektideJarjendiAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisObjektideJarjendContext aritmeetilineAvaldisObjektideJarjend() throws RecognitionException {
		return aritmeetilineAvaldisObjektideJarjend(0);
	}

	private AritmeetilineAvaldisObjektideJarjendContext aritmeetilineAvaldisObjektideJarjend(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisObjektideJarjendContext _localctx = new AritmeetilineAvaldisObjektideJarjendContext(_ctx, _parentState);
		AritmeetilineAvaldisObjektideJarjendContext _prevctx = _localctx;
		int _startState = 42;
		enterRecursionRule(_localctx, 42, RULE_aritmeetilineAvaldisObjektideJarjend, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new ObjektideJarjendiAvaldisObjektideJarjendContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(538); objektideJarjend();
			}
			_ctx.stop = _input.LT(-1);
			setState(545);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ObjektideJarjendiAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisObjektideJarjendContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisObjektideJarjend);
					setState(540);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(541);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(542); objektideJarjend();
					}
					} 
				}
				setState(547);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,36,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisJoonteJarjendContext extends ParserRuleContext {
		public AritmeetilineAvaldisJoonteJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisJoonteJarjend; }
	 
		public AritmeetilineAvaldisJoonteJarjendContext() { }
		public void copyFrom(AritmeetilineAvaldisJoonteJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class JoonteJarjendiAvaldisJoonteJarjendContext extends AritmeetilineAvaldisJoonteJarjendContext {
		public JoonteJarjendContext joonteJarjend() {
			return getRuleContext(JoonteJarjendContext.class,0);
		}
		public JoonteJarjendiAvaldisJoonteJarjendContext(AritmeetilineAvaldisJoonteJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterJoonteJarjendiAvaldisJoonteJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitJoonteJarjendiAvaldisJoonteJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitJoonteJarjendiAvaldisJoonteJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class JoonteJarjendiAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisJoonteJarjendContext {
		public AritmeetilineAvaldisJoonteJarjendContext aritmeetilineAvaldisJoonteJarjend() {
			return getRuleContext(AritmeetilineAvaldisJoonteJarjendContext.class,0);
		}
		public JoonteJarjendContext joonteJarjend() {
			return getRuleContext(JoonteJarjendContext.class,0);
		}
		public JoonteJarjendiAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisJoonteJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterJoonteJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitJoonteJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitJoonteJarjendiAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisJoonteJarjendContext aritmeetilineAvaldisJoonteJarjend() throws RecognitionException {
		return aritmeetilineAvaldisJoonteJarjend(0);
	}

	private AritmeetilineAvaldisJoonteJarjendContext aritmeetilineAvaldisJoonteJarjend(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisJoonteJarjendContext _localctx = new AritmeetilineAvaldisJoonteJarjendContext(_ctx, _parentState);
		AritmeetilineAvaldisJoonteJarjendContext _prevctx = _localctx;
		int _startState = 44;
		enterRecursionRule(_localctx, 44, RULE_aritmeetilineAvaldisJoonteJarjend, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new JoonteJarjendiAvaldisJoonteJarjendContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(549); joonteJarjend();
			}
			_ctx.stop = _input.LT(-1);
			setState(556);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new JoonteJarjendiAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisJoonteJarjendContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisJoonteJarjend);
					setState(551);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(552);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(553); joonteJarjend();
					}
					} 
				}
				setState(558);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisAladeJarjendContext extends ParserRuleContext {
		public AritmeetilineAvaldisAladeJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisAladeJarjend; }
	 
		public AritmeetilineAvaldisAladeJarjendContext() { }
		public void copyFrom(AritmeetilineAvaldisAladeJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AladeJarjendiAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisAladeJarjendContext {
		public AritmeetilineAvaldisAladeJarjendContext aritmeetilineAvaldisAladeJarjend() {
			return getRuleContext(AritmeetilineAvaldisAladeJarjendContext.class,0);
		}
		public AladeJarjendContext aladeJarjend() {
			return getRuleContext(AladeJarjendContext.class,0);
		}
		public AladeJarjendiAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisAladeJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterAladeJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitAladeJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitAladeJarjendiAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AladeJarjendiAvaldisAladeJarjendContext extends AritmeetilineAvaldisAladeJarjendContext {
		public AladeJarjendContext aladeJarjend() {
			return getRuleContext(AladeJarjendContext.class,0);
		}
		public AladeJarjendiAvaldisAladeJarjendContext(AritmeetilineAvaldisAladeJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterAladeJarjendiAvaldisAladeJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitAladeJarjendiAvaldisAladeJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitAladeJarjendiAvaldisAladeJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisAladeJarjendContext aritmeetilineAvaldisAladeJarjend() throws RecognitionException {
		return aritmeetilineAvaldisAladeJarjend(0);
	}

	private AritmeetilineAvaldisAladeJarjendContext aritmeetilineAvaldisAladeJarjend(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisAladeJarjendContext _localctx = new AritmeetilineAvaldisAladeJarjendContext(_ctx, _parentState);
		AritmeetilineAvaldisAladeJarjendContext _prevctx = _localctx;
		int _startState = 46;
		enterRecursionRule(_localctx, 46, RULE_aritmeetilineAvaldisAladeJarjend, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new AladeJarjendiAvaldisAladeJarjendContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(560); aladeJarjend();
			}
			_ctx.stop = _input.LT(-1);
			setState(567);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AladeJarjendiAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisAladeJarjendContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisAladeJarjend);
					setState(562);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(563);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(564); aladeJarjend();
					}
					} 
				}
				setState(569);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisKaartideJarjendContext extends ParserRuleContext {
		public AritmeetilineAvaldisKaartideJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisKaartideJarjend; }
	 
		public AritmeetilineAvaldisKaartideJarjendContext() { }
		public void copyFrom(AritmeetilineAvaldisKaartideJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class KaartideJarjendiAvaldisKaartideJarjendContext extends AritmeetilineAvaldisKaartideJarjendContext {
		public KaartideJarjendContext kaartideJarjend() {
			return getRuleContext(KaartideJarjendContext.class,0);
		}
		public KaartideJarjendiAvaldisKaartideJarjendContext(AritmeetilineAvaldisKaartideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaartideJarjendiAvaldisKaartideJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaartideJarjendiAvaldisKaartideJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaartideJarjendiAvaldisKaartideJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KaartideJarjendiAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisKaartideJarjendContext {
		public AritmeetilineAvaldisKaartideJarjendContext aritmeetilineAvaldisKaartideJarjend() {
			return getRuleContext(AritmeetilineAvaldisKaartideJarjendContext.class,0);
		}
		public KaartideJarjendContext kaartideJarjend() {
			return getRuleContext(KaartideJarjendContext.class,0);
		}
		public KaartideJarjendiAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisKaartideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaartideJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaartideJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaartideJarjendiAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisKaartideJarjendContext aritmeetilineAvaldisKaartideJarjend() throws RecognitionException {
		return aritmeetilineAvaldisKaartideJarjend(0);
	}

	private AritmeetilineAvaldisKaartideJarjendContext aritmeetilineAvaldisKaartideJarjend(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisKaartideJarjendContext _localctx = new AritmeetilineAvaldisKaartideJarjendContext(_ctx, _parentState);
		AritmeetilineAvaldisKaartideJarjendContext _prevctx = _localctx;
		int _startState = 48;
		enterRecursionRule(_localctx, 48, RULE_aritmeetilineAvaldisKaartideJarjend, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new KaartideJarjendiAvaldisKaartideJarjendContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(571); kaartideJarjend();
			}
			_ctx.stop = _input.LT(-1);
			setState(578);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new KaartideJarjendiAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisKaartideJarjendContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisKaartideJarjend);
					setState(573);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(574);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(575); kaartideJarjend();
					}
					} 
				}
				setState(580);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisArvudeJarjendContext extends ParserRuleContext {
		public AritmeetilineAvaldisArvudeJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisArvudeJarjend; }
	 
		public AritmeetilineAvaldisArvudeJarjendContext() { }
		public void copyFrom(AritmeetilineAvaldisArvudeJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ArvudeJarjendiAvaldisArvudeJarjendContext extends AritmeetilineAvaldisArvudeJarjendContext {
		public ArvudeJarjendContext arvudeJarjend() {
			return getRuleContext(ArvudeJarjendContext.class,0);
		}
		public ArvudeJarjendiAvaldisArvudeJarjendContext(AritmeetilineAvaldisArvudeJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeJarjendiAvaldisArvudeJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeJarjendiAvaldisArvudeJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeJarjendiAvaldisArvudeJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArvudeJarjendiAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisArvudeJarjendContext {
		public AritmeetilineAvaldisArvudeJarjendContext aritmeetilineAvaldisArvudeJarjend() {
			return getRuleContext(AritmeetilineAvaldisArvudeJarjendContext.class,0);
		}
		public ArvudeJarjendContext arvudeJarjend() {
			return getRuleContext(ArvudeJarjendContext.class,0);
		}
		public ArvudeJarjendiAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisArvudeJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeJarjendiAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisArvudeJarjendContext aritmeetilineAvaldisArvudeJarjend() throws RecognitionException {
		return aritmeetilineAvaldisArvudeJarjend(0);
	}

	private AritmeetilineAvaldisArvudeJarjendContext aritmeetilineAvaldisArvudeJarjend(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisArvudeJarjendContext _localctx = new AritmeetilineAvaldisArvudeJarjendContext(_ctx, _parentState);
		AritmeetilineAvaldisArvudeJarjendContext _prevctx = _localctx;
		int _startState = 50;
		enterRecursionRule(_localctx, 50, RULE_aritmeetilineAvaldisArvudeJarjend, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new ArvudeJarjendiAvaldisArvudeJarjendContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(582); arvudeJarjend();
			}
			_ctx.stop = _input.LT(-1);
			setState(589);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,40,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ArvudeJarjendiAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisArvudeJarjendContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisArvudeJarjend);
					setState(584);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(585);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(586); arvudeJarjend();
					}
					} 
				}
				setState(591);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,40,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisKoordinaatideJarjendContext extends ParserRuleContext {
		public AritmeetilineAvaldisKoordinaatideJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisKoordinaatideJarjend; }
	 
		public AritmeetilineAvaldisKoordinaatideJarjendContext() { }
		public void copyFrom(AritmeetilineAvaldisKoordinaatideJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class KoordinaatideJarjendiAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisKoordinaatideJarjendContext {
		public KoordinaatideJarjendContext koordinaatideJarjend() {
			return getRuleContext(KoordinaatideJarjendContext.class,0);
		}
		public AritmeetilineAvaldisKoordinaatideJarjendContext aritmeetilineAvaldisKoordinaatideJarjend() {
			return getRuleContext(AritmeetilineAvaldisKoordinaatideJarjendContext.class,0);
		}
		public KoordinaatideJarjendiAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisKoordinaatideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKoordinaatideJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKoordinaatideJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKoordinaatideJarjendiAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KoordinaatideJarjendiAvaldisKoordinaatideJarjendContext extends AritmeetilineAvaldisKoordinaatideJarjendContext {
		public KoordinaatideJarjendContext koordinaatideJarjend() {
			return getRuleContext(KoordinaatideJarjendContext.class,0);
		}
		public KoordinaatideJarjendiAvaldisKoordinaatideJarjendContext(AritmeetilineAvaldisKoordinaatideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKoordinaatideJarjendiAvaldisKoordinaatideJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKoordinaatideJarjendiAvaldisKoordinaatideJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKoordinaatideJarjendiAvaldisKoordinaatideJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisKoordinaatideJarjendContext aritmeetilineAvaldisKoordinaatideJarjend() throws RecognitionException {
		return aritmeetilineAvaldisKoordinaatideJarjend(0);
	}

	private AritmeetilineAvaldisKoordinaatideJarjendContext aritmeetilineAvaldisKoordinaatideJarjend(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisKoordinaatideJarjendContext _localctx = new AritmeetilineAvaldisKoordinaatideJarjendContext(_ctx, _parentState);
		AritmeetilineAvaldisKoordinaatideJarjendContext _prevctx = _localctx;
		int _startState = 52;
		enterRecursionRule(_localctx, 52, RULE_aritmeetilineAvaldisKoordinaatideJarjend, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new KoordinaatideJarjendiAvaldisKoordinaatideJarjendContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(593); koordinaatideJarjend();
			}
			_ctx.stop = _input.LT(-1);
			setState(600);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new KoordinaatideJarjendiAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisKoordinaatideJarjendContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisKoordinaatideJarjend);
					setState(595);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(596);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(597); koordinaatideJarjend();
					}
					} 
				}
				setState(602);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisToevaartusteJarjendContext extends ParserRuleContext {
		public AritmeetilineAvaldisToevaartusteJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisToevaartusteJarjend; }
	 
		public AritmeetilineAvaldisToevaartusteJarjendContext() { }
		public void copyFrom(AritmeetilineAvaldisToevaartusteJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ToevaartusteJarjendiAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisToevaartusteJarjendContext {
		public AritmeetilineAvaldisToevaartusteJarjendContext aritmeetilineAvaldisToevaartusteJarjend() {
			return getRuleContext(AritmeetilineAvaldisToevaartusteJarjendContext.class,0);
		}
		public ToevaartusteJarjendContext toevaartusteJarjend() {
			return getRuleContext(ToevaartusteJarjendContext.class,0);
		}
		public ToevaartusteJarjendiAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisToevaartusteJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterToevaartusteJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitToevaartusteJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitToevaartusteJarjendiAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ToevaartusteJarjendiAvaldisToevaartusteJarjendContext extends AritmeetilineAvaldisToevaartusteJarjendContext {
		public ToevaartusteJarjendContext toevaartusteJarjend() {
			return getRuleContext(ToevaartusteJarjendContext.class,0);
		}
		public ToevaartusteJarjendiAvaldisToevaartusteJarjendContext(AritmeetilineAvaldisToevaartusteJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterToevaartusteJarjendiAvaldisToevaartusteJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitToevaartusteJarjendiAvaldisToevaartusteJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitToevaartusteJarjendiAvaldisToevaartusteJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisToevaartusteJarjendContext aritmeetilineAvaldisToevaartusteJarjend() throws RecognitionException {
		return aritmeetilineAvaldisToevaartusteJarjend(0);
	}

	private AritmeetilineAvaldisToevaartusteJarjendContext aritmeetilineAvaldisToevaartusteJarjend(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisToevaartusteJarjendContext _localctx = new AritmeetilineAvaldisToevaartusteJarjendContext(_ctx, _parentState);
		AritmeetilineAvaldisToevaartusteJarjendContext _prevctx = _localctx;
		int _startState = 54;
		enterRecursionRule(_localctx, 54, RULE_aritmeetilineAvaldisToevaartusteJarjend, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new ToevaartusteJarjendiAvaldisToevaartusteJarjendContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(604); toevaartusteJarjend();
			}
			_ctx.stop = _input.LT(-1);
			setState(611);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,42,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ToevaartusteJarjendiAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisToevaartusteJarjendContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisToevaartusteJarjend);
					setState(606);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(607);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(608); toevaartusteJarjend();
					}
					} 
				}
				setState(613);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,42,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AritmeetilineAvaldisSonedeJarjendContext extends ParserRuleContext {
		public AritmeetilineAvaldisSonedeJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aritmeetilineAvaldisSonedeJarjend; }
	 
		public AritmeetilineAvaldisSonedeJarjendContext() { }
		public void copyFrom(AritmeetilineAvaldisSonedeJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SonedeJarjendiAvaldisSonedeJarjendContext extends AritmeetilineAvaldisSonedeJarjendContext {
		public SonedeJarjendContext sonedeJarjend() {
			return getRuleContext(SonedeJarjendContext.class,0);
		}
		public SonedeJarjendiAvaldisSonedeJarjendContext(AritmeetilineAvaldisSonedeJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterSonedeJarjendiAvaldisSonedeJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitSonedeJarjendiAvaldisSonedeJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitSonedeJarjendiAvaldisSonedeJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SonedeJarjendiAvaldisLiitmineLahutamineContext extends AritmeetilineAvaldisSonedeJarjendContext {
		public SonedeJarjendContext sonedeJarjend() {
			return getRuleContext(SonedeJarjendContext.class,0);
		}
		public AritmeetilineAvaldisSonedeJarjendContext aritmeetilineAvaldisSonedeJarjend() {
			return getRuleContext(AritmeetilineAvaldisSonedeJarjendContext.class,0);
		}
		public SonedeJarjendiAvaldisLiitmineLahutamineContext(AritmeetilineAvaldisSonedeJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterSonedeJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitSonedeJarjendiAvaldisLiitmineLahutamine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitSonedeJarjendiAvaldisLiitmineLahutamine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AritmeetilineAvaldisSonedeJarjendContext aritmeetilineAvaldisSonedeJarjend() throws RecognitionException {
		return aritmeetilineAvaldisSonedeJarjend(0);
	}

	private AritmeetilineAvaldisSonedeJarjendContext aritmeetilineAvaldisSonedeJarjend(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AritmeetilineAvaldisSonedeJarjendContext _localctx = new AritmeetilineAvaldisSonedeJarjendContext(_ctx, _parentState);
		AritmeetilineAvaldisSonedeJarjendContext _prevctx = _localctx;
		int _startState = 56;
		enterRecursionRule(_localctx, 56, RULE_aritmeetilineAvaldisSonedeJarjend, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new SonedeJarjendiAvaldisSonedeJarjendContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(615); sonedeJarjend();
			}
			_ctx.stop = _input.LT(-1);
			setState(622);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,43,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new SonedeJarjendiAvaldisLiitmineLahutamineContext(new AritmeetilineAvaldisSonedeJarjendContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_aritmeetilineAvaldisSonedeJarjend);
					setState(617);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(618);
					_la = _input.LA(1);
					if ( !(_la==28 || _la==41) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(619); sonedeJarjend();
					}
					} 
				}
				setState(624);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,43,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PunktiParameeterContext extends ParserRuleContext {
		public PunktiParameeterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_punktiParameeter; }
	 
		public PunktiParameeterContext() { }
		public void copyFrom(PunktiParameeterContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PunktiParameeterNimiContext extends PunktiParameeterContext {
		public SoneContext sone() {
			return getRuleContext(SoneContext.class,0);
		}
		public PunktiParameeterNimiContext(PunktiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktiParameeterNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktiParameeterNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktiParameeterNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PunktiParameeterKoordinaadidContext extends PunktiParameeterContext {
		public PunktContext punkt() {
			return getRuleContext(PunktContext.class,0);
		}
		public PunktiParameeterKoordinaadidContext(PunktiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktiParameeterKoordinaadid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktiParameeterKoordinaadid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktiParameeterKoordinaadid(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PunktiParameeterKuvaContext extends PunktiParameeterContext {
		public TerminalNode KuvaVaartus() { return getToken(SmapsParser.KuvaVaartus, 0); }
		public PunktiParameeterKuvaContext(PunktiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktiParameeterKuva(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktiParameeterKuva(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktiParameeterKuva(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PunktiParameeterContext punktiParameeter() throws RecognitionException {
		PunktiParameeterContext _localctx = new PunktiParameeterContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_punktiParameeter);
		int _la;
		try {
			setState(637);
			switch (_input.LA(1)) {
			case 3:
			case 26:
				_localctx = new PunktiParameeterKoordinaadidContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(625);
				_la = _input.LA(1);
				if ( !(_la==3 || _la==26) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(626); match(30);
				setState(627); punkt();
				setState(628); match(44);
				}
				break;
			case 19:
				_localctx = new PunktiParameeterNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(630); match(19);
				setState(631); sone();
				setState(632); match(44);
				}
				break;
			case 51:
				_localctx = new PunktiParameeterKuvaContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(634); match(51);
				setState(635); match(KuvaVaartus);
				setState(636); match(44);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjektiParameeterContext extends ParserRuleContext {
		public ObjektiParameeterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objektiParameeter; }
	 
		public ObjektiParameeterContext() { }
		public void copyFrom(ObjektiParameeterContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ObjektiParameeterKoordinaadidContext extends ObjektiParameeterContext {
		public PunktContext punkt() {
			return getRuleContext(PunktContext.class,0);
		}
		public ObjektiParameeterKoordinaadidContext(ObjektiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterObjektiParameeterKoordinaadid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitObjektiParameeterKoordinaadid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitObjektiParameeterKoordinaadid(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ObjektiParameeterTuupContext extends ObjektiParameeterContext {
		public TerminalNode ObjektiTuup() { return getToken(SmapsParser.ObjektiTuup, 0); }
		public ObjektiParameeterTuupContext(ObjektiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterObjektiParameeterTuup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitObjektiParameeterTuup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitObjektiParameeterTuup(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ObjektiParameeterMootmedContext extends ObjektiParameeterContext {
		public ArvudeJarjendContext arvudeJarjend() {
			return getRuleContext(ArvudeJarjendContext.class,0);
		}
		public ObjektiParameeterMootmedContext(ObjektiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterObjektiParameeterMootmed(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitObjektiParameeterMootmed(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitObjektiParameeterMootmed(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ObjektiParameeterNimiContext extends ObjektiParameeterContext {
		public SoneContext sone() {
			return getRuleContext(SoneContext.class,0);
		}
		public ObjektiParameeterNimiContext(ObjektiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterObjektiParameeterNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitObjektiParameeterNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitObjektiParameeterNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ObjektiParameeterKuvaContext extends ObjektiParameeterContext {
		public TerminalNode KuvaVaartus() { return getToken(SmapsParser.KuvaVaartus, 0); }
		public ObjektiParameeterKuvaContext(ObjektiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterObjektiParameeterKuva(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitObjektiParameeterKuva(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitObjektiParameeterKuva(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ObjektiParameeterContext objektiParameeter() throws RecognitionException {
		ObjektiParameeterContext _localctx = new ObjektiParameeterContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_objektiParameeter);
		int _la;
		try {
			setState(659);
			switch (_input.LA(1)) {
			case 3:
			case 26:
				_localctx = new ObjektiParameeterKoordinaadidContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(639);
				_la = _input.LA(1);
				if ( !(_la==3 || _la==26) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(640); match(30);
				setState(641); punkt();
				setState(642); match(44);
				}
				break;
			case 7:
			case 8:
				_localctx = new ObjektiParameeterMootmedContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(644);
				_la = _input.LA(1);
				if ( !(_la==7 || _la==8) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(645); match(30);
				setState(646); arvudeJarjend();
				setState(647); match(44);
				}
				break;
			case 19:
				_localctx = new ObjektiParameeterNimiContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(649); match(19);
				setState(650); sone();
				setState(651); match(44);
				}
				break;
			case 49:
				_localctx = new ObjektiParameeterTuupContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(653); match(49);
				setState(654); match(ObjektiTuup);
				setState(655); match(44);
				}
				break;
			case 51:
				_localctx = new ObjektiParameeterKuvaContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(656); match(51);
				setState(657); match(KuvaVaartus);
				setState(658); match(44);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JooneParameeterContext extends ParserRuleContext {
		public JooneParameeterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jooneParameeter; }
	 
		public JooneParameeterContext() { }
		public void copyFrom(JooneParameeterContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class JooneParameeterTuupContext extends JooneParameeterContext {
		public TerminalNode JooneTuup() { return getToken(SmapsParser.JooneTuup, 0); }
		public JooneParameeterTuupContext(JooneParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterJooneParameeterTuup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitJooneParameeterTuup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitJooneParameeterTuup(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class JooneParameeterKuvaContext extends JooneParameeterContext {
		public TerminalNode KuvaVaartus() { return getToken(SmapsParser.KuvaVaartus, 0); }
		public JooneParameeterKuvaContext(JooneParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterJooneParameeterKuva(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitJooneParameeterKuva(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitJooneParameeterKuva(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class JooneParameeterNimiContext extends JooneParameeterContext {
		public SoneContext sone() {
			return getRuleContext(SoneContext.class,0);
		}
		public JooneParameeterNimiContext(JooneParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterJooneParameeterNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitJooneParameeterNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitJooneParameeterNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class JooneParameeterKoordinaadidContext extends JooneParameeterContext {
		public PunktideJarjendContext punktideJarjend() {
			return getRuleContext(PunktideJarjendContext.class,0);
		}
		public JooneParameeterKoordinaadidContext(JooneParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterJooneParameeterKoordinaadid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitJooneParameeterKoordinaadid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitJooneParameeterKoordinaadid(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JooneParameeterContext jooneParameeter() throws RecognitionException {
		JooneParameeterContext _localctx = new JooneParameeterContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_jooneParameeter);
		int _la;
		try {
			setState(676);
			switch (_input.LA(1)) {
			case 3:
			case 26:
				_localctx = new JooneParameeterKoordinaadidContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(661);
				_la = _input.LA(1);
				if ( !(_la==3 || _la==26) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(662); match(30);
				setState(663); punktideJarjend();
				setState(664); match(44);
				}
				break;
			case 19:
				_localctx = new JooneParameeterNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(666); match(19);
				setState(667); sone();
				setState(668); match(44);
				}
				break;
			case 49:
				_localctx = new JooneParameeterTuupContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(670); match(49);
				setState(671); match(JooneTuup);
				setState(672); match(44);
				}
				break;
			case 51:
				_localctx = new JooneParameeterKuvaContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(673); match(51);
				setState(674); match(KuvaVaartus);
				setState(675); match(44);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AlaParameeterContext extends ParserRuleContext {
		public AlaParameeterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alaParameeter; }
	 
		public AlaParameeterContext() { }
		public void copyFrom(AlaParameeterContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AlaParameeterNimiContext extends AlaParameeterContext {
		public SoneContext sone() {
			return getRuleContext(SoneContext.class,0);
		}
		public AlaParameeterNimiContext(AlaParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterAlaParameeterNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitAlaParameeterNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitAlaParameeterNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AlaParameeterKuvaContext extends AlaParameeterContext {
		public TerminalNode KuvaVaartus() { return getToken(SmapsParser.KuvaVaartus, 0); }
		public AlaParameeterKuvaContext(AlaParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterAlaParameeterKuva(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitAlaParameeterKuva(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitAlaParameeterKuva(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AlaParameeterTuupContext extends AlaParameeterContext {
		public TerminalNode AlaTuup() { return getToken(SmapsParser.AlaTuup, 0); }
		public AlaParameeterTuupContext(AlaParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterAlaParameeterTuup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitAlaParameeterTuup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitAlaParameeterTuup(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AlaParameeterKoordinaadidContext extends AlaParameeterContext {
		public PunktideJarjendContext punktideJarjend() {
			return getRuleContext(PunktideJarjendContext.class,0);
		}
		public AlaParameeterKoordinaadidContext(AlaParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterAlaParameeterKoordinaadid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitAlaParameeterKoordinaadid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitAlaParameeterKoordinaadid(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AlaParameeterContext alaParameeter() throws RecognitionException {
		AlaParameeterContext _localctx = new AlaParameeterContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_alaParameeter);
		int _la;
		try {
			setState(693);
			switch (_input.LA(1)) {
			case 3:
			case 26:
				_localctx = new AlaParameeterKoordinaadidContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(678);
				_la = _input.LA(1);
				if ( !(_la==3 || _la==26) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(679); match(30);
				setState(680); punktideJarjend();
				setState(681); match(44);
				}
				break;
			case 19:
				_localctx = new AlaParameeterNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(683); match(19);
				setState(684); sone();
				setState(685); match(44);
				}
				break;
			case 49:
				_localctx = new AlaParameeterTuupContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(687); match(49);
				setState(688); match(AlaTuup);
				setState(689); match(44);
				}
				break;
			case 51:
				_localctx = new AlaParameeterKuvaContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(690); match(51);
				setState(691); match(KuvaVaartus);
				setState(692); match(44);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KaardiParameeterContext extends ParserRuleContext {
		public KaardiParameeterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kaardiParameeter; }
	 
		public KaardiParameeterContext() { }
		public void copyFrom(KaardiParameeterContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class KaardiParameeterJoonedContext extends KaardiParameeterContext {
		public AritmeetilineAvaldisJoonteJarjendContext aritmeetilineAvaldisJoonteJarjend() {
			return getRuleContext(AritmeetilineAvaldisJoonteJarjendContext.class,0);
		}
		public KaardiParameeterJoonedContext(KaardiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaardiParameeterJooned(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaardiParameeterJooned(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaardiParameeterJooned(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KaardiParameeterTuupContext extends KaardiParameeterContext {
		public TerminalNode KaardiTuup() { return getToken(SmapsParser.KaardiTuup, 0); }
		public KaardiParameeterTuupContext(KaardiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaardiParameeterTuup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaardiParameeterTuup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaardiParameeterTuup(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KaardiParameeterPunktidContext extends KaardiParameeterContext {
		public AritmeetilineAvaldisPunktideJarjendContext aritmeetilineAvaldisPunktideJarjend() {
			return getRuleContext(AritmeetilineAvaldisPunktideJarjendContext.class,0);
		}
		public KaardiParameeterPunktidContext(KaardiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaardiParameeterPunktid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaardiParameeterPunktid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaardiParameeterPunktid(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KaardiParameeterAladContext extends KaardiParameeterContext {
		public AritmeetilineAvaldisAladeJarjendContext aritmeetilineAvaldisAladeJarjend() {
			return getRuleContext(AritmeetilineAvaldisAladeJarjendContext.class,0);
		}
		public KaardiParameeterAladContext(KaardiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaardiParameeterAlad(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaardiParameeterAlad(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaardiParameeterAlad(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KaardiParameeterNimiContext extends KaardiParameeterContext {
		public SoneContext sone() {
			return getRuleContext(SoneContext.class,0);
		}
		public KaardiParameeterNimiContext(KaardiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaardiParameeterNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaardiParameeterNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaardiParameeterNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KaardiParameeterKaardidContext extends KaardiParameeterContext {
		public AritmeetilineAvaldisKaartideJarjendContext aritmeetilineAvaldisKaartideJarjend() {
			return getRuleContext(AritmeetilineAvaldisKaartideJarjendContext.class,0);
		}
		public KaardiParameeterKaardidContext(KaardiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaardiParameeterKaardid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaardiParameeterKaardid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaardiParameeterKaardid(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KaardiParameeterObjektidContext extends KaardiParameeterContext {
		public AritmeetilineAvaldisObjektideJarjendContext aritmeetilineAvaldisObjektideJarjend() {
			return getRuleContext(AritmeetilineAvaldisObjektideJarjendContext.class,0);
		}
		public KaardiParameeterObjektidContext(KaardiParameeterContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaardiParameeterObjektid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaardiParameeterObjektid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaardiParameeterObjektid(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KaardiParameeterContext kaardiParameeter() throws RecognitionException {
		KaardiParameeterContext _localctx = new KaardiParameeterContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_kaardiParameeter);
		try {
			setState(722);
			switch (_input.LA(1)) {
			case 19:
				_localctx = new KaardiParameeterNimiContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(695); match(19);
				setState(696); sone();
				setState(697); match(44);
				}
				break;
			case 49:
				_localctx = new KaardiParameeterTuupContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(699); match(49);
				setState(700); match(KaardiTuup);
				setState(701); match(44);
				}
				break;
			case 39:
				_localctx = new KaardiParameeterPunktidContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(702); match(39);
				setState(703); aritmeetilineAvaldisPunktideJarjend(0);
				setState(704); match(44);
				}
				break;
			case 21:
				_localctx = new KaardiParameeterObjektidContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(706); match(21);
				setState(707); aritmeetilineAvaldisObjektideJarjend(0);
				setState(708); match(44);
				}
				break;
			case 16:
				_localctx = new KaardiParameeterJoonedContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(710); match(16);
				setState(711); aritmeetilineAvaldisJoonteJarjend(0);
				setState(712); match(44);
				}
				break;
			case 12:
				_localctx = new KaardiParameeterAladContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(714); match(12);
				setState(715); aritmeetilineAvaldisAladeJarjend(0);
				setState(716); match(44);
				}
				break;
			case 24:
				_localctx = new KaardiParameeterKaardidContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(718); match(24);
				setState(719); aritmeetilineAvaldisKaartideJarjend(0);
				setState(720); match(44);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArvudeJarjendContext extends ParserRuleContext {
		public AritmeetilineAvaldisArvContext aritmeetilineAvaldisArv(int i) {
			return getRuleContext(AritmeetilineAvaldisArvContext.class,i);
		}
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public List<AritmeetilineAvaldisArvContext> aritmeetilineAvaldisArv() {
			return getRuleContexts(AritmeetilineAvaldisArvContext.class);
		}
		public ArvudeJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arvudeJarjend; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterArvudeJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitArvudeJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitArvudeJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArvudeJarjendContext arvudeJarjend() throws RecognitionException {
		ArvudeJarjendContext _localctx = new ArvudeJarjendContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_arvudeJarjend);
		int _la;
		try {
			setState(736);
			switch (_input.LA(1)) {
			case 34:
				enterOuterAlt(_localctx, 1);
				{
				setState(724); match(34);
				setState(725); aritmeetilineAvaldisArv();
				setState(730);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(726); match(27);
					setState(727); aritmeetilineAvaldisArv();
					}
					}
					setState(732);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(733); match(9);
				}
				break;
			case MuutujaNimi:
				enterOuterAlt(_localctx, 2);
				{
				setState(735); match(MuutujaNimi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KoordinaatideJarjendContext extends ParserRuleContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public AritmeetilineAvaldisPikkuskoordinaatContext aritmeetilineAvaldisPikkuskoordinaat(int i) {
			return getRuleContext(AritmeetilineAvaldisPikkuskoordinaatContext.class,i);
		}
		public List<AritmeetilineAvaldisPikkuskoordinaatContext> aritmeetilineAvaldisPikkuskoordinaat() {
			return getRuleContexts(AritmeetilineAvaldisPikkuskoordinaatContext.class);
		}
		public List<AritmeetilineAvaldisLaiuskoordinaatContext> aritmeetilineAvaldisLaiuskoordinaat() {
			return getRuleContexts(AritmeetilineAvaldisLaiuskoordinaatContext.class);
		}
		public AritmeetilineAvaldisLaiuskoordinaatContext aritmeetilineAvaldisLaiuskoordinaat(int i) {
			return getRuleContext(AritmeetilineAvaldisLaiuskoordinaatContext.class,i);
		}
		public KoordinaatideJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_koordinaatideJarjend; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKoordinaatideJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKoordinaatideJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKoordinaatideJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KoordinaatideJarjendContext koordinaatideJarjend() throws RecognitionException {
		KoordinaatideJarjendContext _localctx = new KoordinaatideJarjendContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_koordinaatideJarjend);
		int _la;
		try {
			setState(761);
			switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(738); match(34);
				setState(739); aritmeetilineAvaldisLaiuskoordinaat(0);
				setState(744);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(740); match(27);
					setState(741); aritmeetilineAvaldisLaiuskoordinaat(0);
					}
					}
					setState(746);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(747); match(9);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(749); match(34);
				setState(750); aritmeetilineAvaldisPikkuskoordinaat(0);
				setState(755);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(751); match(27);
					setState(752); aritmeetilineAvaldisPikkuskoordinaat(0);
					}
					}
					setState(757);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(758); match(9);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(760); match(MuutujaNimi);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ToevaartusteJarjendContext extends ParserRuleContext {
		public List<VordlusAvaldisContext> vordlusAvaldis() {
			return getRuleContexts(VordlusAvaldisContext.class);
		}
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public VordlusAvaldisContext vordlusAvaldis(int i) {
			return getRuleContext(VordlusAvaldisContext.class,i);
		}
		public ToevaartusteJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_toevaartusteJarjend; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterToevaartusteJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitToevaartusteJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitToevaartusteJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ToevaartusteJarjendContext toevaartusteJarjend() throws RecognitionException {
		ToevaartusteJarjendContext _localctx = new ToevaartusteJarjendContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_toevaartusteJarjend);
		int _la;
		try {
			setState(775);
			switch (_input.LA(1)) {
			case 34:
				enterOuterAlt(_localctx, 1);
				{
				setState(763); match(34);
				setState(764); vordlusAvaldis(0);
				setState(769);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(765); match(27);
					setState(766); vordlusAvaldis(0);
					}
					}
					setState(771);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(772); match(9);
				}
				break;
			case MuutujaNimi:
				enterOuterAlt(_localctx, 2);
				{
				setState(774); match(MuutujaNimi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SonedeJarjendContext extends ParserRuleContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public AritmeetilineAvaldisSoneContext aritmeetilineAvaldisSone(int i) {
			return getRuleContext(AritmeetilineAvaldisSoneContext.class,i);
		}
		public List<AritmeetilineAvaldisSoneContext> aritmeetilineAvaldisSone() {
			return getRuleContexts(AritmeetilineAvaldisSoneContext.class);
		}
		public SonedeJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sonedeJarjend; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterSonedeJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitSonedeJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitSonedeJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SonedeJarjendContext sonedeJarjend() throws RecognitionException {
		SonedeJarjendContext _localctx = new SonedeJarjendContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_sonedeJarjend);
		int _la;
		try {
			setState(789);
			switch (_input.LA(1)) {
			case 34:
				enterOuterAlt(_localctx, 1);
				{
				setState(777); match(34);
				setState(778); aritmeetilineAvaldisSone();
				setState(783);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(779); match(27);
					setState(780); aritmeetilineAvaldisSone();
					}
					}
					setState(785);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(786); match(9);
				}
				break;
			case MuutujaNimi:
				enterOuterAlt(_localctx, 2);
				{
				setState(788); match(MuutujaNimi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StruktuurideJarjendContext extends ParserRuleContext {
		public List<TerminalNode> MuutujaNimi() { return getTokens(SmapsParser.MuutujaNimi); }
		public TerminalNode MuutujaNimi(int i) {
			return getToken(SmapsParser.MuutujaNimi, i);
		}
		public StruktuurideJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struktuurideJarjend; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterStruktuurideJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitStruktuurideJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitStruktuurideJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StruktuurideJarjendContext struktuurideJarjend() throws RecognitionException {
		StruktuurideJarjendContext _localctx = new StruktuurideJarjendContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_struktuurideJarjend);
		int _la;
		try {
			setState(802);
			switch (_input.LA(1)) {
			case 34:
				enterOuterAlt(_localctx, 1);
				{
				setState(791); match(34);
				setState(792); match(MuutujaNimi);
				setState(797);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(793); match(27);
					setState(794); match(MuutujaNimi);
					}
					}
					setState(799);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(800); match(9);
				}
				break;
			case MuutujaNimi:
				enterOuterAlt(_localctx, 2);
				{
				setState(801); match(MuutujaNimi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PunktideJarjendContext extends ParserRuleContext {
		public PunktideJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_punktideJarjend; }
	 
		public PunktideJarjendContext() { }
		public void copyFrom(PunktideJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PunktideJarjendJarjendContext extends PunktideJarjendContext {
		public PunktContext punkt(int i) {
			return getRuleContext(PunktContext.class,i);
		}
		public List<PunktContext> punkt() {
			return getRuleContexts(PunktContext.class);
		}
		public PunktideJarjendJarjendContext(PunktideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktideJarjendJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktideJarjendJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktideJarjendJarjend(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PunktideJarjendMuutujaNimiContext extends PunktideJarjendContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public PunktideJarjendMuutujaNimiContext(PunktideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktideJarjendMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktideJarjendMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktideJarjendMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PunktideJarjendContext punktideJarjend() throws RecognitionException {
		PunktideJarjendContext _localctx = new PunktideJarjendContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_punktideJarjend);
		int _la;
		try {
			setState(816);
			switch (_input.LA(1)) {
			case 34:
				_localctx = new PunktideJarjendJarjendContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(804); match(34);
				setState(805); punkt();
				setState(810);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(806); match(27);
					setState(807); punkt();
					}
					}
					setState(812);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(813); match(9);
				}
				break;
			case MuutujaNimi:
				_localctx = new PunktideJarjendMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(815); match(MuutujaNimi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjektideJarjendContext extends ParserRuleContext {
		public ObjektideJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objektideJarjend; }
	 
		public ObjektideJarjendContext() { }
		public void copyFrom(ObjektideJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ObjektideJarjendMuutujaNimiContext extends ObjektideJarjendContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public ObjektideJarjendMuutujaNimiContext(ObjektideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterObjektideJarjendMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitObjektideJarjendMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitObjektideJarjendMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ObjektideJarjendJarjendContext extends ObjektideJarjendContext {
		public List<TerminalNode> MuutujaNimi() { return getTokens(SmapsParser.MuutujaNimi); }
		public TerminalNode MuutujaNimi(int i) {
			return getToken(SmapsParser.MuutujaNimi, i);
		}
		public ObjektideJarjendJarjendContext(ObjektideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterObjektideJarjendJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitObjektideJarjendJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitObjektideJarjendJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ObjektideJarjendContext objektideJarjend() throws RecognitionException {
		ObjektideJarjendContext _localctx = new ObjektideJarjendContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_objektideJarjend);
		int _la;
		try {
			setState(829);
			switch (_input.LA(1)) {
			case 34:
				_localctx = new ObjektideJarjendJarjendContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(818); match(34);
				setState(819); match(MuutujaNimi);
				setState(824);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(820); match(27);
					setState(821); match(MuutujaNimi);
					}
					}
					setState(826);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(827); match(9);
				}
				break;
			case MuutujaNimi:
				_localctx = new ObjektideJarjendMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(828); match(MuutujaNimi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JoonteJarjendContext extends ParserRuleContext {
		public JoonteJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_joonteJarjend; }
	 
		public JoonteJarjendContext() { }
		public void copyFrom(JoonteJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class JoonteJarjendMuutujaNimiContext extends JoonteJarjendContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public JoonteJarjendMuutujaNimiContext(JoonteJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterJoonteJarjendMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitJoonteJarjendMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitJoonteJarjendMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class JoonteJarjendJarjendContext extends JoonteJarjendContext {
		public List<TerminalNode> MuutujaNimi() { return getTokens(SmapsParser.MuutujaNimi); }
		public TerminalNode MuutujaNimi(int i) {
			return getToken(SmapsParser.MuutujaNimi, i);
		}
		public JoonteJarjendJarjendContext(JoonteJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterJoonteJarjendJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitJoonteJarjendJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitJoonteJarjendJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JoonteJarjendContext joonteJarjend() throws RecognitionException {
		JoonteJarjendContext _localctx = new JoonteJarjendContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_joonteJarjend);
		int _la;
		try {
			setState(842);
			switch (_input.LA(1)) {
			case 34:
				_localctx = new JoonteJarjendJarjendContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(831); match(34);
				setState(832); match(MuutujaNimi);
				setState(837);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(833); match(27);
					setState(834); match(MuutujaNimi);
					}
					}
					setState(839);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(840); match(9);
				}
				break;
			case MuutujaNimi:
				_localctx = new JoonteJarjendMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(841); match(MuutujaNimi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AladeJarjendContext extends ParserRuleContext {
		public AladeJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aladeJarjend; }
	 
		public AladeJarjendContext() { }
		public void copyFrom(AladeJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AladeJarjendMuutujaNimiContext extends AladeJarjendContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public AladeJarjendMuutujaNimiContext(AladeJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterAladeJarjendMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitAladeJarjendMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitAladeJarjendMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AladeJarjendJarjendContext extends AladeJarjendContext {
		public List<TerminalNode> MuutujaNimi() { return getTokens(SmapsParser.MuutujaNimi); }
		public TerminalNode MuutujaNimi(int i) {
			return getToken(SmapsParser.MuutujaNimi, i);
		}
		public AladeJarjendJarjendContext(AladeJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterAladeJarjendJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitAladeJarjendJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitAladeJarjendJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AladeJarjendContext aladeJarjend() throws RecognitionException {
		AladeJarjendContext _localctx = new AladeJarjendContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_aladeJarjend);
		int _la;
		try {
			setState(855);
			switch (_input.LA(1)) {
			case 34:
				_localctx = new AladeJarjendJarjendContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(844); match(34);
				setState(845); match(MuutujaNimi);
				setState(850);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(846); match(27);
					setState(847); match(MuutujaNimi);
					}
					}
					setState(852);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(853); match(9);
				}
				break;
			case MuutujaNimi:
				_localctx = new AladeJarjendMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(854); match(MuutujaNimi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KaartideJarjendContext extends ParserRuleContext {
		public KaartideJarjendContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kaartideJarjend; }
	 
		public KaartideJarjendContext() { }
		public void copyFrom(KaartideJarjendContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class KaartideJarjendMuutujaNimiContext extends KaartideJarjendContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public KaartideJarjendMuutujaNimiContext(KaartideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaartideJarjendMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaartideJarjendMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaartideJarjendMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class KaartideJarjendJarjendContext extends KaartideJarjendContext {
		public List<TerminalNode> MuutujaNimi() { return getTokens(SmapsParser.MuutujaNimi); }
		public TerminalNode MuutujaNimi(int i) {
			return getToken(SmapsParser.MuutujaNimi, i);
		}
		public KaartideJarjendJarjendContext(KaartideJarjendContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterKaartideJarjendJarjend(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitKaartideJarjendJarjend(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitKaartideJarjendJarjend(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KaartideJarjendContext kaartideJarjend() throws RecognitionException {
		KaartideJarjendContext _localctx = new KaartideJarjendContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_kaartideJarjend);
		int _la;
		try {
			setState(868);
			switch (_input.LA(1)) {
			case 34:
				_localctx = new KaartideJarjendJarjendContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(857); match(34);
				setState(858); match(MuutujaNimi);
				setState(863);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(859); match(27);
					setState(860); match(MuutujaNimi);
					}
					}
					setState(865);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(866); match(9);
				}
				break;
			case MuutujaNimi:
				_localctx = new KaartideJarjendMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(867); match(MuutujaNimi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PunktiKoordinaadidContext extends ParserRuleContext {
		public PunktiKoordinaadidContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_punktiKoordinaadid; }
	 
		public PunktiKoordinaadidContext() { }
		public void copyFrom(PunktiKoordinaadidContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PunktiKoordinaadidMuutujaNimiContext extends PunktiKoordinaadidContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public PunktiKoordinaadidMuutujaNimiContext(PunktiKoordinaadidContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktiKoordinaadidMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktiKoordinaadidMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktiKoordinaadidMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PunktiKoordinaadidPunktContext extends PunktiKoordinaadidContext {
		public PunktContext punkt() {
			return getRuleContext(PunktContext.class,0);
		}
		public PunktiKoordinaadidPunktContext(PunktiKoordinaadidContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktiKoordinaadidPunkt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktiKoordinaadidPunkt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktiKoordinaadidPunkt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PunktiKoordinaadidContext punktiKoordinaadid() throws RecognitionException {
		PunktiKoordinaadidContext _localctx = new PunktiKoordinaadidContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_punktiKoordinaadid);
		int _la;
		try {
			setState(876);
			switch ( getInterpreter().adaptivePredict(_input,71,_ctx) ) {
			case 1:
				_localctx = new PunktiKoordinaadidPunktContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(870); punkt();
				}
				break;

			case 2:
				_localctx = new PunktiKoordinaadidMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(871); match(MuutujaNimi);
				setState(874);
				_la = _input.LA(1);
				if (_la==40) {
					{
					setState(872); match(40);
					setState(873);
					_la = _input.LA(1);
					if ( !(_la==3 || _la==26) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SoneContext extends ParserRuleContext {
		public SoneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sone; }
	 
		public SoneContext() { }
		public void copyFrom(SoneContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SoneSoneContext extends SoneContext {
		public TerminalNode Sone() { return getToken(SmapsParser.Sone, 0); }
		public SoneSoneContext(SoneContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterSoneSone(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitSoneSone(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitSoneSone(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SoneMuutujaNimiContext extends SoneContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public SoneMuutujaNimiContext(SoneContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterSoneMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitSoneMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitSoneMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SoneContext sone() throws RecognitionException {
		SoneContext _localctx = new SoneContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_sone);
		try {
			setState(880);
			switch (_input.LA(1)) {
			case MuutujaNimi:
				_localctx = new SoneMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(878); match(MuutujaNimi);
				}
				break;
			case Sone:
				_localctx = new SoneSoneContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(879); match(Sone);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PunktContext extends ParserRuleContext {
		public PunktContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_punkt; }
	 
		public PunktContext() { }
		public void copyFrom(PunktContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PunktKoordinaadidContext extends PunktContext {
		public LaiuskoordinaatContext laiuskoordinaat() {
			return getRuleContext(LaiuskoordinaatContext.class,0);
		}
		public PikkuskoordinaatContext pikkuskoordinaat() {
			return getRuleContext(PikkuskoordinaatContext.class,0);
		}
		public PunktKoordinaadidContext(PunktContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktKoordinaadid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktKoordinaadid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktKoordinaadid(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PunktMuutujaNimiContext extends PunktContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public PunktMuutujaNimiContext(PunktContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPunktMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPunktMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPunktMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PunktContext punkt() throws RecognitionException {
		PunktContext _localctx = new PunktContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_punkt);
		try {
			setState(889);
			switch (_input.LA(1)) {
			case 34:
				_localctx = new PunktKoordinaadidContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(882); match(34);
				setState(883); laiuskoordinaat();
				setState(884); match(27);
				setState(885); pikkuskoordinaat();
				setState(886); match(9);
				}
				break;
			case MuutujaNimi:
				_localctx = new PunktMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(888); match(MuutujaNimi);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LaiuskoordinaatContext extends ParserRuleContext {
		public LaiuskoordinaatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_laiuskoordinaat; }
	 
		public LaiuskoordinaatContext() { }
		public void copyFrom(LaiuskoordinaatContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LaiuskoordinaatMuutujaNimiContext extends LaiuskoordinaatContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public LaiuskoordinaatMuutujaNimiContext(LaiuskoordinaatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLaiuskoordinaatMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLaiuskoordinaatMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLaiuskoordinaatMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LaiuskoordinaatLaiuskraadContext extends LaiuskoordinaatContext {
		public LaiuskraadContext laiuskraad() {
			return getRuleContext(LaiuskraadContext.class,0);
		}
		public LaiuskoordinaatLaiuskraadContext(LaiuskoordinaatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLaiuskoordinaatLaiuskraad(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLaiuskoordinaatLaiuskraad(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLaiuskoordinaatLaiuskraad(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LaiuskoordinaatContext laiuskoordinaat() throws RecognitionException {
		LaiuskoordinaatContext _localctx = new LaiuskoordinaatContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_laiuskoordinaat);
		try {
			setState(897);
			switch (_input.LA(1)) {
			case Pohi:
			case Louna:
			case Taisarv:
			case Ujupunktarv:
				_localctx = new LaiuskoordinaatLaiuskraadContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(891); laiuskraad();
				}
				break;
			case MuutujaNimi:
				_localctx = new LaiuskoordinaatMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(892); match(MuutujaNimi);
				setState(895);
				switch ( getInterpreter().adaptivePredict(_input,74,_ctx) ) {
				case 1:
					{
					setState(893); match(40);
					setState(894); match(25);
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PikkuskoordinaatContext extends ParserRuleContext {
		public PikkuskoordinaatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pikkuskoordinaat; }
	 
		public PikkuskoordinaatContext() { }
		public void copyFrom(PikkuskoordinaatContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PikkuskoordinaatMuutujaNimiContext extends PikkuskoordinaatContext {
		public TerminalNode MuutujaNimi() { return getToken(SmapsParser.MuutujaNimi, 0); }
		public PikkuskoordinaatMuutujaNimiContext(PikkuskoordinaatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPikkuskoordinaatMuutujaNimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPikkuskoordinaatMuutujaNimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPikkuskoordinaatMuutujaNimi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PikkuskoordinaatPikkuskraadContext extends PikkuskoordinaatContext {
		public LaiuskraadContext laiuskraad() {
			return getRuleContext(LaiuskraadContext.class,0);
		}
		public PikkuskoordinaatPikkuskraadContext(PikkuskoordinaatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPikkuskoordinaatPikkuskraad(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPikkuskoordinaatPikkuskraad(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPikkuskoordinaatPikkuskraad(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PikkuskoordinaatContext pikkuskoordinaat() throws RecognitionException {
		PikkuskoordinaatContext _localctx = new PikkuskoordinaatContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_pikkuskoordinaat);
		try {
			setState(905);
			switch (_input.LA(1)) {
			case Pohi:
			case Louna:
			case Taisarv:
			case Ujupunktarv:
				_localctx = new PikkuskoordinaatPikkuskraadContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(899); laiuskraad();
				}
				break;
			case MuutujaNimi:
				_localctx = new PikkuskoordinaatMuutujaNimiContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(900); match(MuutujaNimi);
				setState(903);
				switch ( getInterpreter().adaptivePredict(_input,76,_ctx) ) {
				case 1:
					{
					setState(901); match(40);
					setState(902); match(25);
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LaiuskraadContext extends ParserRuleContext {
		public LaiuskraadContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_laiuskraad; }
	 
		public LaiuskraadContext() { }
		public void copyFrom(LaiuskraadContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Laiuskraad1Context extends LaiuskraadContext {
		public LaiuskraadiTahisContext laiuskraadiTahis() {
			return getRuleContext(LaiuskraadiTahisContext.class,0);
		}
		public TerminalNode Ujupunktarv() { return getToken(SmapsParser.Ujupunktarv, 0); }
		public Laiuskraad1Context(LaiuskraadContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLaiuskraad1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLaiuskraad1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLaiuskraad1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Laiuskraad2Context extends LaiuskraadContext {
		public TerminalNode Taisarv() { return getToken(SmapsParser.Taisarv, 0); }
		public LaiuskraadiTahisContext laiuskraadiTahis() {
			return getRuleContext(LaiuskraadiTahisContext.class,0);
		}
		public Laiuskraad2Context(LaiuskraadContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLaiuskraad2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLaiuskraad2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLaiuskraad2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Laiuskraad3Context extends LaiuskraadContext {
		public LaiuskraadiTahisContext laiuskraadiTahis() {
			return getRuleContext(LaiuskraadiTahisContext.class,0);
		}
		public TerminalNode Ujupunktarv() { return getToken(SmapsParser.Ujupunktarv, 0); }
		public Laiuskraad3Context(LaiuskraadContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLaiuskraad3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLaiuskraad3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLaiuskraad3(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Laiuskraad4Context extends LaiuskraadContext {
		public LaiuskraadiTahisContext laiuskraadiTahis() {
			return getRuleContext(LaiuskraadiTahisContext.class,0);
		}
		public TerminalNode Taisarv() { return getToken(SmapsParser.Taisarv, 0); }
		public Laiuskraad4Context(LaiuskraadContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLaiuskraad4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLaiuskraad4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLaiuskraad4(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LaiuskraadContext laiuskraad() throws RecognitionException {
		LaiuskraadContext _localctx = new LaiuskraadContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_laiuskraad);
		try {
			setState(917);
			switch ( getInterpreter().adaptivePredict(_input,78,_ctx) ) {
			case 1:
				_localctx = new Laiuskraad1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(907); laiuskraadiTahis();
				setState(908); match(Ujupunktarv);
				}
				break;

			case 2:
				_localctx = new Laiuskraad2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(910); laiuskraadiTahis();
				setState(911); match(Taisarv);
				}
				break;

			case 3:
				_localctx = new Laiuskraad3Context(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(913); match(Ujupunktarv);
				setState(914); laiuskraadiTahis();
				}
				break;

			case 4:
				_localctx = new Laiuskraad4Context(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(915); match(Taisarv);
				setState(916); laiuskraadiTahis();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PikkuskraadContext extends ParserRuleContext {
		public PikkuskraadContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pikkuskraad; }
	 
		public PikkuskraadContext() { }
		public void copyFrom(PikkuskraadContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Pikkuskraad1Context extends PikkuskraadContext {
		public PikkuskraadiTahisContext pikkuskraadiTahis() {
			return getRuleContext(PikkuskraadiTahisContext.class,0);
		}
		public TerminalNode Ujupunktarv() { return getToken(SmapsParser.Ujupunktarv, 0); }
		public Pikkuskraad1Context(PikkuskraadContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPikkuskraad1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPikkuskraad1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPikkuskraad1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Pikkuskraad4Context extends PikkuskraadContext {
		public TerminalNode Taisarv() { return getToken(SmapsParser.Taisarv, 0); }
		public PikkuskraadiTahisContext pikkuskraadiTahis() {
			return getRuleContext(PikkuskraadiTahisContext.class,0);
		}
		public Pikkuskraad4Context(PikkuskraadContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPikkuskraad4(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPikkuskraad4(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPikkuskraad4(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Pikkuskraad2Context extends PikkuskraadContext {
		public TerminalNode Taisarv() { return getToken(SmapsParser.Taisarv, 0); }
		public PikkuskraadiTahisContext pikkuskraadiTahis() {
			return getRuleContext(PikkuskraadiTahisContext.class,0);
		}
		public Pikkuskraad2Context(PikkuskraadContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPikkuskraad2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPikkuskraad2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPikkuskraad2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Pikkuskraad3Context extends PikkuskraadContext {
		public PikkuskraadiTahisContext pikkuskraadiTahis() {
			return getRuleContext(PikkuskraadiTahisContext.class,0);
		}
		public TerminalNode Ujupunktarv() { return getToken(SmapsParser.Ujupunktarv, 0); }
		public Pikkuskraad3Context(PikkuskraadContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPikkuskraad3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPikkuskraad3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPikkuskraad3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PikkuskraadContext pikkuskraad() throws RecognitionException {
		PikkuskraadContext _localctx = new PikkuskraadContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_pikkuskraad);
		try {
			setState(929);
			switch ( getInterpreter().adaptivePredict(_input,79,_ctx) ) {
			case 1:
				_localctx = new Pikkuskraad1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(919); pikkuskraadiTahis();
				setState(920); match(Ujupunktarv);
				}
				break;

			case 2:
				_localctx = new Pikkuskraad2Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(922); pikkuskraadiTahis();
				setState(923); match(Taisarv);
				}
				break;

			case 3:
				_localctx = new Pikkuskraad3Context(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(925); match(Ujupunktarv);
				setState(926); pikkuskraadiTahis();
				}
				break;

			case 4:
				_localctx = new Pikkuskraad4Context(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(927); match(Taisarv);
				setState(928); pikkuskraadiTahis();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LaiuskraadiTahisContext extends ParserRuleContext {
		public LaiuskraadiTahisContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_laiuskraadiTahis; }
	 
		public LaiuskraadiTahisContext() { }
		public void copyFrom(LaiuskraadiTahisContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LaiuskraadPohiContext extends LaiuskraadiTahisContext {
		public TerminalNode Pohi() { return getToken(SmapsParser.Pohi, 0); }
		public LaiuskraadPohiContext(LaiuskraadiTahisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLaiuskraadPohi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLaiuskraadPohi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLaiuskraadPohi(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LaiuskraadLounaContext extends LaiuskraadiTahisContext {
		public TerminalNode Louna() { return getToken(SmapsParser.Louna, 0); }
		public LaiuskraadLounaContext(LaiuskraadiTahisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterLaiuskraadLouna(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitLaiuskraadLouna(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitLaiuskraadLouna(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LaiuskraadiTahisContext laiuskraadiTahis() throws RecognitionException {
		LaiuskraadiTahisContext _localctx = new LaiuskraadiTahisContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_laiuskraadiTahis);
		try {
			setState(933);
			switch (_input.LA(1)) {
			case Pohi:
				_localctx = new LaiuskraadPohiContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(931); match(Pohi);
				}
				break;
			case Louna:
				_localctx = new LaiuskraadLounaContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(932); match(Louna);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PikkuskraadiTahisContext extends ParserRuleContext {
		public PikkuskraadiTahisContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pikkuskraadiTahis; }
	 
		public PikkuskraadiTahisContext() { }
		public void copyFrom(PikkuskraadiTahisContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PikkuskraadIdaContext extends PikkuskraadiTahisContext {
		public TerminalNode Ida() { return getToken(SmapsParser.Ida, 0); }
		public PikkuskraadIdaContext(PikkuskraadiTahisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPikkuskraadIda(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPikkuskraadIda(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPikkuskraadIda(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PikkuskraadLaasContext extends PikkuskraadiTahisContext {
		public TerminalNode Laas() { return getToken(SmapsParser.Laas, 0); }
		public PikkuskraadLaasContext(PikkuskraadiTahisContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).enterPikkuskraadLaas(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SmapsListener ) ((SmapsListener)listener).exitPikkuskraadLaas(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SmapsVisitor ) return ((SmapsVisitor<? extends T>)visitor).visitPikkuskraadLaas(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PikkuskraadiTahisContext pikkuskraadiTahis() throws RecognitionException {
		PikkuskraadiTahisContext _localctx = new PikkuskraadiTahisContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_pikkuskraadiTahis);
		try {
			setState(937);
			switch (_input.LA(1)) {
			case Ida:
				_localctx = new PikkuskraadIdaContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(935); match(Ida);
				}
				break;
			case Laas:
				_localctx = new PikkuskraadLaasContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(936); match(Laas);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 11: return vordlusAvaldis_sempred((VordlusAvaldisContext)_localctx, predIndex);

		case 13: return aritmeetilineAvaldisArv3_sempred((AritmeetilineAvaldisArv3Context)_localctx, predIndex);

		case 14: return aritmeetilineAvaldisArv2_sempred((AritmeetilineAvaldisArv2Context)_localctx, predIndex);

		case 17: return aritmeetilineAvaldisLaiuskoordinaat_sempred((AritmeetilineAvaldisLaiuskoordinaatContext)_localctx, predIndex);

		case 18: return aritmeetilineAvaldisPikkuskoordinaat_sempred((AritmeetilineAvaldisPikkuskoordinaatContext)_localctx, predIndex);

		case 20: return aritmeetilineAvaldisPunktideJarjend_sempred((AritmeetilineAvaldisPunktideJarjendContext)_localctx, predIndex);

		case 21: return aritmeetilineAvaldisObjektideJarjend_sempred((AritmeetilineAvaldisObjektideJarjendContext)_localctx, predIndex);

		case 22: return aritmeetilineAvaldisJoonteJarjend_sempred((AritmeetilineAvaldisJoonteJarjendContext)_localctx, predIndex);

		case 23: return aritmeetilineAvaldisAladeJarjend_sempred((AritmeetilineAvaldisAladeJarjendContext)_localctx, predIndex);

		case 24: return aritmeetilineAvaldisKaartideJarjend_sempred((AritmeetilineAvaldisKaartideJarjendContext)_localctx, predIndex);

		case 25: return aritmeetilineAvaldisArvudeJarjend_sempred((AritmeetilineAvaldisArvudeJarjendContext)_localctx, predIndex);

		case 26: return aritmeetilineAvaldisKoordinaatideJarjend_sempred((AritmeetilineAvaldisKoordinaatideJarjendContext)_localctx, predIndex);

		case 27: return aritmeetilineAvaldisToevaartusteJarjend_sempred((AritmeetilineAvaldisToevaartusteJarjendContext)_localctx, predIndex);

		case 28: return aritmeetilineAvaldisSonedeJarjend_sempred((AritmeetilineAvaldisSonedeJarjendContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisToevaartusteJarjend_sempred(AritmeetilineAvaldisToevaartusteJarjendContext _localctx, int predIndex) {
		switch (predIndex) {
		case 12: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisPunktideJarjend_sempred(AritmeetilineAvaldisPunktideJarjendContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisArvudeJarjend_sempred(AritmeetilineAvaldisArvudeJarjendContext _localctx, int predIndex) {
		switch (predIndex) {
		case 10: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisPikkuskoordinaat_sempred(AritmeetilineAvaldisPikkuskoordinaatContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisAladeJarjend_sempred(AritmeetilineAvaldisAladeJarjendContext _localctx, int predIndex) {
		switch (predIndex) {
		case 8: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisKoordinaatideJarjend_sempred(AritmeetilineAvaldisKoordinaatideJarjendContext _localctx, int predIndex) {
		switch (predIndex) {
		case 11: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisArv3_sempred(AritmeetilineAvaldisArv3Context _localctx, int predIndex) {
		switch (predIndex) {
		case 1: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean vordlusAvaldis_sempred(VordlusAvaldisContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisArv2_sempred(AritmeetilineAvaldisArv2Context _localctx, int predIndex) {
		switch (predIndex) {
		case 2: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisJoonteJarjend_sempred(AritmeetilineAvaldisJoonteJarjendContext _localctx, int predIndex) {
		switch (predIndex) {
		case 7: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisSonedeJarjend_sempred(AritmeetilineAvaldisSonedeJarjendContext _localctx, int predIndex) {
		switch (predIndex) {
		case 13: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisLaiuskoordinaat_sempred(AritmeetilineAvaldisLaiuskoordinaatContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisObjektideJarjend_sempred(AritmeetilineAvaldisObjektideJarjendContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6: return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean aritmeetilineAvaldisKaartideJarjend_sempred(AritmeetilineAvaldisKaartideJarjendContext _localctx, int predIndex) {
		switch (predIndex) {
		case 9: return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3I\u03ae\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\3\2\7\2n\n\2\f\2\16\2q\13\2\3\3\3\3\3\3\3\3\3"+
		"\3\5\3x\n\3\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u0087"+
		"\n\5\3\6\3\6\3\6\3\6\7\6\u008d\n\6\f\6\16\6\u0090\13\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\7\6\u00a1\n\6\f\6\16\6\u00a4"+
		"\13\6\3\6\3\6\3\6\3\6\3\6\7\6\u00ab\n\6\f\6\16\6\u00ae\13\6\3\6\3\6\3"+
		"\6\3\6\3\6\7\6\u00b5\n\6\f\6\16\6\u00b8\13\6\3\6\3\6\3\6\3\6\3\6\7\6\u00bf"+
		"\n\6\f\6\16\6\u00c2\13\6\3\6\5\6\u00c5\n\6\3\7\5\7\u00c8\n\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\5\7\u00d0\n\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u00d8\n\7\3\7"+
		"\3\7\3\7\3\7\5\7\u00de\n\7\3\7\3\7\3\7\5\7\u00e3\n\7\3\7\3\7\3\7\3\7\5"+
		"\7\u00e9\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u0135\n\7\3\b\3\b\7\b\u0139\n\b\f\b\16"+
		"\b\u013c\13\b\3\b\5\b\u013f\n\b\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u0147\n\t"+
		"\f\t\16\t\u014a\13\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13\7\13\u0154\n\13"+
		"\f\13\16\13\u0157\13\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\7\f\u0161\n"+
		"\f\f\f\16\f\u0164\13\f\3\f\3\f\3\f\3\f\3\f\7\f\u016b\n\f\f\f\16\f\u016e"+
		"\13\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\7\f\u0179\n\f\f\f\16\f\u017c"+
		"\13\f\3\f\3\f\3\f\3\f\7\f\u0182\n\f\f\f\16\f\u0185\13\f\3\f\3\f\3\f\7"+
		"\f\u018a\n\f\f\f\16\f\u018d\13\f\3\f\3\f\5\f\u0191\n\f\3\r\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u01c8\n\r"+
		"\3\r\3\r\3\r\7\r\u01cd\n\r\f\r\16\r\u01d0\13\r\3\16\3\16\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\7\17\u01da\n\17\f\17\16\17\u01dd\13\17\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\7\20\u01e5\n\20\f\20\16\20\u01e8\13\20\3\21\3\21\3\21"+
		"\5\21\u01ed\n\21\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u01f5\n\22\3\23\3"+
		"\23\3\23\3\23\3\23\3\23\7\23\u01fd\n\23\f\23\16\23\u0200\13\23\3\24\3"+
		"\24\3\24\3\24\3\24\3\24\7\24\u0208\n\24\f\24\16\24\u020b\13\24\3\25\3"+
		"\25\5\25\u020f\n\25\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u0217\n\26\f\26"+
		"\16\26\u021a\13\26\3\27\3\27\3\27\3\27\3\27\3\27\7\27\u0222\n\27\f\27"+
		"\16\27\u0225\13\27\3\30\3\30\3\30\3\30\3\30\3\30\7\30\u022d\n\30\f\30"+
		"\16\30\u0230\13\30\3\31\3\31\3\31\3\31\3\31\3\31\7\31\u0238\n\31\f\31"+
		"\16\31\u023b\13\31\3\32\3\32\3\32\3\32\3\32\3\32\7\32\u0243\n\32\f\32"+
		"\16\32\u0246\13\32\3\33\3\33\3\33\3\33\3\33\3\33\7\33\u024e\n\33\f\33"+
		"\16\33\u0251\13\33\3\34\3\34\3\34\3\34\3\34\3\34\7\34\u0259\n\34\f\34"+
		"\16\34\u025c\13\34\3\35\3\35\3\35\3\35\3\35\3\35\7\35\u0264\n\35\f\35"+
		"\16\35\u0267\13\35\3\36\3\36\3\36\3\36\3\36\3\36\7\36\u026f\n\36\f\36"+
		"\16\36\u0272\13\36\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3"+
		"\37\3\37\5\37\u0280\n\37\3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 "+
		"\3 \3 \3 \3 \3 \5 \u0296\n \3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!"+
		"\3!\5!\u02a7\n!\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3"+
		"\"\3\"\5\"\u02b8\n\"\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3"+
		"#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\5#\u02d5\n#\3$\3$\3$\3$\7$\u02db\n$\f"+
		"$\16$\u02de\13$\3$\3$\3$\5$\u02e3\n$\3%\3%\3%\3%\7%\u02e9\n%\f%\16%\u02ec"+
		"\13%\3%\3%\3%\3%\3%\3%\7%\u02f4\n%\f%\16%\u02f7\13%\3%\3%\3%\5%\u02fc"+
		"\n%\3&\3&\3&\3&\7&\u0302\n&\f&\16&\u0305\13&\3&\3&\3&\5&\u030a\n&\3\'"+
		"\3\'\3\'\3\'\7\'\u0310\n\'\f\'\16\'\u0313\13\'\3\'\3\'\3\'\5\'\u0318\n"+
		"\'\3(\3(\3(\3(\7(\u031e\n(\f(\16(\u0321\13(\3(\3(\5(\u0325\n(\3)\3)\3"+
		")\3)\7)\u032b\n)\f)\16)\u032e\13)\3)\3)\3)\5)\u0333\n)\3*\3*\3*\3*\7*"+
		"\u0339\n*\f*\16*\u033c\13*\3*\3*\5*\u0340\n*\3+\3+\3+\3+\7+\u0346\n+\f"+
		"+\16+\u0349\13+\3+\3+\5+\u034d\n+\3,\3,\3,\3,\7,\u0353\n,\f,\16,\u0356"+
		"\13,\3,\3,\5,\u035a\n,\3-\3-\3-\3-\7-\u0360\n-\f-\16-\u0363\13-\3-\3-"+
		"\5-\u0367\n-\3.\3.\3.\3.\5.\u036d\n.\5.\u036f\n.\3/\3/\5/\u0373\n/\3\60"+
		"\3\60\3\60\3\60\3\60\3\60\3\60\5\60\u037c\n\60\3\61\3\61\3\61\3\61\5\61"+
		"\u0382\n\61\5\61\u0384\n\61\3\62\3\62\3\62\3\62\5\62\u038a\n\62\5\62\u038c"+
		"\n\62\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\5\63\u0398\n\63"+
		"\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\5\64\u03a4\n\64\3\65"+
		"\3\65\5\65\u03a8\n\65\3\66\3\66\5\66\u03ac\n\66\3\66\2\20\30\34\36$&*"+
		",.\60\62\64\668:\67\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60"+
		"\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhj\2\f\3\2GH\4\2&&((\4\2\f\f\23\23\6"+
		"\2\6\b//\62\62\64\64\4\2\7\7\62\62\4\2\36\36++\5\2\3\3\20\20\61\61\3\2"+
		">?\4\2\5\5\34\34\3\2\t\n\u03ff\2o\3\2\2\2\4w\3\2\2\2\6y\3\2\2\2\b\u0086"+
		"\3\2\2\2\n\u00c4\3\2\2\2\f\u0134\3\2\2\2\16\u0136\3\2\2\2\20\u0140\3\2"+
		"\2\2\22\u014d\3\2\2\2\24\u0150\3\2\2\2\26\u0190\3\2\2\2\30\u01c7\3\2\2"+
		"\2\32\u01d1\3\2\2\2\34\u01d3\3\2\2\2\36\u01de\3\2\2\2 \u01ec\3\2\2\2\""+
		"\u01f4\3\2\2\2$\u01f6\3\2\2\2&\u0201\3\2\2\2(\u020e\3\2\2\2*\u0210\3\2"+
		"\2\2,\u021b\3\2\2\2.\u0226\3\2\2\2\60\u0231\3\2\2\2\62\u023c\3\2\2\2\64"+
		"\u0247\3\2\2\2\66\u0252\3\2\2\28\u025d\3\2\2\2:\u0268\3\2\2\2<\u027f\3"+
		"\2\2\2>\u0295\3\2\2\2@\u02a6\3\2\2\2B\u02b7\3\2\2\2D\u02d4\3\2\2\2F\u02e2"+
		"\3\2\2\2H\u02fb\3\2\2\2J\u0309\3\2\2\2L\u0317\3\2\2\2N\u0324\3\2\2\2P"+
		"\u0332\3\2\2\2R\u033f\3\2\2\2T\u034c\3\2\2\2V\u0359\3\2\2\2X\u0366\3\2"+
		"\2\2Z\u036e\3\2\2\2\\\u0372\3\2\2\2^\u037b\3\2\2\2`\u0383\3\2\2\2b\u038b"+
		"\3\2\2\2d\u0397\3\2\2\2f\u03a3\3\2\2\2h\u03a7\3\2\2\2j\u03ab\3\2\2\2l"+
		"n\5\4\3\2ml\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2p\3\3\2\2\2qo\3\2\2\2"+
		"rx\5\b\5\2sx\5\n\6\2tx\5\f\7\2ux\5\16\b\2vx\5\26\f\2wr\3\2\2\2ws\3\2\2"+
		"\2wt\3\2\2\2wu\3\2\2\2wv\3\2\2\2x\5\3\2\2\2yz\t\2\2\2z\7\3\2\2\2{|\7\67"+
		"\2\2|}\7=\2\2}\u0087\7.\2\2~\177\7\67\2\2\177\u0080\7\21\2\2\u0080\u0081"+
		"\7=\2\2\u0081\u0087\7.\2\2\u0082\u0083\78\2\2\u0083\u0084\7\21\2\2\u0084"+
		"\u0085\7=\2\2\u0085\u0087\7.\2\2\u0086{\3\2\2\2\u0086~\3\2\2\2\u0086\u0082"+
		"\3\2\2\2\u0087\t\3\2\2\2\u0088\u0089\7\66\2\2\u0089\u008a\7=\2\2\u008a"+
		"\u008e\7$\2\2\u008b\u008d\5<\37\2\u008c\u008b\3\2\2\2\u008d\u0090\3\2"+
		"\2\2\u008e\u008c\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u0091\3\2\2\2\u0090"+
		"\u008e\3\2\2\2\u0091\u00c5\7\13\2\2\u0092\u0093\7\66\2\2\u0093\u0094\7"+
		"=\2\2\u0094\u0095\7!\2\2\u0095\u0096\5$\23\2\u0096\u0097\7\35\2\2\u0097"+
		"\u0098\5&\24\2\u0098\u0099\7\35\2\2\u0099\u009a\5(\25\2\u009a\u009b\7"+
		"-\2\2\u009b\u00c5\3\2\2\2\u009c\u009d\7\30\2\2\u009d\u009e\7=\2\2\u009e"+
		"\u00a2\7$\2\2\u009f\u00a1\5> \2\u00a0\u009f\3\2\2\2\u00a1\u00a4\3\2\2"+
		"\2\u00a2\u00a0\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a5\3\2\2\2\u00a4\u00a2"+
		"\3\2\2\2\u00a5\u00c5\7\13\2\2\u00a6\u00a7\7\4\2\2\u00a7\u00a8\7=\2\2\u00a8"+
		"\u00ac\7$\2\2\u00a9\u00ab\5@!\2\u00aa\u00a9\3\2\2\2\u00ab\u00ae\3\2\2"+
		"\2\u00ac\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\u00af\3\2\2\2\u00ae\u00ac"+
		"\3\2\2\2\u00af\u00c5\7\13\2\2\u00b0\u00b1\7%\2\2\u00b1\u00b2\7=\2\2\u00b2"+
		"\u00b6\7$\2\2\u00b3\u00b5\5B\"\2\u00b4\u00b3\3\2\2\2\u00b5\u00b8\3\2\2"+
		"\2\u00b6\u00b4\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b9\3\2\2\2\u00b8\u00b6"+
		"\3\2\2\2\u00b9\u00c5\7\13\2\2\u00ba\u00bb\7\26\2\2\u00bb\u00bc\7=\2\2"+
		"\u00bc\u00c0\7$\2\2\u00bd\u00bf\5D#\2\u00be\u00bd\3\2\2\2\u00bf\u00c2"+
		"\3\2\2\2\u00c0\u00be\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1\u00c3\3\2\2\2\u00c2"+
		"\u00c0\3\2\2\2\u00c3\u00c5\7\13\2\2\u00c4\u0088\3\2\2\2\u00c4\u0092\3"+
		"\2\2\2\u00c4\u009c\3\2\2\2\u00c4\u00a6\3\2\2\2\u00c4\u00b0\3\2\2\2\u00c4"+
		"\u00ba\3\2\2\2\u00c5\13\3\2\2\2\u00c6\u00c8\7#\2\2\u00c7\u00c6\3\2\2\2"+
		"\u00c7\u00c8\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9\u00ca\7=\2\2\u00ca\u00cb"+
		"\7\31\2\2\u00cb\u00cc\5\32\16\2\u00cc\u00cd\7.\2\2\u00cd\u0135\3\2\2\2"+
		"\u00ce\u00d0\7\r\2\2\u00cf\u00ce\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0\u00d1"+
		"\3\2\2\2\u00d1\u00d2\7=\2\2\u00d2\u00d3\7\31\2\2\u00d3\u00d4\5\32\16\2"+
		"\u00d4\u00d5\7.\2\2\u00d5\u0135\3\2\2\2\u00d6\u00d8\t\3\2\2\u00d7\u00d6"+
		"\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00d9\3\2\2\2\u00d9\u00da\7=\2\2\u00da"+
		"\u00dd\7\31\2\2\u00db\u00de\5$\23\2\u00dc\u00de\5&\24\2\u00dd\u00db\3"+
		"\2\2\2\u00dd\u00dc\3\2\2\2\u00de\u00df\3\2\2\2\u00df\u00e0\7.\2\2\u00e0"+
		"\u0135\3\2\2\2\u00e1\u00e3\t\4\2\2\u00e2\u00e1\3\2\2\2\u00e2\u00e3\3\2"+
		"\2\2\u00e3\u00e4\3\2\2\2\u00e4\u00e5\7=\2\2\u00e5\u00e6\7\31\2\2\u00e6"+
		"\u0135\5\30\r\2\u00e7\u00e9\7\60\2\2\u00e8\u00e7\3\2\2\2\u00e8\u00e9\3"+
		"\2\2\2\u00e9\u00ea\3\2\2\2\u00ea\u00eb\7=\2\2\u00eb\u00ec\7\31\2\2\u00ec"+
		"\u00ed\7A\2\2\u00ed\u0135\7.\2\2\u00ee\u00ef\7#\2\2\u00ef\u00f0\7\21\2"+
		"\2\u00f0\u00f1\7=\2\2\u00f1\u00f2\7\31\2\2\u00f2\u00f3\5\64\33\2\u00f3"+
		"\u00f4\7.\2\2\u00f4\u0135\3\2\2\2\u00f5\u00f6\7\r\2\2\u00f6\u00f7\7\21"+
		"\2\2\u00f7\u00f8\7=\2\2\u00f8\u00f9\7\31\2\2\u00f9\u00fa\5\64\33\2\u00fa"+
		"\u00fb\7.\2\2\u00fb\u0135\3\2\2\2\u00fc\u00fd\t\3\2\2\u00fd\u00fe\7\21"+
		"\2\2\u00fe\u00ff\7=\2\2\u00ff\u0100\7\31\2\2\u0100\u0101\5\66\34\2\u0101"+
		"\u0102\7.\2\2\u0102\u0135\3\2\2\2\u0103\u0104\t\4\2\2\u0104\u0105\7\21"+
		"\2\2\u0105\u0106\7=\2\2\u0106\u0107\7\31\2\2\u0107\u0108\58\35\2\u0108"+
		"\u0109\7.\2\2\u0109\u0135\3\2\2\2\u010a\u010b\7\60\2\2\u010b\u010c\7\21"+
		"\2\2\u010c\u010d\7=\2\2\u010d\u010e\7\31\2\2\u010e\u010f\5:\36\2\u010f"+
		"\u0110\7.\2\2\u0110\u0135\3\2\2\2\u0111\u0112\7\66\2\2\u0112\u0113\7\21"+
		"\2\2\u0113\u0114\7=\2\2\u0114\u0115\7\31\2\2\u0115\u0116\5*\26\2\u0116"+
		"\u0117\7.\2\2\u0117\u0135\3\2\2\2\u0118\u0119\7\30\2\2\u0119\u011a\7\21"+
		"\2\2\u011a\u011b\7=\2\2\u011b\u011c\7\31\2\2\u011c\u011d\5,\27\2\u011d"+
		"\u011e\7.\2\2\u011e\u0135\3\2\2\2\u011f\u0120\7\4\2\2\u0120\u0121\7\21"+
		"\2\2\u0121\u0122\7=\2\2\u0122\u0123\7\31\2\2\u0123\u0124\5.\30\2\u0124"+
		"\u0125\7.\2\2\u0125\u0135\3\2\2\2\u0126\u0127\7%\2\2\u0127\u0128\7\21"+
		"\2\2\u0128\u0129\7=\2\2\u0129\u012a\7\31\2\2\u012a\u012b\5\60\31\2\u012b"+
		"\u012c\7.\2\2\u012c\u0135\3\2\2\2\u012d\u012e\7\26\2\2\u012e\u012f\7\21"+
		"\2\2\u012f\u0130\7=\2\2\u0130\u0131\7\31\2\2\u0131\u0132\5\62\32\2\u0132"+
		"\u0133\7.\2\2\u0133\u0135\3\2\2\2\u0134\u00c7\3\2\2\2\u0134\u00cf\3\2"+
		"\2\2\u0134\u00d7\3\2\2\2\u0134\u00e2\3\2\2\2\u0134\u00e8\3\2\2\2\u0134"+
		"\u00ee\3\2\2\2\u0134\u00f5\3\2\2\2\u0134\u00fc\3\2\2\2\u0134\u0103\3\2"+
		"\2\2\u0134\u010a\3\2\2\2\u0134\u0111\3\2\2\2\u0134\u0118\3\2\2\2\u0134"+
		"\u011f\3\2\2\2\u0134\u0126\3\2\2\2\u0134\u012d\3\2\2\2\u0135\r\3\2\2\2"+
		"\u0136\u013a\5\20\t\2\u0137\u0139\5\22\n\2\u0138\u0137\3\2\2\2\u0139\u013c"+
		"\3\2\2\2\u013a\u0138\3\2\2\2\u013a\u013b\3\2\2\2\u013b\u013e\3\2\2\2\u013c"+
		"\u013a\3\2\2\2\u013d\u013f\5\24\13\2\u013e\u013d\3\2\2\2\u013e\u013f\3"+
		"\2\2\2\u013f\17\3\2\2\2\u0140\u0141\7\"\2\2\u0141\u0142\7!\2\2\u0142\u0143"+
		"\5\30\r\2\u0143\u0144\7\24\2\2\u0144\u0148\7$\2\2\u0145\u0147\5\4\3\2"+
		"\u0146\u0145\3\2\2\2\u0147\u014a\3\2\2\2\u0148\u0146\3\2\2\2\u0148\u0149"+
		"\3\2\2\2\u0149\u014b\3\2\2\2\u014a\u0148\3\2\2\2\u014b\u014c\7\13\2\2"+
		"\u014c\21\3\2\2\2\u014d\u014e\7\'\2\2\u014e\u014f\5\20\t\2\u014f\23\3"+
		"\2\2\2\u0150\u0151\7\'\2\2\u0151\u0155\7$\2\2\u0152\u0154\5\4\3\2\u0153"+
		"\u0152\3\2\2\2\u0154\u0157\3\2\2\2\u0155\u0153\3\2\2\2\u0155\u0156\3\2"+
		"\2\2\u0156\u0158\3\2\2\2\u0157\u0155\3\2\2\2\u0158\u0159\7\13\2\2\u0159"+
		"\25\3\2\2\2\u015a\u015b\7\37\2\2\u015b\u015c\7!\2\2\u015c\u015d\5\30\r"+
		"\2\u015d\u015e\7\24\2\2\u015e\u0162\7$\2\2\u015f\u0161\5\4\3\2\u0160\u015f"+
		"\3\2\2\2\u0161\u0164\3\2\2\2\u0162\u0160\3\2\2\2\u0162\u0163\3\2\2\2\u0163"+
		"\u0165\3\2\2\2\u0164\u0162\3\2\2\2\u0165\u0166\7\13\2\2\u0166\u0191\3"+
		"\2\2\2\u0167\u0168\7\17\2\2\u0168\u016c\7$\2\2\u0169\u016b\5\4\3\2\u016a"+
		"\u0169\3\2\2\2\u016b\u016e\3\2\2\2\u016c\u016a\3\2\2\2\u016c\u016d\3\2"+
		"\2\2\u016d\u016f\3\2\2\2\u016e\u016c\3\2\2\2\u016f\u0170\7\13\2\2\u0170"+
		"\u0171\7\37\2\2\u0171\u0172\7!\2\2\u0172\u0173\5\30\r\2\u0173\u0174\7"+
		"-\2\2\u0174\u0191\3\2\2\2\u0175\u0176\7,\2\2\u0176\u017a\7!\2\2\u0177"+
		"\u0179\5\4\3\2\u0178\u0177\3\2\2\2\u0179\u017c\3\2\2\2\u017a\u0178\3\2"+
		"\2\2\u017a\u017b\3\2\2\2\u017b\u017d\3\2\2\2\u017c\u017a\3\2\2\2\u017d"+
		"\u017e\7.\2\2\u017e\u017f\5\30\r\2\u017f\u0183\7.\2\2\u0180\u0182\5\4"+
		"\3\2\u0181\u0180\3\2\2\2\u0182\u0185\3\2\2\2\u0183\u0181\3\2\2\2\u0183"+
		"\u0184\3\2\2\2\u0184\u0186\3\2\2\2\u0185\u0183\3\2\2\2\u0186\u0187\7\24"+
		"\2\2\u0187\u018b\7$\2\2\u0188\u018a\5\4\3\2\u0189\u0188\3\2\2\2\u018a"+
		"\u018d\3\2\2\2\u018b\u0189\3\2\2\2\u018b\u018c\3\2\2\2\u018c\u018e\3\2"+
		"\2\2\u018d\u018b\3\2\2\2\u018e\u018f\7\13\2\2\u018f\u0191\3\2\2\2\u0190"+
		"\u015a\3\2\2\2\u0190\u0167\3\2\2\2\u0190\u0175\3\2\2\2\u0191\27\3\2\2"+
		"\2\u0192\u0193\b\r\1\2\u0193\u0194\5\32\16\2\u0194\u0195\t\5\2\2\u0195"+
		"\u0196\5\32\16\2\u0196\u01c8\3\2\2\2\u0197\u0198\5$\23\2\u0198\u0199\t"+
		"\5\2\2\u0199\u019a\5$\23\2\u019a\u01c8\3\2\2\2\u019b\u019c\5&\24\2\u019c"+
		"\u019d\t\5\2\2\u019d\u019e\5&\24\2\u019e\u01c8\3\2\2\2\u019f\u01a0\7A"+
		"\2\2\u01a0\u01a1\t\5\2\2\u01a1\u01c8\7A\2\2\u01a2\u01a3\5*\26\2\u01a3"+
		"\u01a4\t\6\2\2\u01a4\u01a5\5*\26\2\u01a5\u01c8\3\2\2\2\u01a6\u01a7\5,"+
		"\27\2\u01a7\u01a8\t\6\2\2\u01a8\u01a9\5,\27\2\u01a9\u01c8\3\2\2\2\u01aa"+
		"\u01ab\5.\30\2\u01ab\u01ac\t\6\2\2\u01ac\u01ad\5.\30\2\u01ad\u01c8\3\2"+
		"\2\2\u01ae\u01af\5\60\31\2\u01af\u01b0\t\6\2\2\u01b0\u01b1\5\60\31\2\u01b1"+
		"\u01c8\3\2\2\2\u01b2\u01b3\5\62\32\2\u01b3\u01b4\t\6\2\2\u01b4\u01b5\5"+
		"\62\32\2\u01b5\u01c8\3\2\2\2\u01b6\u01b7\5\64\33\2\u01b7\u01b8\t\6\2\2"+
		"\u01b8\u01b9\5\64\33\2\u01b9\u01c8\3\2\2\2\u01ba\u01bb\5\66\34\2\u01bb"+
		"\u01bc\t\6\2\2\u01bc\u01bd\5\66\34\2\u01bd\u01c8\3\2\2\2\u01be\u01bf\5"+
		"8\35\2\u01bf\u01c0\t\6\2\2\u01c0\u01c1\58\35\2\u01c1\u01c8\3\2\2\2\u01c2"+
		"\u01c3\5:\36\2\u01c3\u01c4\t\6\2\2\u01c4\u01c5\5:\36\2\u01c5\u01c8\3\2"+
		"\2\2\u01c6\u01c8\7@\2\2\u01c7\u0192\3\2\2\2\u01c7\u0197\3\2\2\2\u01c7"+
		"\u019b\3\2\2\2\u01c7\u019f\3\2\2\2\u01c7\u01a2\3\2\2\2\u01c7\u01a6\3\2"+
		"\2\2\u01c7\u01aa\3\2\2\2\u01c7\u01ae\3\2\2\2\u01c7\u01b2\3\2\2\2\u01c7"+
		"\u01b6\3\2\2\2\u01c7\u01ba\3\2\2\2\u01c7\u01be\3\2\2\2\u01c7\u01c2\3\2"+
		"\2\2\u01c7\u01c6\3\2\2\2\u01c8\u01ce\3\2\2\2\u01c9\u01ca\f\4\2\2\u01ca"+
		"\u01cb\t\6\2\2\u01cb\u01cd\7@\2\2\u01cc\u01c9\3\2\2\2\u01cd\u01d0\3\2"+
		"\2\2\u01ce\u01cc\3\2\2\2\u01ce\u01cf\3\2\2\2\u01cf\31\3\2\2\2\u01d0\u01ce"+
		"\3\2\2\2\u01d1\u01d2\5\34\17\2\u01d2\33\3\2\2\2\u01d3\u01d4\b\17\1\2\u01d4"+
		"\u01d5\5\36\20\2\u01d5\u01db\3\2\2\2\u01d6\u01d7\f\4\2\2\u01d7\u01d8\t"+
		"\7\2\2\u01d8\u01da\5\36\20\2\u01d9\u01d6\3\2\2\2\u01da\u01dd\3\2\2\2\u01db"+
		"\u01d9\3\2\2\2\u01db\u01dc\3\2\2\2\u01dc\35\3\2\2\2\u01dd\u01db\3\2\2"+
		"\2\u01de\u01df\b\20\1\2\u01df\u01e0\5 \21\2\u01e0\u01e6\3\2\2\2\u01e1"+
		"\u01e2\f\4\2\2\u01e2\u01e3\t\b\2\2\u01e3\u01e5\5 \21\2\u01e4\u01e1\3\2"+
		"\2\2\u01e5\u01e8\3\2\2\2\u01e6\u01e4\3\2\2\2\u01e6\u01e7\3\2\2\2\u01e7"+
		"\37\3\2\2\2\u01e8\u01e6\3\2\2\2\u01e9\u01ea\7\36\2\2\u01ea\u01ed\5 \21"+
		"\2\u01eb\u01ed\5\"\22\2\u01ec\u01e9\3\2\2\2\u01ec\u01eb\3\2\2\2\u01ed"+
		"!\3\2\2\2\u01ee\u01f5\7=\2\2\u01ef\u01f5\t\t\2\2\u01f0\u01f1\7!\2\2\u01f1"+
		"\u01f2\5\32\16\2\u01f2\u01f3\7\24\2\2\u01f3\u01f5\3\2\2\2\u01f4\u01ee"+
		"\3\2\2\2\u01f4\u01ef\3\2\2\2\u01f4\u01f0\3\2\2\2\u01f5#\3\2\2\2\u01f6"+
		"\u01f7\b\23\1\2\u01f7\u01f8\5`\61\2\u01f8\u01fe\3\2\2\2\u01f9\u01fa\f"+
		"\4\2\2\u01fa\u01fb\t\7\2\2\u01fb\u01fd\5`\61\2\u01fc\u01f9\3\2\2\2\u01fd"+
		"\u0200\3\2\2\2\u01fe\u01fc\3\2\2\2\u01fe\u01ff\3\2\2\2\u01ff%\3\2\2\2"+
		"\u0200\u01fe\3\2\2\2\u0201\u0202\b\24\1\2\u0202\u0203\5b\62\2\u0203\u0209"+
		"\3\2\2\2\u0204\u0205\f\4\2\2\u0205\u0206\t\7\2\2\u0206\u0208\5b\62\2\u0207"+
		"\u0204\3\2\2\2\u0208\u020b\3\2\2\2\u0209\u0207\3\2\2\2\u0209\u020a\3\2"+
		"\2\2\u020a\'\3\2\2\2\u020b\u0209\3\2\2\2\u020c\u020f\7=\2\2\u020d\u020f"+
		"\7A\2\2\u020e\u020c\3\2\2\2\u020e\u020d\3\2\2\2\u020f)\3\2\2\2\u0210\u0211"+
		"\b\26\1\2\u0211\u0212\5P)\2\u0212\u0218\3\2\2\2\u0213\u0214\f\4\2\2\u0214"+
		"\u0215\t\7\2\2\u0215\u0217\5P)\2\u0216\u0213\3\2\2\2\u0217\u021a\3\2\2"+
		"\2\u0218\u0216\3\2\2\2\u0218\u0219\3\2\2\2\u0219+\3\2\2\2\u021a\u0218"+
		"\3\2\2\2\u021b\u021c\b\27\1\2\u021c\u021d\5R*\2\u021d\u0223\3\2\2\2\u021e"+
		"\u021f\f\4\2\2\u021f\u0220\t\7\2\2\u0220\u0222\5R*\2\u0221\u021e\3\2\2"+
		"\2\u0222\u0225\3\2\2\2\u0223\u0221\3\2\2\2\u0223\u0224\3\2\2\2\u0224-"+
		"\3\2\2\2\u0225\u0223\3\2\2\2\u0226\u0227\b\30\1\2\u0227\u0228\5T+\2\u0228"+
		"\u022e\3\2\2\2\u0229\u022a\f\4\2\2\u022a\u022b\t\7\2\2\u022b\u022d\5T"+
		"+\2\u022c\u0229\3\2\2\2\u022d\u0230\3\2\2\2\u022e\u022c\3\2\2\2\u022e"+
		"\u022f\3\2\2\2\u022f/\3\2\2\2\u0230\u022e\3\2\2\2\u0231\u0232\b\31\1\2"+
		"\u0232\u0233\5V,\2\u0233\u0239\3\2\2\2\u0234\u0235\f\4\2\2\u0235\u0236"+
		"\t\7\2\2\u0236\u0238\5V,\2\u0237\u0234\3\2\2\2\u0238\u023b\3\2\2\2\u0239"+
		"\u0237\3\2\2\2\u0239\u023a\3\2\2\2\u023a\61\3\2\2\2\u023b\u0239\3\2\2"+
		"\2\u023c\u023d\b\32\1\2\u023d\u023e\5X-\2\u023e\u0244\3\2\2\2\u023f\u0240"+
		"\f\4\2\2\u0240\u0241\t\7\2\2\u0241\u0243\5X-\2\u0242\u023f\3\2\2\2\u0243"+
		"\u0246\3\2\2\2\u0244\u0242\3\2\2\2\u0244\u0245\3\2\2\2\u0245\63\3\2\2"+
		"\2\u0246\u0244\3\2\2\2\u0247\u0248\b\33\1\2\u0248\u0249\5F$\2\u0249\u024f"+
		"\3\2\2\2\u024a\u024b\f\4\2\2\u024b\u024c\t\7\2\2\u024c\u024e\5F$\2\u024d"+
		"\u024a\3\2\2\2\u024e\u0251\3\2\2\2\u024f\u024d\3\2\2\2\u024f\u0250\3\2"+
		"\2\2\u0250\65\3\2\2\2\u0251\u024f\3\2\2\2\u0252\u0253\b\34\1\2\u0253\u0254"+
		"\5H%\2\u0254\u025a\3\2\2\2\u0255\u0256\f\4\2\2\u0256\u0257\t\7\2\2\u0257"+
		"\u0259\5H%\2\u0258\u0255\3\2\2\2\u0259\u025c\3\2\2\2\u025a\u0258\3\2\2"+
		"\2\u025a\u025b\3\2\2\2\u025b\67\3\2\2\2\u025c\u025a\3\2\2\2\u025d\u025e"+
		"\b\35\1\2\u025e\u025f\5J&\2\u025f\u0265\3\2\2\2\u0260\u0261\f\4\2\2\u0261"+
		"\u0262\t\7\2\2\u0262\u0264\5J&\2\u0263\u0260\3\2\2\2\u0264\u0267\3\2\2"+
		"\2\u0265\u0263\3\2\2\2\u0265\u0266\3\2\2\2\u02669\3\2\2\2\u0267\u0265"+
		"\3\2\2\2\u0268\u0269\b\36\1\2\u0269\u026a\5L\'\2\u026a\u0270\3\2\2\2\u026b"+
		"\u026c\f\4\2\2\u026c\u026d\t\7\2\2\u026d\u026f\5L\'\2\u026e\u026b\3\2"+
		"\2\2\u026f\u0272\3\2\2\2\u0270\u026e\3\2\2\2\u0270\u0271\3\2\2\2\u0271"+
		";\3\2\2\2\u0272\u0270\3\2\2\2\u0273\u0274\t\n\2\2\u0274\u0275\7 \2\2\u0275"+
		"\u0276\5^\60\2\u0276\u0277\7.\2\2\u0277\u0280\3\2\2\2\u0278\u0279\7\25"+
		"\2\2\u0279\u027a\5\\/\2\u027a\u027b\7.\2\2\u027b\u0280\3\2\2\2\u027c\u027d"+
		"\7\65\2\2\u027d\u027e\7F\2\2\u027e\u0280\7.\2\2\u027f\u0273\3\2\2\2\u027f"+
		"\u0278\3\2\2\2\u027f\u027c\3\2\2\2\u0280=\3\2\2\2\u0281\u0282\t\n\2\2"+
		"\u0282\u0283\7 \2\2\u0283\u0284\5^\60\2\u0284\u0285\7.\2\2\u0285\u0296"+
		"\3\2\2\2\u0286\u0287\t\13\2\2\u0287\u0288\7 \2\2\u0288\u0289\5F$\2\u0289"+
		"\u028a\7.\2\2\u028a\u0296\3\2\2\2\u028b\u028c\7\25\2\2\u028c\u028d\5\\"+
		"/\2\u028d\u028e\7.\2\2\u028e\u0296\3\2\2\2\u028f\u0290\7\63\2\2\u0290"+
		"\u0291\7B\2\2\u0291\u0296\7.\2\2\u0292\u0293\7\65\2\2\u0293\u0294\7F\2"+
		"\2\u0294\u0296\7.\2\2\u0295\u0281\3\2\2\2\u0295\u0286\3\2\2\2\u0295\u028b"+
		"\3\2\2\2\u0295\u028f\3\2\2\2\u0295\u0292\3\2\2\2\u0296?\3\2\2\2\u0297"+
		"\u0298\t\n\2\2\u0298\u0299\7 \2\2\u0299\u029a\5P)\2\u029a\u029b\7.\2\2"+
		"\u029b\u02a7\3\2\2\2\u029c\u029d\7\25\2\2\u029d\u029e\5\\/\2\u029e\u029f"+
		"\7.\2\2\u029f\u02a7\3\2\2\2\u02a0\u02a1\7\63\2\2\u02a1\u02a2\7C\2\2\u02a2"+
		"\u02a7\7.\2\2\u02a3\u02a4\7\65\2\2\u02a4\u02a5\7F\2\2\u02a5\u02a7\7.\2"+
		"\2\u02a6\u0297\3\2\2\2\u02a6\u029c\3\2\2\2\u02a6\u02a0\3\2\2\2\u02a6\u02a3"+
		"\3\2\2\2\u02a7A\3\2\2\2\u02a8\u02a9\t\n\2\2\u02a9\u02aa\7 \2\2\u02aa\u02ab"+
		"\5P)\2\u02ab\u02ac\7.\2\2\u02ac\u02b8\3\2\2\2\u02ad\u02ae\7\25\2\2\u02ae"+
		"\u02af\5\\/\2\u02af\u02b0\7.\2\2\u02b0\u02b8\3\2\2\2\u02b1\u02b2\7\63"+
		"\2\2\u02b2\u02b3\7D\2\2\u02b3\u02b8\7.\2\2\u02b4\u02b5\7\65\2\2\u02b5"+
		"\u02b6\7F\2\2\u02b6\u02b8\7.\2\2\u02b7\u02a8\3\2\2\2\u02b7\u02ad\3\2\2"+
		"\2\u02b7\u02b1\3\2\2\2\u02b7\u02b4\3\2\2\2\u02b8C\3\2\2\2\u02b9\u02ba"+
		"\7\25\2\2\u02ba\u02bb\5\\/\2\u02bb\u02bc\7.\2\2\u02bc\u02d5\3\2\2\2\u02bd"+
		"\u02be\7\63\2\2\u02be\u02bf\7E\2\2\u02bf\u02d5\7.\2\2\u02c0\u02c1\7)\2"+
		"\2\u02c1\u02c2\5*\26\2\u02c2\u02c3\7.\2\2\u02c3\u02d5\3\2\2\2\u02c4\u02c5"+
		"\7\27\2\2\u02c5\u02c6\5,\27\2\u02c6\u02c7\7.\2\2\u02c7\u02d5\3\2\2\2\u02c8"+
		"\u02c9\7\22\2\2\u02c9\u02ca\5.\30\2\u02ca\u02cb\7.\2\2\u02cb\u02d5\3\2"+
		"\2\2\u02cc\u02cd\7\16\2\2\u02cd\u02ce\5\60\31\2\u02ce\u02cf\7.\2\2\u02cf"+
		"\u02d5\3\2\2\2\u02d0\u02d1\7\32\2\2\u02d1\u02d2\5\62\32\2\u02d2\u02d3"+
		"\7.\2\2\u02d3\u02d5\3\2\2\2\u02d4\u02b9\3\2\2\2\u02d4\u02bd\3\2\2\2\u02d4"+
		"\u02c0\3\2\2\2\u02d4\u02c4\3\2\2\2\u02d4\u02c8\3\2\2\2\u02d4\u02cc\3\2"+
		"\2\2\u02d4\u02d0\3\2\2\2\u02d5E\3\2\2\2\u02d6\u02d7\7$\2\2\u02d7\u02dc"+
		"\5\32\16\2\u02d8\u02d9\7\35\2\2\u02d9\u02db\5\32\16\2\u02da\u02d8\3\2"+
		"\2\2\u02db\u02de\3\2\2\2\u02dc\u02da\3\2\2\2\u02dc\u02dd\3\2\2\2\u02dd"+
		"\u02df\3\2\2\2\u02de\u02dc\3\2\2\2\u02df\u02e0\7\13\2\2\u02e0\u02e3\3"+
		"\2\2\2\u02e1\u02e3\7=\2\2\u02e2\u02d6\3\2\2\2\u02e2\u02e1\3\2\2\2\u02e3"+
		"G\3\2\2\2\u02e4\u02e5\7$\2\2\u02e5\u02ea\5$\23\2\u02e6\u02e7\7\35\2\2"+
		"\u02e7\u02e9\5$\23\2\u02e8\u02e6\3\2\2\2\u02e9\u02ec\3\2\2\2\u02ea\u02e8"+
		"\3\2\2\2\u02ea\u02eb\3\2\2\2\u02eb\u02ed\3\2\2\2\u02ec\u02ea\3\2\2\2\u02ed"+
		"\u02ee\7\13\2\2\u02ee\u02fc\3\2\2\2\u02ef\u02f0\7$\2\2\u02f0\u02f5\5&"+
		"\24\2\u02f1\u02f2\7\35\2\2\u02f2\u02f4\5&\24\2\u02f3\u02f1\3\2\2\2\u02f4"+
		"\u02f7\3\2\2\2\u02f5\u02f3\3\2\2\2\u02f5\u02f6\3\2\2\2\u02f6\u02f8\3\2"+
		"\2\2\u02f7\u02f5\3\2\2\2\u02f8\u02f9\7\13\2\2\u02f9\u02fc\3\2\2\2\u02fa"+
		"\u02fc\7=\2\2\u02fb\u02e4\3\2\2\2\u02fb\u02ef\3\2\2\2\u02fb\u02fa\3\2"+
		"\2\2\u02fcI\3\2\2\2\u02fd\u02fe\7$\2\2\u02fe\u0303\5\30\r\2\u02ff\u0300"+
		"\7\35\2\2\u0300\u0302\5\30\r\2\u0301\u02ff\3\2\2\2\u0302\u0305\3\2\2\2"+
		"\u0303\u0301\3\2\2\2\u0303\u0304\3\2\2\2\u0304\u0306\3\2\2\2\u0305\u0303"+
		"\3\2\2\2\u0306\u0307\7\13\2\2\u0307\u030a\3\2\2\2\u0308\u030a\7=\2\2\u0309"+
		"\u02fd\3\2\2\2\u0309\u0308\3\2\2\2\u030aK\3\2\2\2\u030b\u030c\7$\2\2\u030c"+
		"\u0311\5(\25\2\u030d\u030e\7\35\2\2\u030e\u0310\5(\25\2\u030f\u030d\3"+
		"\2\2\2\u0310\u0313\3\2\2\2\u0311\u030f\3\2\2\2\u0311\u0312\3\2\2\2\u0312"+
		"\u0314\3\2\2\2\u0313\u0311\3\2\2\2\u0314\u0315\7\13\2\2\u0315\u0318\3"+
		"\2\2\2\u0316\u0318\7=\2\2\u0317\u030b\3\2\2\2\u0317\u0316\3\2\2\2\u0318"+
		"M\3\2\2\2\u0319\u031a\7$\2\2\u031a\u031f\7=\2\2\u031b\u031c\7\35\2\2\u031c"+
		"\u031e\7=\2\2\u031d\u031b\3\2\2\2\u031e\u0321\3\2\2\2\u031f\u031d\3\2"+
		"\2\2\u031f\u0320\3\2\2\2\u0320\u0322\3\2\2\2\u0321\u031f\3\2\2\2\u0322"+
		"\u0325\7\13\2\2\u0323\u0325\7=\2\2\u0324\u0319\3\2\2\2\u0324\u0323\3\2"+
		"\2\2\u0325O\3\2\2\2\u0326\u0327\7$\2\2\u0327\u032c\5^\60\2\u0328\u0329"+
		"\7\35\2\2\u0329\u032b\5^\60\2\u032a\u0328\3\2\2\2\u032b\u032e\3\2\2\2"+
		"\u032c\u032a\3\2\2\2\u032c\u032d\3\2\2\2\u032d\u032f\3\2\2\2\u032e\u032c"+
		"\3\2\2\2\u032f\u0330\7\13\2\2\u0330\u0333\3\2\2\2\u0331\u0333\7=\2\2\u0332"+
		"\u0326\3\2\2\2\u0332\u0331\3\2\2\2\u0333Q\3\2\2\2\u0334\u0335\7$\2\2\u0335"+
		"\u033a\7=\2\2\u0336\u0337\7\35\2\2\u0337\u0339\7=\2\2\u0338\u0336\3\2"+
		"\2\2\u0339\u033c\3\2\2\2\u033a\u0338\3\2\2\2\u033a\u033b\3\2\2\2\u033b"+
		"\u033d\3\2\2\2\u033c\u033a\3\2\2\2\u033d\u0340\7\13\2\2\u033e\u0340\7"+
		"=\2\2\u033f\u0334\3\2\2\2\u033f\u033e\3\2\2\2\u0340S\3\2\2\2\u0341\u0342"+
		"\7$\2\2\u0342\u0347\7=\2\2\u0343\u0344\7\35\2\2\u0344\u0346\7=\2\2\u0345"+
		"\u0343\3\2\2\2\u0346\u0349\3\2\2\2\u0347\u0345\3\2\2\2\u0347\u0348\3\2"+
		"\2\2\u0348\u034a\3\2\2\2\u0349\u0347\3\2\2\2\u034a\u034d\7\13\2\2\u034b"+
		"\u034d\7=\2\2\u034c\u0341\3\2\2\2\u034c\u034b\3\2\2\2\u034dU\3\2\2\2\u034e"+
		"\u034f\7$\2\2\u034f\u0354\7=\2\2\u0350\u0351\7\35\2\2\u0351\u0353\7=\2"+
		"\2\u0352\u0350\3\2\2\2\u0353\u0356\3\2\2\2\u0354\u0352\3\2\2\2\u0354\u0355"+
		"\3\2\2\2\u0355\u0357\3\2\2\2\u0356\u0354\3\2\2\2\u0357\u035a\7\13\2\2"+
		"\u0358\u035a\7=\2\2\u0359\u034e\3\2\2\2\u0359\u0358\3\2\2\2\u035aW\3\2"+
		"\2\2\u035b\u035c\7$\2\2\u035c\u0361\7=\2\2\u035d\u035e\7\35\2\2\u035e"+
		"\u0360\7=\2\2\u035f\u035d\3\2\2\2\u0360\u0363\3\2\2\2\u0361\u035f\3\2"+
		"\2\2\u0361\u0362\3\2\2\2\u0362\u0364\3\2\2\2\u0363\u0361\3\2\2\2\u0364"+
		"\u0367\7\13\2\2\u0365\u0367\7=\2\2\u0366\u035b\3\2\2\2\u0366\u0365\3\2"+
		"\2\2\u0367Y\3\2\2\2\u0368\u036f\5^\60\2\u0369\u036c\7=\2\2\u036a\u036b"+
		"\7*\2\2\u036b\u036d\t\n\2\2\u036c\u036a\3\2\2\2\u036c\u036d\3\2\2\2\u036d"+
		"\u036f\3\2\2\2\u036e\u0368\3\2\2\2\u036e\u0369\3\2\2\2\u036f[\3\2\2\2"+
		"\u0370\u0373\7=\2\2\u0371\u0373\7A\2\2\u0372\u0370\3\2\2\2\u0372\u0371"+
		"\3\2\2\2\u0373]\3\2\2\2\u0374\u0375\7$\2\2\u0375\u0376\5`\61\2\u0376\u0377"+
		"\7\35\2\2\u0377\u0378\5b\62\2\u0378\u0379\7\13\2\2\u0379\u037c\3\2\2\2"+
		"\u037a\u037c\7=\2\2\u037b\u0374\3\2\2\2\u037b\u037a\3\2\2\2\u037c_\3\2"+
		"\2\2\u037d\u0384\5d\63\2\u037e\u0381\7=\2\2\u037f\u0380\7*\2\2\u0380\u0382"+
		"\7\33\2\2\u0381\u037f\3\2\2\2\u0381\u0382\3\2\2\2\u0382\u0384\3\2\2\2"+
		"\u0383\u037d\3\2\2\2\u0383\u037e\3\2\2\2\u0384a\3\2\2\2\u0385\u038c\5"+
		"d\63\2\u0386\u0389\7=\2\2\u0387\u0388\7*\2\2\u0388\u038a\7\33\2\2\u0389"+
		"\u0387\3\2\2\2\u0389\u038a\3\2\2\2\u038a\u038c\3\2\2\2\u038b\u0385\3\2"+
		"\2\2\u038b\u0386\3\2\2\2\u038cc\3\2\2\2\u038d\u038e\5h\65\2\u038e\u038f"+
		"\7?\2\2\u038f\u0398\3\2\2\2\u0390\u0391\5h\65\2\u0391\u0392\7>\2\2\u0392"+
		"\u0398\3\2\2\2\u0393\u0394\7?\2\2\u0394\u0398\5h\65\2\u0395\u0396\7>\2"+
		"\2\u0396\u0398\5h\65\2\u0397\u038d\3\2\2\2\u0397\u0390\3\2\2\2\u0397\u0393"+
		"\3\2\2\2\u0397\u0395\3\2\2\2\u0398e\3\2\2\2\u0399\u039a\5j\66\2\u039a"+
		"\u039b\7?\2\2\u039b\u03a4\3\2\2\2\u039c\u039d\5j\66\2\u039d\u039e\7>\2"+
		"\2\u039e\u03a4\3\2\2\2\u039f\u03a0\7?\2\2\u03a0\u03a4\5j\66\2\u03a1\u03a2"+
		"\7>\2\2\u03a2\u03a4\5j\66\2\u03a3\u0399\3\2\2\2\u03a3\u039c\3\2\2\2\u03a3"+
		"\u039f\3\2\2\2\u03a3\u03a1\3\2\2\2\u03a4g\3\2\2\2\u03a5\u03a8\79\2\2\u03a6"+
		"\u03a8\7:\2\2\u03a7\u03a5\3\2\2\2\u03a7\u03a6\3\2\2\2\u03a8i\3\2\2\2\u03a9"+
		"\u03ac\7;\2\2\u03aa\u03ac\7<\2\2\u03ab\u03a9\3\2\2\2\u03ab\u03aa\3\2\2"+
		"\2\u03ack\3\2\2\2Tow\u0086\u008e\u00a2\u00ac\u00b6\u00c0\u00c4\u00c7\u00cf"+
		"\u00d7\u00dd\u00e2\u00e8\u0134\u013a\u013e\u0148\u0155\u0162\u016c\u017a"+
		"\u0183\u018b\u0190\u01c7\u01ce\u01db\u01e6\u01ec\u01f4\u01fe\u0209\u020e"+
		"\u0218\u0223\u022e\u0239\u0244\u024f\u025a\u0265\u0270\u027f\u0295\u02a6"+
		"\u02b7\u02d4\u02dc\u02e2\u02ea\u02f5\u02fb\u0303\u0309\u0311\u0317\u031f"+
		"\u0324\u032c\u0332\u033a\u033f\u0347\u034c\u0354\u0359\u0361\u0366\u036c"+
		"\u036e\u0372\u037b\u0381\u0383\u0389\u038b\u0397\u03a3\u03a7\u03ab";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}