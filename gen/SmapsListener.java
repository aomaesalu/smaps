// Generated from C:/Users/Ants-Oskar/Documents/Bitbucket/smaps/src\Smaps.g4 by ANTLR 4.x
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SmapsParser}.
 */
public interface SmapsListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SmapsParser#AladeJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterAladeJarjendMuutujaNimi(@NotNull SmapsParser.AladeJarjendMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#AladeJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitAladeJarjendMuutujaNimi(@NotNull SmapsParser.AladeJarjendMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisToevaartusteJarjend}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisToevaartusteJarjend(@NotNull SmapsParser.VordlusAvaldisToevaartusteJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisToevaartusteJarjend}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisToevaartusteJarjend(@NotNull SmapsParser.VordlusAvaldisToevaartusteJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ObjektideJarjendiAvaldisObjektideJarjend}.
	 * @param ctx the parse tree
	 */
	void enterObjektideJarjendiAvaldisObjektideJarjend(@NotNull SmapsParser.ObjektideJarjendiAvaldisObjektideJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ObjektideJarjendiAvaldisObjektideJarjend}.
	 * @param ctx the parse tree
	 */
	void exitObjektideJarjendiAvaldisObjektideJarjend(@NotNull SmapsParser.ObjektideJarjendiAvaldisObjektideJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ObjektiParameeterTuup}.
	 * @param ctx the parse tree
	 */
	void enterObjektiParameeterTuup(@NotNull SmapsParser.ObjektiParameeterTuupContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ObjektiParameeterTuup}.
	 * @param ctx the parse tree
	 */
	void exitObjektiParameeterTuup(@NotNull SmapsParser.ObjektiParameeterTuupContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterArvudeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ArvudeJarjendiAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitArvudeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ArvudeJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#LaiuskoordinaatideAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterLaiuskoordinaatideAvaldisLiitmineLahutamine(@NotNull SmapsParser.LaiuskoordinaatideAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#LaiuskoordinaatideAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitLaiuskoordinaatideAvaldisLiitmineLahutamine(@NotNull SmapsParser.LaiuskoordinaatideAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#JoonteJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterJoonteJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.JoonteJarjendiAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#JoonteJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitJoonteJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.JoonteJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#AlaParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void enterAlaParameeterKoordinaadid(@NotNull SmapsParser.AlaParameeterKoordinaadidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#AlaParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void exitAlaParameeterKoordinaadid(@NotNull SmapsParser.AlaParameeterKoordinaadidContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisJoonteJarjend}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisJoonteJarjend(@NotNull SmapsParser.VordlusAvaldisJoonteJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisJoonteJarjend}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisJoonteJarjend(@NotNull SmapsParser.VordlusAvaldisJoonteJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktiKoordinaadidPunkt}.
	 * @param ctx the parse tree
	 */
	void enterPunktiKoordinaadidPunkt(@NotNull SmapsParser.PunktiKoordinaadidPunktContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktiKoordinaadidPunkt}.
	 * @param ctx the parse tree
	 */
	void exitPunktiKoordinaadidPunkt(@NotNull SmapsParser.PunktiKoordinaadidPunktContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#valikulause}.
	 * @param ctx the parse tree
	 */
	void enterValikulause(@NotNull SmapsParser.ValikulauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#valikulause}.
	 * @param ctx the parse tree
	 */
	void exitValikulause(@NotNull SmapsParser.ValikulauseContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#sonedeJarjend}.
	 * @param ctx the parse tree
	 */
	void enterSonedeJarjend(@NotNull SmapsParser.SonedeJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#sonedeJarjend}.
	 * @param ctx the parse tree
	 */
	void exitSonedeJarjend(@NotNull SmapsParser.SonedeJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KorduslauseWhile}.
	 * @param ctx the parse tree
	 */
	void enterKorduslauseWhile(@NotNull SmapsParser.KorduslauseWhileContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KorduslauseWhile}.
	 * @param ctx the parse tree
	 */
	void exitKorduslauseWhile(@NotNull SmapsParser.KorduslauseWhileContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#omistamine}.
	 * @param ctx the parse tree
	 */
	void enterOmistamine(@NotNull SmapsParser.OmistamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#omistamine}.
	 * @param ctx the parse tree
	 */
	void exitOmistamine(@NotNull SmapsParser.OmistamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PikkuskoordinaatideAvaldisPikkuskoordinaat}.
	 * @param ctx the parse tree
	 */
	void enterPikkuskoordinaatideAvaldisPikkuskoordinaat(@NotNull SmapsParser.PikkuskoordinaatideAvaldisPikkuskoordinaatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PikkuskoordinaatideAvaldisPikkuskoordinaat}.
	 * @param ctx the parse tree
	 */
	void exitPikkuskoordinaatideAvaldisPikkuskoordinaat(@NotNull SmapsParser.PikkuskoordinaatideAvaldisPikkuskoordinaatContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#programm}.
	 * @param ctx the parse tree
	 */
	void enterProgramm(@NotNull SmapsParser.ProgrammContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#programm}.
	 * @param ctx the parse tree
	 */
	void exitProgramm(@NotNull SmapsParser.ProgrammContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisSonedeJarjend}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisSonedeJarjend(@NotNull SmapsParser.VordlusAvaldisSonedeJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisSonedeJarjend}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisSonedeJarjend(@NotNull SmapsParser.VordlusAvaldisSonedeJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KoordinaatideJarjendiAvaldisKoordinaatideJarjend}.
	 * @param ctx the parse tree
	 */
	void enterKoordinaatideJarjendiAvaldisKoordinaatideJarjend(@NotNull SmapsParser.KoordinaatideJarjendiAvaldisKoordinaatideJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KoordinaatideJarjendiAvaldisKoordinaatideJarjend}.
	 * @param ctx the parse tree
	 */
	void exitKoordinaatideJarjendiAvaldisKoordinaatideJarjend(@NotNull SmapsParser.KoordinaatideJarjendiAvaldisKoordinaatideJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaardiParameeterNimi}.
	 * @param ctx the parse tree
	 */
	void enterKaardiParameeterNimi(@NotNull SmapsParser.KaardiParameeterNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaardiParameeterNimi}.
	 * @param ctx the parse tree
	 */
	void exitKaardiParameeterNimi(@NotNull SmapsParser.KaardiParameeterNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterPunktMuutujaNimi(@NotNull SmapsParser.PunktMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitPunktMuutujaNimi(@NotNull SmapsParser.PunktMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#AlaParameeterKuva}.
	 * @param ctx the parse tree
	 */
	void enterAlaParameeterKuva(@NotNull SmapsParser.AlaParameeterKuvaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#AlaParameeterKuva}.
	 * @param ctx the parse tree
	 */
	void exitAlaParameeterKuva(@NotNull SmapsParser.AlaParameeterKuvaContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#kommentaar}.
	 * @param ctx the parse tree
	 */
	void enterKommentaar(@NotNull SmapsParser.KommentaarContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#kommentaar}.
	 * @param ctx the parse tree
	 */
	void exitKommentaar(@NotNull SmapsParser.KommentaarContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#AlaParameeterTuup}.
	 * @param ctx the parse tree
	 */
	void enterAlaParameeterTuup(@NotNull SmapsParser.AlaParameeterTuupContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#AlaParameeterTuup}.
	 * @param ctx the parse tree
	 */
	void exitAlaParameeterTuup(@NotNull SmapsParser.AlaParameeterTuupContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#arvudeJarjend}.
	 * @param ctx the parse tree
	 */
	void enterArvudeJarjend(@NotNull SmapsParser.ArvudeJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#arvudeJarjend}.
	 * @param ctx the parse tree
	 */
	void exitArvudeJarjend(@NotNull SmapsParser.ArvudeJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterArvudeAvaldisLiitmineLahutamine(@NotNull SmapsParser.ArvudeAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitArvudeAvaldisLiitmineLahutamine(@NotNull SmapsParser.ArvudeAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaartideJarjendiAvaldisKaartideJarjend}.
	 * @param ctx the parse tree
	 */
	void enterKaartideJarjendiAvaldisKaartideJarjend(@NotNull SmapsParser.KaartideJarjendiAvaldisKaartideJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaartideJarjendiAvaldisKaartideJarjend}.
	 * @param ctx the parse tree
	 */
	void exitKaartideJarjendiAvaldisKaartideJarjend(@NotNull SmapsParser.KaartideJarjendiAvaldisKaartideJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#SoneMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterSoneMuutujaNimi(@NotNull SmapsParser.SoneMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#SoneMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitSoneMuutujaNimi(@NotNull SmapsParser.SoneMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisSone}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisSone(@NotNull SmapsParser.VordlusAvaldisSoneContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisSone}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisSone(@NotNull SmapsParser.VordlusAvaldisSoneContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PikkuskoordinaatideAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterPikkuskoordinaatideAvaldisLiitmineLahutamine(@NotNull SmapsParser.PikkuskoordinaatideAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PikkuskoordinaatideAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitPikkuskoordinaatideAvaldisLiitmineLahutamine(@NotNull SmapsParser.PikkuskoordinaatideAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void enterPunktKoordinaadid(@NotNull SmapsParser.PunktKoordinaadidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void exitPunktKoordinaadid(@NotNull SmapsParser.PunktKoordinaadidContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaartideJarjendJarjend}.
	 * @param ctx the parse tree
	 */
	void enterKaartideJarjendJarjend(@NotNull SmapsParser.KaartideJarjendJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaartideJarjendJarjend}.
	 * @param ctx the parse tree
	 */
	void exitKaartideJarjendJarjend(@NotNull SmapsParser.KaartideJarjendJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaardiParameeterObjektid}.
	 * @param ctx the parse tree
	 */
	void enterKaardiParameeterObjektid(@NotNull SmapsParser.KaardiParameeterObjektidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaardiParameeterObjektid}.
	 * @param ctx the parse tree
	 */
	void exitKaardiParameeterObjektid(@NotNull SmapsParser.KaardiParameeterObjektidContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#LaiuskoordinaatMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterLaiuskoordinaatMuutujaNimi(@NotNull SmapsParser.LaiuskoordinaatMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#LaiuskoordinaatMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitLaiuskoordinaatMuutujaNimi(@NotNull SmapsParser.LaiuskoordinaatMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisObjektideJarjend}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisObjektideJarjend(@NotNull SmapsParser.VordlusAvaldisObjektideJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisObjektideJarjend}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisObjektideJarjend(@NotNull SmapsParser.VordlusAvaldisObjektideJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisToevaartus}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisToevaartus(@NotNull SmapsParser.VordlusAvaldisToevaartusContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisToevaartus}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisToevaartus(@NotNull SmapsParser.VordlusAvaldisToevaartusContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#lause}.
	 * @param ctx the parse tree
	 */
	void enterLause(@NotNull SmapsParser.LauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#lause}.
	 * @param ctx the parse tree
	 */
	void exitLause(@NotNull SmapsParser.LauseContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisKoordinaatideJarjend}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisKoordinaatideJarjend(@NotNull SmapsParser.VordlusAvaldisKoordinaatideJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisKoordinaatideJarjend}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisKoordinaatideJarjend(@NotNull SmapsParser.VordlusAvaldisKoordinaatideJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisArvud}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisArvud(@NotNull SmapsParser.VordlusAvaldisArvudContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisArvud}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisArvud(@NotNull SmapsParser.VordlusAvaldisArvudContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisArvudeJarjend}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisArvudeJarjend(@NotNull SmapsParser.VordlusAvaldisArvudeJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisArvudeJarjend}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisArvudeJarjend(@NotNull SmapsParser.VordlusAvaldisArvudeJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#SonedeJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterSonedeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.SonedeJarjendiAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#SonedeJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitSonedeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.SonedeJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktideJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterPunktideJarjendMuutujaNimi(@NotNull SmapsParser.PunktideJarjendMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktideJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitPunktideJarjendMuutujaNimi(@NotNull SmapsParser.PunktideJarjendMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#koordinaatideJarjend}.
	 * @param ctx the parse tree
	 */
	void enterKoordinaatideJarjend(@NotNull SmapsParser.KoordinaatideJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#koordinaatideJarjend}.
	 * @param ctx the parse tree
	 */
	void exitKoordinaatideJarjend(@NotNull SmapsParser.KoordinaatideJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PikkuskraadIda}.
	 * @param ctx the parse tree
	 */
	void enterPikkuskraadIda(@NotNull SmapsParser.PikkuskraadIdaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PikkuskraadIda}.
	 * @param ctx the parse tree
	 */
	void exitPikkuskraadIda(@NotNull SmapsParser.PikkuskraadIdaContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#AladeJarjendiAvaldisAladeJarjend}.
	 * @param ctx the parse tree
	 */
	void enterAladeJarjendiAvaldisAladeJarjend(@NotNull SmapsParser.AladeJarjendiAvaldisAladeJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#AladeJarjendiAvaldisAladeJarjend}.
	 * @param ctx the parse tree
	 */
	void exitAladeJarjendiAvaldisAladeJarjend(@NotNull SmapsParser.AladeJarjendiAvaldisAladeJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#muutujaDeklaratsioon}.
	 * @param ctx the parse tree
	 */
	void enterMuutujaDeklaratsioon(@NotNull SmapsParser.MuutujaDeklaratsioonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#muutujaDeklaratsioon}.
	 * @param ctx the parse tree
	 */
	void exitMuutujaDeklaratsioon(@NotNull SmapsParser.MuutujaDeklaratsioonContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisKoordinaadid(@NotNull SmapsParser.VordlusAvaldisKoordinaadidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisKoordinaadid(@NotNull SmapsParser.VordlusAvaldisKoordinaadidContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#JoonteJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterJoonteJarjendMuutujaNimi(@NotNull SmapsParser.JoonteJarjendMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#JoonteJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitJoonteJarjendMuutujaNimi(@NotNull SmapsParser.JoonteJarjendMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#AladeJarjendJarjend}.
	 * @param ctx the parse tree
	 */
	void enterAladeJarjendJarjend(@NotNull SmapsParser.AladeJarjendJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#AladeJarjendJarjend}.
	 * @param ctx the parse tree
	 */
	void exitAladeJarjendJarjend(@NotNull SmapsParser.AladeJarjendJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PikkuskoordinaatMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterPikkuskoordinaatMuutujaNimi(@NotNull SmapsParser.PikkuskoordinaatMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PikkuskoordinaatMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitPikkuskoordinaatMuutujaNimi(@NotNull SmapsParser.PikkuskoordinaatMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#LaiuskoordinaatideAvaldisLaiuskoordinaat}.
	 * @param ctx the parse tree
	 */
	void enterLaiuskoordinaatideAvaldisLaiuskoordinaat(@NotNull SmapsParser.LaiuskoordinaatideAvaldisLaiuskoordinaatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#LaiuskoordinaatideAvaldisLaiuskoordinaat}.
	 * @param ctx the parse tree
	 */
	void exitLaiuskoordinaatideAvaldisLaiuskoordinaat(@NotNull SmapsParser.LaiuskoordinaatideAvaldisLaiuskoordinaatContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaartideJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterKaartideJarjendMuutujaNimi(@NotNull SmapsParser.KaartideJarjendMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaartideJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitKaartideJarjendMuutujaNimi(@NotNull SmapsParser.KaartideJarjendMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#JooneParameeterTuup}.
	 * @param ctx the parse tree
	 */
	void enterJooneParameeterTuup(@NotNull SmapsParser.JooneParameeterTuupContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#JooneParameeterTuup}.
	 * @param ctx the parse tree
	 */
	void exitJooneParameeterTuup(@NotNull SmapsParser.JooneParameeterTuupContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktiParameeterNimi}.
	 * @param ctx the parse tree
	 */
	void enterPunktiParameeterNimi(@NotNull SmapsParser.PunktiParameeterNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktiParameeterNimi}.
	 * @param ctx the parse tree
	 */
	void exitPunktiParameeterNimi(@NotNull SmapsParser.PunktiParameeterNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ToevaartusteJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterToevaartusteJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ToevaartusteJarjendiAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ToevaartusteJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitToevaartusteJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ToevaartusteJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PikkuskoordinaatPikkuskraad}.
	 * @param ctx the parse tree
	 */
	void enterPikkuskoordinaatPikkuskraad(@NotNull SmapsParser.PikkuskoordinaatPikkuskraadContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PikkuskoordinaatPikkuskraad}.
	 * @param ctx the parse tree
	 */
	void exitPikkuskoordinaatPikkuskraad(@NotNull SmapsParser.PikkuskoordinaatPikkuskraadContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktideJarjendJarjend}.
	 * @param ctx the parse tree
	 */
	void enterPunktideJarjendJarjend(@NotNull SmapsParser.PunktideJarjendJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktideJarjendJarjend}.
	 * @param ctx the parse tree
	 */
	void exitPunktideJarjendJarjend(@NotNull SmapsParser.PunktideJarjendJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterPunktideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.PunktideJarjendiAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitPunktideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.PunktideJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ObjektideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterObjektideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ObjektideJarjendiAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ObjektideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitObjektideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ObjektideJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ObjektiParameeterKuva}.
	 * @param ctx the parse tree
	 */
	void enterObjektiParameeterKuva(@NotNull SmapsParser.ObjektiParameeterKuvaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ObjektiParameeterKuva}.
	 * @param ctx the parse tree
	 */
	void exitObjektiParameeterKuva(@NotNull SmapsParser.ObjektiParameeterKuvaContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaardiParameeterTuup}.
	 * @param ctx the parse tree
	 */
	void enterKaardiParameeterTuup(@NotNull SmapsParser.KaardiParameeterTuupContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaardiParameeterTuup}.
	 * @param ctx the parse tree
	 */
	void exitKaardiParameeterTuup(@NotNull SmapsParser.KaardiParameeterTuupContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisPunktideJarjend}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisPunktideJarjend(@NotNull SmapsParser.VordlusAvaldisPunktideJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisPunktideJarjend}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisPunktideJarjend(@NotNull SmapsParser.VordlusAvaldisPunktideJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#LaiuskraadLouna}.
	 * @param ctx the parse tree
	 */
	void enterLaiuskraadLouna(@NotNull SmapsParser.LaiuskraadLounaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#LaiuskraadLouna}.
	 * @param ctx the parse tree
	 */
	void exitLaiuskraadLouna(@NotNull SmapsParser.LaiuskraadLounaContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#SonedeJarjendiAvaldisSonedeJarjend}.
	 * @param ctx the parse tree
	 */
	void enterSonedeJarjendiAvaldisSonedeJarjend(@NotNull SmapsParser.SonedeJarjendiAvaldisSonedeJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#SonedeJarjendiAvaldisSonedeJarjend}.
	 * @param ctx the parse tree
	 */
	void exitSonedeJarjendiAvaldisSonedeJarjend(@NotNull SmapsParser.SonedeJarjendiAvaldisSonedeJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ObjektiParameeterMootmed}.
	 * @param ctx the parse tree
	 */
	void enterObjektiParameeterMootmed(@NotNull SmapsParser.ObjektiParameeterMootmedContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ObjektiParameeterMootmed}.
	 * @param ctx the parse tree
	 */
	void exitObjektiParameeterMootmed(@NotNull SmapsParser.ObjektiParameeterMootmedContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ToevaartusteJarjendiAvaldisToevaartusteJarjend}.
	 * @param ctx the parse tree
	 */
	void enterToevaartusteJarjendiAvaldisToevaartusteJarjend(@NotNull SmapsParser.ToevaartusteJarjendiAvaldisToevaartusteJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ToevaartusteJarjendiAvaldisToevaartusteJarjend}.
	 * @param ctx the parse tree
	 */
	void exitToevaartusteJarjendiAvaldisToevaartusteJarjend(@NotNull SmapsParser.ToevaartusteJarjendiAvaldisToevaartusteJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#JooneParameeterNimi}.
	 * @param ctx the parse tree
	 */
	void enterJooneParameeterNimi(@NotNull SmapsParser.JooneParameeterNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#JooneParameeterNimi}.
	 * @param ctx the parse tree
	 */
	void exitJooneParameeterNimi(@NotNull SmapsParser.JooneParameeterNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaardiParameeterKaardid}.
	 * @param ctx the parse tree
	 */
	void enterKaardiParameeterKaardid(@NotNull SmapsParser.KaardiParameeterKaardidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaardiParameeterKaardid}.
	 * @param ctx the parse tree
	 */
	void exitKaardiParameeterKaardid(@NotNull SmapsParser.KaardiParameeterKaardidContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#JoonteJarjendiAvaldisJoonteJarjend}.
	 * @param ctx the parse tree
	 */
	void enterJoonteJarjendiAvaldisJoonteJarjend(@NotNull SmapsParser.JoonteJarjendiAvaldisJoonteJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#JoonteJarjendiAvaldisJoonteJarjend}.
	 * @param ctx the parse tree
	 */
	void exitJoonteJarjendiAvaldisJoonteJarjend(@NotNull SmapsParser.JoonteJarjendiAvaldisJoonteJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeAvaldisKorrutamineJagamine}.
	 * @param ctx the parse tree
	 */
	void enterArvudeAvaldisKorrutamineJagamine(@NotNull SmapsParser.ArvudeAvaldisKorrutamineJagamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeAvaldisKorrutamineJagamine}.
	 * @param ctx the parse tree
	 */
	void exitArvudeAvaldisKorrutamineJagamine(@NotNull SmapsParser.ArvudeAvaldisKorrutamineJagamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#toevaartusteJarjend}.
	 * @param ctx the parse tree
	 */
	void enterToevaartusteJarjend(@NotNull SmapsParser.ToevaartusteJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#toevaartusteJarjend}.
	 * @param ctx the parse tree
	 */
	void exitToevaartusteJarjend(@NotNull SmapsParser.ToevaartusteJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PikkuskraadLaas}.
	 * @param ctx the parse tree
	 */
	void enterPikkuskraadLaas(@NotNull SmapsParser.PikkuskraadLaasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PikkuskraadLaas}.
	 * @param ctx the parse tree
	 */
	void exitPikkuskraadLaas(@NotNull SmapsParser.PikkuskraadLaasContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#SonedeAvaldisSone}.
	 * @param ctx the parse tree
	 */
	void enterSonedeAvaldisSone(@NotNull SmapsParser.SonedeAvaldisSoneContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#SonedeAvaldisSone}.
	 * @param ctx the parse tree
	 */
	void exitSonedeAvaldisSone(@NotNull SmapsParser.SonedeAvaldisSoneContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#valikulauseElse}.
	 * @param ctx the parse tree
	 */
	void enterValikulauseElse(@NotNull SmapsParser.ValikulauseElseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#valikulauseElse}.
	 * @param ctx the parse tree
	 */
	void exitValikulauseElse(@NotNull SmapsParser.ValikulauseElseContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktiParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void enterPunktiParameeterKoordinaadid(@NotNull SmapsParser.PunktiParameeterKoordinaadidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktiParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void exitPunktiParameeterKoordinaadid(@NotNull SmapsParser.PunktiParameeterKoordinaadidContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#JoonteJarjendJarjend}.
	 * @param ctx the parse tree
	 */
	void enterJoonteJarjendJarjend(@NotNull SmapsParser.JoonteJarjendJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#JoonteJarjendJarjend}.
	 * @param ctx the parse tree
	 */
	void exitJoonteJarjendJarjend(@NotNull SmapsParser.JoonteJarjendJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ObjektiParameeterNimi}.
	 * @param ctx the parse tree
	 */
	void enterObjektiParameeterNimi(@NotNull SmapsParser.ObjektiParameeterNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ObjektiParameeterNimi}.
	 * @param ctx the parse tree
	 */
	void exitObjektiParameeterNimi(@NotNull SmapsParser.ObjektiParameeterNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#aritmeetilineAvaldisArv}.
	 * @param ctx the parse tree
	 */
	void enterAritmeetilineAvaldisArv(@NotNull SmapsParser.AritmeetilineAvaldisArvContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#aritmeetilineAvaldisArv}.
	 * @param ctx the parse tree
	 */
	void exitAritmeetilineAvaldisArv(@NotNull SmapsParser.AritmeetilineAvaldisArvContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#LaiuskoordinaatLaiuskraad}.
	 * @param ctx the parse tree
	 */
	void enterLaiuskoordinaatLaiuskraad(@NotNull SmapsParser.LaiuskoordinaatLaiuskraadContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#LaiuskoordinaatLaiuskraad}.
	 * @param ctx the parse tree
	 */
	void exitLaiuskoordinaatLaiuskraad(@NotNull SmapsParser.LaiuskoordinaatLaiuskraadContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#AladeJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterAladeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.AladeJarjendiAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#AladeJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitAladeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.AladeJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#LaiuskraadPohi}.
	 * @param ctx the parse tree
	 */
	void enterLaiuskraadPohi(@NotNull SmapsParser.LaiuskraadPohiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#LaiuskraadPohi}.
	 * @param ctx the parse tree
	 */
	void exitLaiuskraadPohi(@NotNull SmapsParser.LaiuskraadPohiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeAvaldisMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterArvudeAvaldisMuutujaNimi(@NotNull SmapsParser.ArvudeAvaldisMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeAvaldisMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitArvudeAvaldisMuutujaNimi(@NotNull SmapsParser.ArvudeAvaldisMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaardiParameeterAlad}.
	 * @param ctx the parse tree
	 */
	void enterKaardiParameeterAlad(@NotNull SmapsParser.KaardiParameeterAladContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaardiParameeterAlad}.
	 * @param ctx the parse tree
	 */
	void exitKaardiParameeterAlad(@NotNull SmapsParser.KaardiParameeterAladContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ObjektideJarjendJarjend}.
	 * @param ctx the parse tree
	 */
	void enterObjektideJarjendJarjend(@NotNull SmapsParser.ObjektideJarjendJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ObjektideJarjendJarjend}.
	 * @param ctx the parse tree
	 */
	void exitObjektideJarjendJarjend(@NotNull SmapsParser.ObjektideJarjendJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KorduslauseDoWhile}.
	 * @param ctx the parse tree
	 */
	void enterKorduslauseDoWhile(@NotNull SmapsParser.KorduslauseDoWhileContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KorduslauseDoWhile}.
	 * @param ctx the parse tree
	 */
	void exitKorduslauseDoWhile(@NotNull SmapsParser.KorduslauseDoWhileContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisKaartidedeJarjend}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisKaartidedeJarjend(@NotNull SmapsParser.VordlusAvaldisKaartidedeJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisKaartidedeJarjend}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisKaartidedeJarjend(@NotNull SmapsParser.VordlusAvaldisKaartidedeJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktiParameeterKuva}.
	 * @param ctx the parse tree
	 */
	void enterPunktiParameeterKuva(@NotNull SmapsParser.PunktiParameeterKuvaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktiParameeterKuva}.
	 * @param ctx the parse tree
	 */
	void exitPunktiParameeterKuva(@NotNull SmapsParser.PunktiParameeterKuvaContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaardiParameeterPunktid}.
	 * @param ctx the parse tree
	 */
	void enterKaardiParameeterPunktid(@NotNull SmapsParser.KaardiParameeterPunktidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaardiParameeterPunktid}.
	 * @param ctx the parse tree
	 */
	void exitKaardiParameeterPunktid(@NotNull SmapsParser.KaardiParameeterPunktidContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#struktuurideJarjend}.
	 * @param ctx the parse tree
	 */
	void enterStruktuurideJarjend(@NotNull SmapsParser.StruktuurideJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#struktuurideJarjend}.
	 * @param ctx the parse tree
	 */
	void exitStruktuurideJarjend(@NotNull SmapsParser.StruktuurideJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#SonedeAvaldisMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterSonedeAvaldisMuutujaNimi(@NotNull SmapsParser.SonedeAvaldisMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#SonedeAvaldisMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitSonedeAvaldisMuutujaNimi(@NotNull SmapsParser.SonedeAvaldisMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#Pikkuskraad1}.
	 * @param ctx the parse tree
	 */
	void enterPikkuskraad1(@NotNull SmapsParser.Pikkuskraad1Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#Pikkuskraad1}.
	 * @param ctx the parse tree
	 */
	void exitPikkuskraad1(@NotNull SmapsParser.Pikkuskraad1Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#Pikkuskraad4}.
	 * @param ctx the parse tree
	 */
	void enterPikkuskraad4(@NotNull SmapsParser.Pikkuskraad4Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#Pikkuskraad4}.
	 * @param ctx the parse tree
	 */
	void exitPikkuskraad4(@NotNull SmapsParser.Pikkuskraad4Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#Pikkuskraad2}.
	 * @param ctx the parse tree
	 */
	void enterPikkuskraad2(@NotNull SmapsParser.Pikkuskraad2Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#Pikkuskraad2}.
	 * @param ctx the parse tree
	 */
	void exitPikkuskraad2(@NotNull SmapsParser.Pikkuskraad2Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KoordinaatideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterKoordinaatideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.KoordinaatideJarjendiAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KoordinaatideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitKoordinaatideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.KoordinaatideJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#Pikkuskraad3}.
	 * @param ctx the parse tree
	 */
	void enterPikkuskraad3(@NotNull SmapsParser.Pikkuskraad3Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#Pikkuskraad3}.
	 * @param ctx the parse tree
	 */
	void exitPikkuskraad3(@NotNull SmapsParser.Pikkuskraad3Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#JooneParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void enterJooneParameeterKoordinaadid(@NotNull SmapsParser.JooneParameeterKoordinaadidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#JooneParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void exitJooneParameeterKoordinaadid(@NotNull SmapsParser.JooneParameeterKoordinaadidContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeAvaldisSuluavaldis}.
	 * @param ctx the parse tree
	 */
	void enterArvudeAvaldisSuluavaldis(@NotNull SmapsParser.ArvudeAvaldisSuluavaldisContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeAvaldisSuluavaldis}.
	 * @param ctx the parse tree
	 */
	void exitArvudeAvaldisSuluavaldis(@NotNull SmapsParser.ArvudeAvaldisSuluavaldisContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeAvaldisArv}.
	 * @param ctx the parse tree
	 */
	void enterArvudeAvaldisArv(@NotNull SmapsParser.ArvudeAvaldisArvContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeAvaldisArv}.
	 * @param ctx the parse tree
	 */
	void exitArvudeAvaldisArv(@NotNull SmapsParser.ArvudeAvaldisArvContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#valikulauseIf}.
	 * @param ctx the parse tree
	 */
	void enterValikulauseIf(@NotNull SmapsParser.ValikulauseIfContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#valikulauseIf}.
	 * @param ctx the parse tree
	 */
	void exitValikulauseIf(@NotNull SmapsParser.ValikulauseIfContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#struktuuriDeklaratsioon}.
	 * @param ctx the parse tree
	 */
	void enterStruktuuriDeklaratsioon(@NotNull SmapsParser.StruktuuriDeklaratsioonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#struktuuriDeklaratsioon}.
	 * @param ctx the parse tree
	 */
	void exitStruktuuriDeklaratsioon(@NotNull SmapsParser.StruktuuriDeklaratsioonContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#valikulauseElseIf}.
	 * @param ctx the parse tree
	 */
	void enterValikulauseElseIf(@NotNull SmapsParser.ValikulauseElseIfContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#valikulauseElseIf}.
	 * @param ctx the parse tree
	 */
	void exitValikulauseElseIf(@NotNull SmapsParser.ValikulauseElseIfContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisToevaartused}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisToevaartused(@NotNull SmapsParser.VordlusAvaldisToevaartusedContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisToevaartused}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisToevaartused(@NotNull SmapsParser.VordlusAvaldisToevaartusedContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaartideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void enterKaartideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.KaartideJarjendiAvaldisLiitmineLahutamineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaartideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 */
	void exitKaartideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.KaartideJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#AlaParameeterNimi}.
	 * @param ctx the parse tree
	 */
	void enterAlaParameeterNimi(@NotNull SmapsParser.AlaParameeterNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#AlaParameeterNimi}.
	 * @param ctx the parse tree
	 */
	void exitAlaParameeterNimi(@NotNull SmapsParser.AlaParameeterNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktideJarjendiAvaldisPunktideJarjend}.
	 * @param ctx the parse tree
	 */
	void enterPunktideJarjendiAvaldisPunktideJarjend(@NotNull SmapsParser.PunktideJarjendiAvaldisPunktideJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktideJarjendiAvaldisPunktideJarjend}.
	 * @param ctx the parse tree
	 */
	void exitPunktideJarjendiAvaldisPunktideJarjend(@NotNull SmapsParser.PunktideJarjendiAvaldisPunktideJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KorduslauseFor}.
	 * @param ctx the parse tree
	 */
	void enterKorduslauseFor(@NotNull SmapsParser.KorduslauseForContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KorduslauseFor}.
	 * @param ctx the parse tree
	 */
	void exitKorduslauseFor(@NotNull SmapsParser.KorduslauseForContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeJarjendiAvaldisArvudeJarjend}.
	 * @param ctx the parse tree
	 */
	void enterArvudeJarjendiAvaldisArvudeJarjend(@NotNull SmapsParser.ArvudeJarjendiAvaldisArvudeJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeJarjendiAvaldisArvudeJarjend}.
	 * @param ctx the parse tree
	 */
	void exitArvudeJarjendiAvaldisArvudeJarjend(@NotNull SmapsParser.ArvudeJarjendiAvaldisArvudeJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ObjektideJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterObjektideJarjendMuutujaNimi(@NotNull SmapsParser.ObjektideJarjendMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ObjektideJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitObjektideJarjendMuutujaNimi(@NotNull SmapsParser.ObjektideJarjendMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#JooneParameeterKuva}.
	 * @param ctx the parse tree
	 */
	void enterJooneParameeterKuva(@NotNull SmapsParser.JooneParameeterKuvaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#JooneParameeterKuva}.
	 * @param ctx the parse tree
	 */
	void exitJooneParameeterKuva(@NotNull SmapsParser.JooneParameeterKuvaContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#VordlusAvaldisAladeJarjend}.
	 * @param ctx the parse tree
	 */
	void enterVordlusAvaldisAladeJarjend(@NotNull SmapsParser.VordlusAvaldisAladeJarjendContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#VordlusAvaldisAladeJarjend}.
	 * @param ctx the parse tree
	 */
	void exitVordlusAvaldisAladeJarjend(@NotNull SmapsParser.VordlusAvaldisAladeJarjendContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeAvaldisTriviaalne1}.
	 * @param ctx the parse tree
	 */
	void enterArvudeAvaldisTriviaalne1(@NotNull SmapsParser.ArvudeAvaldisTriviaalne1Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeAvaldisTriviaalne1}.
	 * @param ctx the parse tree
	 */
	void exitArvudeAvaldisTriviaalne1(@NotNull SmapsParser.ArvudeAvaldisTriviaalne1Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#SoneSone}.
	 * @param ctx the parse tree
	 */
	void enterSoneSone(@NotNull SmapsParser.SoneSoneContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#SoneSone}.
	 * @param ctx the parse tree
	 */
	void exitSoneSone(@NotNull SmapsParser.SoneSoneContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeAvaldisTriviaalne3}.
	 * @param ctx the parse tree
	 */
	void enterArvudeAvaldisTriviaalne3(@NotNull SmapsParser.ArvudeAvaldisTriviaalne3Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeAvaldisTriviaalne3}.
	 * @param ctx the parse tree
	 */
	void exitArvudeAvaldisTriviaalne3(@NotNull SmapsParser.ArvudeAvaldisTriviaalne3Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#KaardiParameeterJooned}.
	 * @param ctx the parse tree
	 */
	void enterKaardiParameeterJooned(@NotNull SmapsParser.KaardiParameeterJoonedContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#KaardiParameeterJooned}.
	 * @param ctx the parse tree
	 */
	void exitKaardiParameeterJooned(@NotNull SmapsParser.KaardiParameeterJoonedContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeAvaldisTriviaalne2}.
	 * @param ctx the parse tree
	 */
	void enterArvudeAvaldisTriviaalne2(@NotNull SmapsParser.ArvudeAvaldisTriviaalne2Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeAvaldisTriviaalne2}.
	 * @param ctx the parse tree
	 */
	void exitArvudeAvaldisTriviaalne2(@NotNull SmapsParser.ArvudeAvaldisTriviaalne2Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ObjektiParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void enterObjektiParameeterKoordinaadid(@NotNull SmapsParser.ObjektiParameeterKoordinaadidContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ObjektiParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 */
	void exitObjektiParameeterKoordinaadid(@NotNull SmapsParser.ObjektiParameeterKoordinaadidContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#Laiuskraad1}.
	 * @param ctx the parse tree
	 */
	void enterLaiuskraad1(@NotNull SmapsParser.Laiuskraad1Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#Laiuskraad1}.
	 * @param ctx the parse tree
	 */
	void exitLaiuskraad1(@NotNull SmapsParser.Laiuskraad1Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#Laiuskraad2}.
	 * @param ctx the parse tree
	 */
	void enterLaiuskraad2(@NotNull SmapsParser.Laiuskraad2Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#Laiuskraad2}.
	 * @param ctx the parse tree
	 */
	void exitLaiuskraad2(@NotNull SmapsParser.Laiuskraad2Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#Laiuskraad3}.
	 * @param ctx the parse tree
	 */
	void enterLaiuskraad3(@NotNull SmapsParser.Laiuskraad3Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#Laiuskraad3}.
	 * @param ctx the parse tree
	 */
	void exitLaiuskraad3(@NotNull SmapsParser.Laiuskraad3Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#PunktiKoordinaadidMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void enterPunktiKoordinaadidMuutujaNimi(@NotNull SmapsParser.PunktiKoordinaadidMuutujaNimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#PunktiKoordinaadidMuutujaNimi}.
	 * @param ctx the parse tree
	 */
	void exitPunktiKoordinaadidMuutujaNimi(@NotNull SmapsParser.PunktiKoordinaadidMuutujaNimiContext ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#Laiuskraad4}.
	 * @param ctx the parse tree
	 */
	void enterLaiuskraad4(@NotNull SmapsParser.Laiuskraad4Context ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#Laiuskraad4}.
	 * @param ctx the parse tree
	 */
	void exitLaiuskraad4(@NotNull SmapsParser.Laiuskraad4Context ctx);

	/**
	 * Enter a parse tree produced by {@link SmapsParser#ArvudeAvaldisUnaarneMiinus}.
	 * @param ctx the parse tree
	 */
	void enterArvudeAvaldisUnaarneMiinus(@NotNull SmapsParser.ArvudeAvaldisUnaarneMiinusContext ctx);
	/**
	 * Exit a parse tree produced by {@link SmapsParser#ArvudeAvaldisUnaarneMiinus}.
	 * @param ctx the parse tree
	 */
	void exitArvudeAvaldisUnaarneMiinus(@NotNull SmapsParser.ArvudeAvaldisUnaarneMiinusContext ctx);
}