// Generated from C:/Users/Ants-Oskar/Documents/Bitbucket/smaps/src\Smaps.g4 by ANTLR 4.x

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * This class provides an empty implementation of {@link SmapsListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
public class SmapsBaseListener implements SmapsListener {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAladeJarjendMuutujaNimi(@NotNull SmapsParser.AladeJarjendMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAladeJarjendMuutujaNimi(@NotNull SmapsParser.AladeJarjendMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisToevaartusteJarjend(@NotNull SmapsParser.VordlusAvaldisToevaartusteJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisToevaartusteJarjend(@NotNull SmapsParser.VordlusAvaldisToevaartusteJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterObjektideJarjendiAvaldisObjektideJarjend(@NotNull SmapsParser.ObjektideJarjendiAvaldisObjektideJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitObjektideJarjendiAvaldisObjektideJarjend(@NotNull SmapsParser.ObjektideJarjendiAvaldisObjektideJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterObjektiParameeterTuup(@NotNull SmapsParser.ObjektiParameeterTuupContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitObjektiParameeterTuup(@NotNull SmapsParser.ObjektiParameeterTuupContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ArvudeJarjendiAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ArvudeJarjendiAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLaiuskoordinaatideAvaldisLiitmineLahutamine(@NotNull SmapsParser.LaiuskoordinaatideAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLaiuskoordinaatideAvaldisLiitmineLahutamine(@NotNull SmapsParser.LaiuskoordinaatideAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJoonteJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.JoonteJarjendiAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJoonteJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.JoonteJarjendiAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAlaParameeterKoordinaadid(@NotNull SmapsParser.AlaParameeterKoordinaadidContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAlaParameeterKoordinaadid(@NotNull SmapsParser.AlaParameeterKoordinaadidContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisJoonteJarjend(@NotNull SmapsParser.VordlusAvaldisJoonteJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisJoonteJarjend(@NotNull SmapsParser.VordlusAvaldisJoonteJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktiKoordinaadidPunkt(@NotNull SmapsParser.PunktiKoordinaadidPunktContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktiKoordinaadidPunkt(@NotNull SmapsParser.PunktiKoordinaadidPunktContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterValikulause(@NotNull SmapsParser.ValikulauseContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitValikulause(@NotNull SmapsParser.ValikulauseContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSonedeJarjend(@NotNull SmapsParser.SonedeJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSonedeJarjend(@NotNull SmapsParser.SonedeJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKorduslauseWhile(@NotNull SmapsParser.KorduslauseWhileContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKorduslauseWhile(@NotNull SmapsParser.KorduslauseWhileContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterOmistamine(@NotNull SmapsParser.OmistamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitOmistamine(@NotNull SmapsParser.OmistamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPikkuskoordinaatideAvaldisPikkuskoordinaat(@NotNull SmapsParser.PikkuskoordinaatideAvaldisPikkuskoordinaatContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPikkuskoordinaatideAvaldisPikkuskoordinaat(@NotNull SmapsParser.PikkuskoordinaatideAvaldisPikkuskoordinaatContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterProgramm(@NotNull SmapsParser.ProgrammContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitProgramm(@NotNull SmapsParser.ProgrammContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisSonedeJarjend(@NotNull SmapsParser.VordlusAvaldisSonedeJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisSonedeJarjend(@NotNull SmapsParser.VordlusAvaldisSonedeJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKoordinaatideJarjendiAvaldisKoordinaatideJarjend(@NotNull SmapsParser.KoordinaatideJarjendiAvaldisKoordinaatideJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKoordinaatideJarjendiAvaldisKoordinaatideJarjend(@NotNull SmapsParser.KoordinaatideJarjendiAvaldisKoordinaatideJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaardiParameeterNimi(@NotNull SmapsParser.KaardiParameeterNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaardiParameeterNimi(@NotNull SmapsParser.KaardiParameeterNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktMuutujaNimi(@NotNull SmapsParser.PunktMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktMuutujaNimi(@NotNull SmapsParser.PunktMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAlaParameeterKuva(@NotNull SmapsParser.AlaParameeterKuvaContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAlaParameeterKuva(@NotNull SmapsParser.AlaParameeterKuvaContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKommentaar(@NotNull SmapsParser.KommentaarContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKommentaar(@NotNull SmapsParser.KommentaarContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAlaParameeterTuup(@NotNull SmapsParser.AlaParameeterTuupContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAlaParameeterTuup(@NotNull SmapsParser.AlaParameeterTuupContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeJarjend(@NotNull SmapsParser.ArvudeJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeJarjend(@NotNull SmapsParser.ArvudeJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeAvaldisLiitmineLahutamine(@NotNull SmapsParser.ArvudeAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeAvaldisLiitmineLahutamine(@NotNull SmapsParser.ArvudeAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaartideJarjendiAvaldisKaartideJarjend(@NotNull SmapsParser.KaartideJarjendiAvaldisKaartideJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaartideJarjendiAvaldisKaartideJarjend(@NotNull SmapsParser.KaartideJarjendiAvaldisKaartideJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSoneMuutujaNimi(@NotNull SmapsParser.SoneMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSoneMuutujaNimi(@NotNull SmapsParser.SoneMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisSone(@NotNull SmapsParser.VordlusAvaldisSoneContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisSone(@NotNull SmapsParser.VordlusAvaldisSoneContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPikkuskoordinaatideAvaldisLiitmineLahutamine(@NotNull SmapsParser.PikkuskoordinaatideAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPikkuskoordinaatideAvaldisLiitmineLahutamine(@NotNull SmapsParser.PikkuskoordinaatideAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktKoordinaadid(@NotNull SmapsParser.PunktKoordinaadidContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktKoordinaadid(@NotNull SmapsParser.PunktKoordinaadidContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaartideJarjendJarjend(@NotNull SmapsParser.KaartideJarjendJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaartideJarjendJarjend(@NotNull SmapsParser.KaartideJarjendJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaardiParameeterObjektid(@NotNull SmapsParser.KaardiParameeterObjektidContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaardiParameeterObjektid(@NotNull SmapsParser.KaardiParameeterObjektidContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLaiuskoordinaatMuutujaNimi(@NotNull SmapsParser.LaiuskoordinaatMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLaiuskoordinaatMuutujaNimi(@NotNull SmapsParser.LaiuskoordinaatMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisObjektideJarjend(@NotNull SmapsParser.VordlusAvaldisObjektideJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisObjektideJarjend(@NotNull SmapsParser.VordlusAvaldisObjektideJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisToevaartus(@NotNull SmapsParser.VordlusAvaldisToevaartusContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisToevaartus(@NotNull SmapsParser.VordlusAvaldisToevaartusContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLause(@NotNull SmapsParser.LauseContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLause(@NotNull SmapsParser.LauseContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisKoordinaatideJarjend(@NotNull SmapsParser.VordlusAvaldisKoordinaatideJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisKoordinaatideJarjend(@NotNull SmapsParser.VordlusAvaldisKoordinaatideJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisArvud(@NotNull SmapsParser.VordlusAvaldisArvudContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisArvud(@NotNull SmapsParser.VordlusAvaldisArvudContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisArvudeJarjend(@NotNull SmapsParser.VordlusAvaldisArvudeJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisArvudeJarjend(@NotNull SmapsParser.VordlusAvaldisArvudeJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSonedeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.SonedeJarjendiAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSonedeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.SonedeJarjendiAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktideJarjendMuutujaNimi(@NotNull SmapsParser.PunktideJarjendMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktideJarjendMuutujaNimi(@NotNull SmapsParser.PunktideJarjendMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKoordinaatideJarjend(@NotNull SmapsParser.KoordinaatideJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKoordinaatideJarjend(@NotNull SmapsParser.KoordinaatideJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPikkuskraadIda(@NotNull SmapsParser.PikkuskraadIdaContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPikkuskraadIda(@NotNull SmapsParser.PikkuskraadIdaContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAladeJarjendiAvaldisAladeJarjend(@NotNull SmapsParser.AladeJarjendiAvaldisAladeJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAladeJarjendiAvaldisAladeJarjend(@NotNull SmapsParser.AladeJarjendiAvaldisAladeJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMuutujaDeklaratsioon(@NotNull SmapsParser.MuutujaDeklaratsioonContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMuutujaDeklaratsioon(@NotNull SmapsParser.MuutujaDeklaratsioonContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisKoordinaadid(@NotNull SmapsParser.VordlusAvaldisKoordinaadidContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisKoordinaadid(@NotNull SmapsParser.VordlusAvaldisKoordinaadidContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJoonteJarjendMuutujaNimi(@NotNull SmapsParser.JoonteJarjendMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJoonteJarjendMuutujaNimi(@NotNull SmapsParser.JoonteJarjendMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAladeJarjendJarjend(@NotNull SmapsParser.AladeJarjendJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAladeJarjendJarjend(@NotNull SmapsParser.AladeJarjendJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPikkuskoordinaatMuutujaNimi(@NotNull SmapsParser.PikkuskoordinaatMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPikkuskoordinaatMuutujaNimi(@NotNull SmapsParser.PikkuskoordinaatMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLaiuskoordinaatideAvaldisLaiuskoordinaat(@NotNull SmapsParser.LaiuskoordinaatideAvaldisLaiuskoordinaatContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLaiuskoordinaatideAvaldisLaiuskoordinaat(@NotNull SmapsParser.LaiuskoordinaatideAvaldisLaiuskoordinaatContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaartideJarjendMuutujaNimi(@NotNull SmapsParser.KaartideJarjendMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaartideJarjendMuutujaNimi(@NotNull SmapsParser.KaartideJarjendMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJooneParameeterTuup(@NotNull SmapsParser.JooneParameeterTuupContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJooneParameeterTuup(@NotNull SmapsParser.JooneParameeterTuupContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktiParameeterNimi(@NotNull SmapsParser.PunktiParameeterNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktiParameeterNimi(@NotNull SmapsParser.PunktiParameeterNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterToevaartusteJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ToevaartusteJarjendiAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitToevaartusteJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ToevaartusteJarjendiAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPikkuskoordinaatPikkuskraad(@NotNull SmapsParser.PikkuskoordinaatPikkuskraadContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPikkuskoordinaatPikkuskraad(@NotNull SmapsParser.PikkuskoordinaatPikkuskraadContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktideJarjendJarjend(@NotNull SmapsParser.PunktideJarjendJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktideJarjendJarjend(@NotNull SmapsParser.PunktideJarjendJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.PunktideJarjendiAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.PunktideJarjendiAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterObjektideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ObjektideJarjendiAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitObjektideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ObjektideJarjendiAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterObjektiParameeterKuva(@NotNull SmapsParser.ObjektiParameeterKuvaContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitObjektiParameeterKuva(@NotNull SmapsParser.ObjektiParameeterKuvaContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaardiParameeterTuup(@NotNull SmapsParser.KaardiParameeterTuupContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaardiParameeterTuup(@NotNull SmapsParser.KaardiParameeterTuupContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisPunktideJarjend(@NotNull SmapsParser.VordlusAvaldisPunktideJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisPunktideJarjend(@NotNull SmapsParser.VordlusAvaldisPunktideJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLaiuskraadLouna(@NotNull SmapsParser.LaiuskraadLounaContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLaiuskraadLouna(@NotNull SmapsParser.LaiuskraadLounaContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSonedeJarjendiAvaldisSonedeJarjend(@NotNull SmapsParser.SonedeJarjendiAvaldisSonedeJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSonedeJarjendiAvaldisSonedeJarjend(@NotNull SmapsParser.SonedeJarjendiAvaldisSonedeJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterObjektiParameeterMootmed(@NotNull SmapsParser.ObjektiParameeterMootmedContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitObjektiParameeterMootmed(@NotNull SmapsParser.ObjektiParameeterMootmedContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterToevaartusteJarjendiAvaldisToevaartusteJarjend(@NotNull SmapsParser.ToevaartusteJarjendiAvaldisToevaartusteJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitToevaartusteJarjendiAvaldisToevaartusteJarjend(@NotNull SmapsParser.ToevaartusteJarjendiAvaldisToevaartusteJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJooneParameeterNimi(@NotNull SmapsParser.JooneParameeterNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJooneParameeterNimi(@NotNull SmapsParser.JooneParameeterNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaardiParameeterKaardid(@NotNull SmapsParser.KaardiParameeterKaardidContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaardiParameeterKaardid(@NotNull SmapsParser.KaardiParameeterKaardidContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJoonteJarjendiAvaldisJoonteJarjend(@NotNull SmapsParser.JoonteJarjendiAvaldisJoonteJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJoonteJarjendiAvaldisJoonteJarjend(@NotNull SmapsParser.JoonteJarjendiAvaldisJoonteJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeAvaldisKorrutamineJagamine(@NotNull SmapsParser.ArvudeAvaldisKorrutamineJagamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeAvaldisKorrutamineJagamine(@NotNull SmapsParser.ArvudeAvaldisKorrutamineJagamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterToevaartusteJarjend(@NotNull SmapsParser.ToevaartusteJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitToevaartusteJarjend(@NotNull SmapsParser.ToevaartusteJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPikkuskraadLaas(@NotNull SmapsParser.PikkuskraadLaasContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPikkuskraadLaas(@NotNull SmapsParser.PikkuskraadLaasContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSonedeAvaldisSone(@NotNull SmapsParser.SonedeAvaldisSoneContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSonedeAvaldisSone(@NotNull SmapsParser.SonedeAvaldisSoneContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterValikulauseElse(@NotNull SmapsParser.ValikulauseElseContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitValikulauseElse(@NotNull SmapsParser.ValikulauseElseContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktiParameeterKoordinaadid(@NotNull SmapsParser.PunktiParameeterKoordinaadidContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktiParameeterKoordinaadid(@NotNull SmapsParser.PunktiParameeterKoordinaadidContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJoonteJarjendJarjend(@NotNull SmapsParser.JoonteJarjendJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJoonteJarjendJarjend(@NotNull SmapsParser.JoonteJarjendJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterObjektiParameeterNimi(@NotNull SmapsParser.ObjektiParameeterNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitObjektiParameeterNimi(@NotNull SmapsParser.ObjektiParameeterNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAritmeetilineAvaldisArv(@NotNull SmapsParser.AritmeetilineAvaldisArvContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAritmeetilineAvaldisArv(@NotNull SmapsParser.AritmeetilineAvaldisArvContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLaiuskoordinaatLaiuskraad(@NotNull SmapsParser.LaiuskoordinaatLaiuskraadContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLaiuskoordinaatLaiuskraad(@NotNull SmapsParser.LaiuskoordinaatLaiuskraadContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAladeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.AladeJarjendiAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAladeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.AladeJarjendiAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLaiuskraadPohi(@NotNull SmapsParser.LaiuskraadPohiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLaiuskraadPohi(@NotNull SmapsParser.LaiuskraadPohiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeAvaldisMuutujaNimi(@NotNull SmapsParser.ArvudeAvaldisMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeAvaldisMuutujaNimi(@NotNull SmapsParser.ArvudeAvaldisMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaardiParameeterAlad(@NotNull SmapsParser.KaardiParameeterAladContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaardiParameeterAlad(@NotNull SmapsParser.KaardiParameeterAladContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterObjektideJarjendJarjend(@NotNull SmapsParser.ObjektideJarjendJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitObjektideJarjendJarjend(@NotNull SmapsParser.ObjektideJarjendJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKorduslauseDoWhile(@NotNull SmapsParser.KorduslauseDoWhileContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKorduslauseDoWhile(@NotNull SmapsParser.KorduslauseDoWhileContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisKaartidedeJarjend(@NotNull SmapsParser.VordlusAvaldisKaartidedeJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisKaartidedeJarjend(@NotNull SmapsParser.VordlusAvaldisKaartidedeJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktiParameeterKuva(@NotNull SmapsParser.PunktiParameeterKuvaContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktiParameeterKuva(@NotNull SmapsParser.PunktiParameeterKuvaContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaardiParameeterPunktid(@NotNull SmapsParser.KaardiParameeterPunktidContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaardiParameeterPunktid(@NotNull SmapsParser.KaardiParameeterPunktidContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStruktuurideJarjend(@NotNull SmapsParser.StruktuurideJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStruktuurideJarjend(@NotNull SmapsParser.StruktuurideJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSonedeAvaldisMuutujaNimi(@NotNull SmapsParser.SonedeAvaldisMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSonedeAvaldisMuutujaNimi(@NotNull SmapsParser.SonedeAvaldisMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPikkuskraad1(@NotNull SmapsParser.Pikkuskraad1Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPikkuskraad1(@NotNull SmapsParser.Pikkuskraad1Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPikkuskraad4(@NotNull SmapsParser.Pikkuskraad4Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPikkuskraad4(@NotNull SmapsParser.Pikkuskraad4Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPikkuskraad2(@NotNull SmapsParser.Pikkuskraad2Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPikkuskraad2(@NotNull SmapsParser.Pikkuskraad2Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKoordinaatideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.KoordinaatideJarjendiAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKoordinaatideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.KoordinaatideJarjendiAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPikkuskraad3(@NotNull SmapsParser.Pikkuskraad3Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPikkuskraad3(@NotNull SmapsParser.Pikkuskraad3Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJooneParameeterKoordinaadid(@NotNull SmapsParser.JooneParameeterKoordinaadidContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJooneParameeterKoordinaadid(@NotNull SmapsParser.JooneParameeterKoordinaadidContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeAvaldisSuluavaldis(@NotNull SmapsParser.ArvudeAvaldisSuluavaldisContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeAvaldisSuluavaldis(@NotNull SmapsParser.ArvudeAvaldisSuluavaldisContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeAvaldisArv(@NotNull SmapsParser.ArvudeAvaldisArvContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeAvaldisArv(@NotNull SmapsParser.ArvudeAvaldisArvContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterValikulauseIf(@NotNull SmapsParser.ValikulauseIfContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitValikulauseIf(@NotNull SmapsParser.ValikulauseIfContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterStruktuuriDeklaratsioon(@NotNull SmapsParser.StruktuuriDeklaratsioonContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitStruktuuriDeklaratsioon(@NotNull SmapsParser.StruktuuriDeklaratsioonContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterValikulauseElseIf(@NotNull SmapsParser.ValikulauseElseIfContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitValikulauseElseIf(@NotNull SmapsParser.ValikulauseElseIfContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisToevaartused(@NotNull SmapsParser.VordlusAvaldisToevaartusedContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisToevaartused(@NotNull SmapsParser.VordlusAvaldisToevaartusedContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaartideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.KaartideJarjendiAvaldisLiitmineLahutamineContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaartideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.KaartideJarjendiAvaldisLiitmineLahutamineContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterAlaParameeterNimi(@NotNull SmapsParser.AlaParameeterNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitAlaParameeterNimi(@NotNull SmapsParser.AlaParameeterNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktideJarjendiAvaldisPunktideJarjend(@NotNull SmapsParser.PunktideJarjendiAvaldisPunktideJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktideJarjendiAvaldisPunktideJarjend(@NotNull SmapsParser.PunktideJarjendiAvaldisPunktideJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKorduslauseFor(@NotNull SmapsParser.KorduslauseForContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKorduslauseFor(@NotNull SmapsParser.KorduslauseForContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeJarjendiAvaldisArvudeJarjend(@NotNull SmapsParser.ArvudeJarjendiAvaldisArvudeJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeJarjendiAvaldisArvudeJarjend(@NotNull SmapsParser.ArvudeJarjendiAvaldisArvudeJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterObjektideJarjendMuutujaNimi(@NotNull SmapsParser.ObjektideJarjendMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitObjektideJarjendMuutujaNimi(@NotNull SmapsParser.ObjektideJarjendMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterJooneParameeterKuva(@NotNull SmapsParser.JooneParameeterKuvaContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitJooneParameeterKuva(@NotNull SmapsParser.JooneParameeterKuvaContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterVordlusAvaldisAladeJarjend(@NotNull SmapsParser.VordlusAvaldisAladeJarjendContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitVordlusAvaldisAladeJarjend(@NotNull SmapsParser.VordlusAvaldisAladeJarjendContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeAvaldisTriviaalne1(@NotNull SmapsParser.ArvudeAvaldisTriviaalne1Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeAvaldisTriviaalne1(@NotNull SmapsParser.ArvudeAvaldisTriviaalne1Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterSoneSone(@NotNull SmapsParser.SoneSoneContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSoneSone(@NotNull SmapsParser.SoneSoneContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeAvaldisTriviaalne3(@NotNull SmapsParser.ArvudeAvaldisTriviaalne3Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeAvaldisTriviaalne3(@NotNull SmapsParser.ArvudeAvaldisTriviaalne3Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterKaardiParameeterJooned(@NotNull SmapsParser.KaardiParameeterJoonedContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitKaardiParameeterJooned(@NotNull SmapsParser.KaardiParameeterJoonedContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeAvaldisTriviaalne2(@NotNull SmapsParser.ArvudeAvaldisTriviaalne2Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeAvaldisTriviaalne2(@NotNull SmapsParser.ArvudeAvaldisTriviaalne2Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterObjektiParameeterKoordinaadid(@NotNull SmapsParser.ObjektiParameeterKoordinaadidContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitObjektiParameeterKoordinaadid(@NotNull SmapsParser.ObjektiParameeterKoordinaadidContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLaiuskraad1(@NotNull SmapsParser.Laiuskraad1Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLaiuskraad1(@NotNull SmapsParser.Laiuskraad1Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLaiuskraad2(@NotNull SmapsParser.Laiuskraad2Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLaiuskraad2(@NotNull SmapsParser.Laiuskraad2Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLaiuskraad3(@NotNull SmapsParser.Laiuskraad3Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLaiuskraad3(@NotNull SmapsParser.Laiuskraad3Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterPunktiKoordinaadidMuutujaNimi(@NotNull SmapsParser.PunktiKoordinaadidMuutujaNimiContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitPunktiKoordinaadidMuutujaNimi(@NotNull SmapsParser.PunktiKoordinaadidMuutujaNimiContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterLaiuskraad4(@NotNull SmapsParser.Laiuskraad4Context ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLaiuskraad4(@NotNull SmapsParser.Laiuskraad4Context ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterArvudeAvaldisUnaarneMiinus(@NotNull SmapsParser.ArvudeAvaldisUnaarneMiinusContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitArvudeAvaldisUnaarneMiinus(@NotNull SmapsParser.ArvudeAvaldisUnaarneMiinusContext ctx) { }

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEveryRule(@NotNull ParserRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitEveryRule(@NotNull ParserRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitTerminal(@NotNull TerminalNode node) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitErrorNode(@NotNull ErrorNode node) { }
}