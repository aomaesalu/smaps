// Generated from C:/Users/Ants-Oskar/Documents/Bitbucket/smaps/src\Smaps.g4 by ANTLR 4.x
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SmapsParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SmapsVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SmapsParser#AladeJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAladeJarjendMuutujaNimi(@NotNull SmapsParser.AladeJarjendMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisToevaartusteJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisToevaartusteJarjend(@NotNull SmapsParser.VordlusAvaldisToevaartusteJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ObjektideJarjendiAvaldisObjektideJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjektideJarjendiAvaldisObjektideJarjend(@NotNull SmapsParser.ObjektideJarjendiAvaldisObjektideJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ObjektiParameeterTuup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjektiParameeterTuup(@NotNull SmapsParser.ObjektiParameeterTuupContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ArvudeJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#LaiuskoordinaatideAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLaiuskoordinaatideAvaldisLiitmineLahutamine(@NotNull SmapsParser.LaiuskoordinaatideAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#JoonteJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoonteJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.JoonteJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#AlaParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlaParameeterKoordinaadid(@NotNull SmapsParser.AlaParameeterKoordinaadidContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisJoonteJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisJoonteJarjend(@NotNull SmapsParser.VordlusAvaldisJoonteJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktiKoordinaadidPunkt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktiKoordinaadidPunkt(@NotNull SmapsParser.PunktiKoordinaadidPunktContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#valikulause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValikulause(@NotNull SmapsParser.ValikulauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#sonedeJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSonedeJarjend(@NotNull SmapsParser.SonedeJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KorduslauseWhile}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKorduslauseWhile(@NotNull SmapsParser.KorduslauseWhileContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#omistamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOmistamine(@NotNull SmapsParser.OmistamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PikkuskoordinaatideAvaldisPikkuskoordinaat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPikkuskoordinaatideAvaldisPikkuskoordinaat(@NotNull SmapsParser.PikkuskoordinaatideAvaldisPikkuskoordinaatContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#programm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgramm(@NotNull SmapsParser.ProgrammContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisSonedeJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisSonedeJarjend(@NotNull SmapsParser.VordlusAvaldisSonedeJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KoordinaatideJarjendiAvaldisKoordinaatideJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKoordinaatideJarjendiAvaldisKoordinaatideJarjend(@NotNull SmapsParser.KoordinaatideJarjendiAvaldisKoordinaatideJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaardiParameeterNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaardiParameeterNimi(@NotNull SmapsParser.KaardiParameeterNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktMuutujaNimi(@NotNull SmapsParser.PunktMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#AlaParameeterKuva}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlaParameeterKuva(@NotNull SmapsParser.AlaParameeterKuvaContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#kommentaar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKommentaar(@NotNull SmapsParser.KommentaarContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#AlaParameeterTuup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlaParameeterTuup(@NotNull SmapsParser.AlaParameeterTuupContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#arvudeJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeJarjend(@NotNull SmapsParser.ArvudeJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeAvaldisLiitmineLahutamine(@NotNull SmapsParser.ArvudeAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaartideJarjendiAvaldisKaartideJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaartideJarjendiAvaldisKaartideJarjend(@NotNull SmapsParser.KaartideJarjendiAvaldisKaartideJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#SoneMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSoneMuutujaNimi(@NotNull SmapsParser.SoneMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisSone}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisSone(@NotNull SmapsParser.VordlusAvaldisSoneContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PikkuskoordinaatideAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPikkuskoordinaatideAvaldisLiitmineLahutamine(@NotNull SmapsParser.PikkuskoordinaatideAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktKoordinaadid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktKoordinaadid(@NotNull SmapsParser.PunktKoordinaadidContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaartideJarjendJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaartideJarjendJarjend(@NotNull SmapsParser.KaartideJarjendJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaardiParameeterObjektid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaardiParameeterObjektid(@NotNull SmapsParser.KaardiParameeterObjektidContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#LaiuskoordinaatMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLaiuskoordinaatMuutujaNimi(@NotNull SmapsParser.LaiuskoordinaatMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisObjektideJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisObjektideJarjend(@NotNull SmapsParser.VordlusAvaldisObjektideJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisToevaartus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisToevaartus(@NotNull SmapsParser.VordlusAvaldisToevaartusContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#lause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLause(@NotNull SmapsParser.LauseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisKoordinaatideJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisKoordinaatideJarjend(@NotNull SmapsParser.VordlusAvaldisKoordinaatideJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisArvud}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisArvud(@NotNull SmapsParser.VordlusAvaldisArvudContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisArvudeJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisArvudeJarjend(@NotNull SmapsParser.VordlusAvaldisArvudeJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#SonedeJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSonedeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.SonedeJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktideJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktideJarjendMuutujaNimi(@NotNull SmapsParser.PunktideJarjendMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#koordinaatideJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKoordinaatideJarjend(@NotNull SmapsParser.KoordinaatideJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PikkuskraadIda}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPikkuskraadIda(@NotNull SmapsParser.PikkuskraadIdaContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#AladeJarjendiAvaldisAladeJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAladeJarjendiAvaldisAladeJarjend(@NotNull SmapsParser.AladeJarjendiAvaldisAladeJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#muutujaDeklaratsioon}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMuutujaDeklaratsioon(@NotNull SmapsParser.MuutujaDeklaratsioonContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisKoordinaadid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisKoordinaadid(@NotNull SmapsParser.VordlusAvaldisKoordinaadidContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#JoonteJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoonteJarjendMuutujaNimi(@NotNull SmapsParser.JoonteJarjendMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#AladeJarjendJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAladeJarjendJarjend(@NotNull SmapsParser.AladeJarjendJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PikkuskoordinaatMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPikkuskoordinaatMuutujaNimi(@NotNull SmapsParser.PikkuskoordinaatMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#LaiuskoordinaatideAvaldisLaiuskoordinaat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLaiuskoordinaatideAvaldisLaiuskoordinaat(@NotNull SmapsParser.LaiuskoordinaatideAvaldisLaiuskoordinaatContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaartideJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaartideJarjendMuutujaNimi(@NotNull SmapsParser.KaartideJarjendMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#JooneParameeterTuup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJooneParameeterTuup(@NotNull SmapsParser.JooneParameeterTuupContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktiParameeterNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktiParameeterNimi(@NotNull SmapsParser.PunktiParameeterNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ToevaartusteJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToevaartusteJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ToevaartusteJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PikkuskoordinaatPikkuskraad}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPikkuskoordinaatPikkuskraad(@NotNull SmapsParser.PikkuskoordinaatPikkuskraadContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktideJarjendJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktideJarjendJarjend(@NotNull SmapsParser.PunktideJarjendJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.PunktideJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ObjektideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjektideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.ObjektideJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ObjektiParameeterKuva}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjektiParameeterKuva(@NotNull SmapsParser.ObjektiParameeterKuvaContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaardiParameeterTuup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaardiParameeterTuup(@NotNull SmapsParser.KaardiParameeterTuupContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisPunktideJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisPunktideJarjend(@NotNull SmapsParser.VordlusAvaldisPunktideJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#LaiuskraadLouna}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLaiuskraadLouna(@NotNull SmapsParser.LaiuskraadLounaContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#SonedeJarjendiAvaldisSonedeJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSonedeJarjendiAvaldisSonedeJarjend(@NotNull SmapsParser.SonedeJarjendiAvaldisSonedeJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ObjektiParameeterMootmed}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjektiParameeterMootmed(@NotNull SmapsParser.ObjektiParameeterMootmedContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ToevaartusteJarjendiAvaldisToevaartusteJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToevaartusteJarjendiAvaldisToevaartusteJarjend(@NotNull SmapsParser.ToevaartusteJarjendiAvaldisToevaartusteJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#JooneParameeterNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJooneParameeterNimi(@NotNull SmapsParser.JooneParameeterNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaardiParameeterKaardid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaardiParameeterKaardid(@NotNull SmapsParser.KaardiParameeterKaardidContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#JoonteJarjendiAvaldisJoonteJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoonteJarjendiAvaldisJoonteJarjend(@NotNull SmapsParser.JoonteJarjendiAvaldisJoonteJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeAvaldisKorrutamineJagamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeAvaldisKorrutamineJagamine(@NotNull SmapsParser.ArvudeAvaldisKorrutamineJagamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#toevaartusteJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToevaartusteJarjend(@NotNull SmapsParser.ToevaartusteJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PikkuskraadLaas}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPikkuskraadLaas(@NotNull SmapsParser.PikkuskraadLaasContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#SonedeAvaldisSone}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSonedeAvaldisSone(@NotNull SmapsParser.SonedeAvaldisSoneContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#valikulauseElse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValikulauseElse(@NotNull SmapsParser.ValikulauseElseContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktiParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktiParameeterKoordinaadid(@NotNull SmapsParser.PunktiParameeterKoordinaadidContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#JoonteJarjendJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoonteJarjendJarjend(@NotNull SmapsParser.JoonteJarjendJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ObjektiParameeterNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjektiParameeterNimi(@NotNull SmapsParser.ObjektiParameeterNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#aritmeetilineAvaldisArv}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAritmeetilineAvaldisArv(@NotNull SmapsParser.AritmeetilineAvaldisArvContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#LaiuskoordinaatLaiuskraad}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLaiuskoordinaatLaiuskraad(@NotNull SmapsParser.LaiuskoordinaatLaiuskraadContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#AladeJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAladeJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.AladeJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#LaiuskraadPohi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLaiuskraadPohi(@NotNull SmapsParser.LaiuskraadPohiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeAvaldisMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeAvaldisMuutujaNimi(@NotNull SmapsParser.ArvudeAvaldisMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaardiParameeterAlad}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaardiParameeterAlad(@NotNull SmapsParser.KaardiParameeterAladContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ObjektideJarjendJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjektideJarjendJarjend(@NotNull SmapsParser.ObjektideJarjendJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KorduslauseDoWhile}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKorduslauseDoWhile(@NotNull SmapsParser.KorduslauseDoWhileContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisKaartidedeJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisKaartidedeJarjend(@NotNull SmapsParser.VordlusAvaldisKaartidedeJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktiParameeterKuva}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktiParameeterKuva(@NotNull SmapsParser.PunktiParameeterKuvaContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaardiParameeterPunktid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaardiParameeterPunktid(@NotNull SmapsParser.KaardiParameeterPunktidContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#struktuurideJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruktuurideJarjend(@NotNull SmapsParser.StruktuurideJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#SonedeAvaldisMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSonedeAvaldisMuutujaNimi(@NotNull SmapsParser.SonedeAvaldisMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#Pikkuskraad1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPikkuskraad1(@NotNull SmapsParser.Pikkuskraad1Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#Pikkuskraad4}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPikkuskraad4(@NotNull SmapsParser.Pikkuskraad4Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#Pikkuskraad2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPikkuskraad2(@NotNull SmapsParser.Pikkuskraad2Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KoordinaatideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKoordinaatideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.KoordinaatideJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#Pikkuskraad3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPikkuskraad3(@NotNull SmapsParser.Pikkuskraad3Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#JooneParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJooneParameeterKoordinaadid(@NotNull SmapsParser.JooneParameeterKoordinaadidContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeAvaldisSuluavaldis}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeAvaldisSuluavaldis(@NotNull SmapsParser.ArvudeAvaldisSuluavaldisContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeAvaldisArv}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeAvaldisArv(@NotNull SmapsParser.ArvudeAvaldisArvContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#valikulauseIf}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValikulauseIf(@NotNull SmapsParser.ValikulauseIfContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#struktuuriDeklaratsioon}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruktuuriDeklaratsioon(@NotNull SmapsParser.StruktuuriDeklaratsioonContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#valikulauseElseIf}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValikulauseElseIf(@NotNull SmapsParser.ValikulauseElseIfContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisToevaartused}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisToevaartused(@NotNull SmapsParser.VordlusAvaldisToevaartusedContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaartideJarjendiAvaldisLiitmineLahutamine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaartideJarjendiAvaldisLiitmineLahutamine(@NotNull SmapsParser.KaartideJarjendiAvaldisLiitmineLahutamineContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#AlaParameeterNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlaParameeterNimi(@NotNull SmapsParser.AlaParameeterNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktideJarjendiAvaldisPunktideJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktideJarjendiAvaldisPunktideJarjend(@NotNull SmapsParser.PunktideJarjendiAvaldisPunktideJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KorduslauseFor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKorduslauseFor(@NotNull SmapsParser.KorduslauseForContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeJarjendiAvaldisArvudeJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeJarjendiAvaldisArvudeJarjend(@NotNull SmapsParser.ArvudeJarjendiAvaldisArvudeJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ObjektideJarjendMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjektideJarjendMuutujaNimi(@NotNull SmapsParser.ObjektideJarjendMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#JooneParameeterKuva}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJooneParameeterKuva(@NotNull SmapsParser.JooneParameeterKuvaContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#VordlusAvaldisAladeJarjend}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVordlusAvaldisAladeJarjend(@NotNull SmapsParser.VordlusAvaldisAladeJarjendContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeAvaldisTriviaalne1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeAvaldisTriviaalne1(@NotNull SmapsParser.ArvudeAvaldisTriviaalne1Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#SoneSone}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSoneSone(@NotNull SmapsParser.SoneSoneContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeAvaldisTriviaalne3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeAvaldisTriviaalne3(@NotNull SmapsParser.ArvudeAvaldisTriviaalne3Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#KaardiParameeterJooned}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKaardiParameeterJooned(@NotNull SmapsParser.KaardiParameeterJoonedContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeAvaldisTriviaalne2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeAvaldisTriviaalne2(@NotNull SmapsParser.ArvudeAvaldisTriviaalne2Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ObjektiParameeterKoordinaadid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjektiParameeterKoordinaadid(@NotNull SmapsParser.ObjektiParameeterKoordinaadidContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#Laiuskraad1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLaiuskraad1(@NotNull SmapsParser.Laiuskraad1Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#Laiuskraad2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLaiuskraad2(@NotNull SmapsParser.Laiuskraad2Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#Laiuskraad3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLaiuskraad3(@NotNull SmapsParser.Laiuskraad3Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#PunktiKoordinaadidMuutujaNimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPunktiKoordinaadidMuutujaNimi(@NotNull SmapsParser.PunktiKoordinaadidMuutujaNimiContext ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#Laiuskraad4}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLaiuskraad4(@NotNull SmapsParser.Laiuskraad4Context ctx);

	/**
	 * Visit a parse tree produced by {@link SmapsParser#ArvudeAvaldisUnaarneMiinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArvudeAvaldisUnaarneMiinus(@NotNull SmapsParser.ArvudeAvaldisUnaarneMiinusContext ctx);
}